<?
function jour_ferie($timestamp){
    $jour = date("d", $timestamp);
    $mois = date("m", $timestamp);
    $annee = date("Y", $timestamp);
    $EstFerie = '' ;
    // dates f�ri�es fixes
    if($jour == 1 && $mois == 1) $EstFerie = 1; // 1er janvier
    if($jour == 1 && $mois == 5) $EstFerie = 1; // 1er mai
    if($jour == 8 && $mois == 5) $EstFerie = 1; // 8 mai
    if($jour == 14 && $mois == 7) $EstFerie = 1; // 14 juillet
    if($jour == 15 && $mois == 8) $EstFerie = 1; // 15 aout
    if($jour == 1 && $mois == 11) $EstFerie = 1; // 1 novembre
    if($jour == 11 && $mois == 11) $EstFerie = 1; // 11 novembre
    if($jour == 25 && $mois == 12) $EstFerie = 1; // 25 d�cembre
    // fetes religieuses mobiles
    $pak = easter_date($annee);
    $jp = date("d", $pak);
    $mp = date("m", $pak);
    if($jp == $jour && $mp == $mois){ $EstFerie = 1;} // P�ques
    $lpk = mktime(date("H", $pak), date("i", $pak), date("s", $pak), date("m", $pak) , date("d", $pak) +1, date("Y", $pak) );
    $jp = date("d", $lpk);
    $mp = date("m", $lpk);
    if($jp == $jour && $mp == $mois){ $EstFerie = 1; }// Lundi de P�ques
    $asc = mktime(date("H", $pak), date("i", $pak), date("s", $pak), date("m", $pak) , date("d", $pak) + 39, date("Y", $pak) );
    $jp = date("d", $asc);
    $mp = date("m", $asc);
    if($jp == $jour && $mp == $mois){ $EstFerie = 1;}//ascension
    $pe = mktime(date("H", $pak), date("i", $pak), date("s", $pak), date("m", $pak), date("d", $pak) + 49, date("Y", $pak) );
    $jp = date("d", $pe);
    $mp = date("m", $pe);
    if($jp == $jour && $mp == $mois) { $EstFerie = 1;}// Pentec�te
    $lp = mktime(date("H", $asc), date("i", $pak), date("s", $pak), date("m", $pak),     date("d", $pak) + 50, date("Y", $pak) );
    $jp = date("d", $lp);
    $mp = date("m", $lp);
    if($jp == $jour && $mp == $mois) { $EstFerie = 1;}// lundi Pentec�te
    // Samedis et dimanches
   // $jour_sem = jddayofweek(unixtojd($timestamp), '');
   // if($jour_sem ==  || $jour_sem == 6) $EstFerie = 1;
    // ces deux lignes au dessus sont � retirer si vous ne d�sirez pas faire
    // apparaitre les
    // samedis et dimanches comme f�ri�s.
    return $EstFerie;
}

function dateFR($jj, $mm, $aaaa) {
  $userDate = mktime(0,0,0,$mm,$jj,$aaaa);
  $jours = array('dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi');
  $mois = array('', 'janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre');
  return $jours[date("w", $userDate)] . " " . $jj . " " .   $mois[date("n", $userDate)] . " " . $aaaa; 
  }

function stripAccentsECH($string){
	return strtr($string,'�����������������������������������������������������',
'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUYNn');
}

function DateEcheance($dateFact,$type_paiement,$diff,$type_fact,$formatDATE){
    include 'conf/param_docvte.php';

    setlocale(LC_TIME, "fr_FR", "fr_FR@euro", "fr", "FR", "fra_fra", "fra");

    $TABdateFact            = explode("-", $dateFact); //Date de facturation
    //$TABdateCreaFact        = explode("-", $dateCreaFact); // Date creation facture
    //$timestampdateCreaFact  = mktime(0, 0, 0,  $TABdateCreaFact[1] , $TABdateCreaFact[2] , $TABdateCreaFact[0]);
    $timestampdateFact      = mktime(0, 0, 0,  $TABdateFact[1] , $TABdateFact[2] , $TABdateFact[0]);

    //--> type paiement
    if(ereg("prelevement",strtolower(stripAccentsECH($type_paiement)))){
        $type_paiement  = 'PRLV';
        $execECH        = true;

    }elseif(ereg("mandat",strtolower(stripAccentsECH($type_paiement)))){
        $type_paiement  = 'MDTADMIN';
        $execECH        = true;
        $diff = 0; // pas de paiement diff�r�

    }elseif(ereg("virement",strtolower(stripAccentsECH($type_paiement)))){
        $type_paiement  = 'VIRMNT';
        $execECH        = true;
        $diff = 0; // pas de paiement diff�r�

    }elseif(ereg("3_ch",strtolower(stripAccentsECH($type_paiement)))){
        $type_paiement  = '3CHQ';
        $execECH        = true;
        $diff = 0; // pas de paiement diff�r�

    }elseif(ereg("carte",strtolower(stripAccentsECH($type_paiement))) or ereg("CB",strtolower(stripAccentsECH($type_paiement)))){
        $type_paiement  = 'CB';
        $execECH        = true;
        $diff = 0; // pas de paiement diff�r�

    }elseif(ereg("cheque",strtolower(stripAccentsECH($type_paiement))) or ereg("cheque",strtolower(stripAccentsECH($type_paiement))) ){
        $type_paiement  = 'CHQ';
        $execECH        = true;
        $diff = 0; // pas de paiement diff�r�

    }else{
        $execECH = false;
        if($formatDATE == 'date_fr' or empty($formatDATE)){
            $Ddateech = $dateFact;
        }elseif($formatDATE == 'ftime_Fr' ){
           $Ddateech  = strftime("%A %d %B %Y" , $timestampdateFact);
        }
         $diff = 0; // pas de paiement diff�r�

    }
     
    if($execECH == true){
            if($type_fact == 'fact_ABO' or $type_fact == 'fact_ABOVOIP'){ 
                 $CalcSE = true;

            }else{
                $type_fact      = 'fact_DIV';    
                $CalcSE         = true;
                if($jrsDateSTART[$type_fact][$type_paiement] != '') $CalcSE   = false;

            }
            if($jrsDateSTART[$type_fact][$type_paiement] != '') $TABdateFact[2] = $jrsDateSTART[$type_fact][$type_paiement]; //-> Si jour impos�
            $addM       = $TABNBRmoisDateEch[$type_fact][$type_paiement];  //--> + X MOIS
            $addD       = $TABNBRjrsDateEch[$type_fact][$type_paiement] + $diff; //--> + X JOURS
            $timestamp  = mktime(0, 0, 0,  $TABdateFact[1] + $addM, $TABdateFact[2] + $addD, $TABdateFact[0]);

            //-------------------------------------> Analyse des jours f�ri� et week-end entre date_fact date_echeance
             if($CalcSE == true){
                  $dateDeb = $dateFact;
                  $dateFin = date('Y', $timestamp)."-".date('m', $timestamp)."-".date('d', $timestamp);

                  $date = new DateTime($dateDeb);
                  do {
                    if (($date -> format('N') != 6) && ($date -> format('N') != 7)) {
                        $date -> format('Y-m-d');
                    }
                    $dateDeb = $date -> format('Y-m-d');

                    if(jour_ferie(mktime(0, 0, 0,   $date -> format('m'),  $date -> format('d'),  $date -> format('Y'))) == 1){
                        $addD ++;
                    }
                    if(ereg("samedi",strftime("%A %d %B %Y" , mktime(0, 0, 0,   $date -> format('m'),  $date -> format('d'),  $date -> format('Y'))))){
                       $addD ++;
                    }
                    if(ereg("dimanche",strftime("%A %d %B %Y" , mktime(0, 0, 0,   $date -> format('m'),  $date -> format('d'),  $date -> format('Y'))))){
                        $addD ++;
                    }

                    $date -> modify('+1 day');
                  }
                  while ($dateDeb < $dateFin);
             }
            //-------------------------------------> Analyse du resultat finale
            //-> Detection jour f�rie
                if(jour_ferie(mktime(0, 0, 0,  $TABdateFact[1] + $addM, $TABdateFact[2] + $addD, $TABdateFact[0])) == 1){ // si c un jour ferie
                    $addD = $addD + 1; // ajoute un jour
                }
            //<--

            $timestamp      = mktime(0, 0, 0,  $TABdateFact[1] + $addM, $TABdateFact[2] + $addD, $TABdateFact[0]);
            $Ddateech       = strftime("%A %d %B %Y" , $timestamp);

            //--> Detection si week-end
                if(ereg("samedi",$Ddateech)){ // Si c un samedi
                   $timestamp = mktime(0, 0, 0,  $TABdateFact[1] + $addM, $TABdateFact[2] + $addD + 2, $TABdateFact[0]);
                }elseif(ereg("dimanche",$Ddateech)){ // si c un dimanche
                   $timestamp  = mktime(0, 0, 0,  $TABdateFact[1] + $addM, $TABdateFact[2] + $addD + 1, $TABdateFact[0]);
                }
            //<--
    
            if($formatDATE == 'date_fr' or empty($formatDATE)){
                $Ddateech = date('Y', $timestamp)."-".date('m', $timestamp)."-".date('d', $timestamp);
            }elseif($formatDATE == 'ftime_Fr' ){
                $Ddateech  = strftime("%A %d %B %Y" , $timestamp);
            }
    }
    return   $Ddateech;
}
?>
