<?php
ini_set('display_errors','off');
session_start();

include '__config_data.php'; // --> variable environnement TALKAK
include '__AL.php';   // -->autload

$_POST['lg'] = 'fr';  //--> par défaut
include '../LG/'.$_POST['lg'].'/data_lg.php';  //--> traductions des textes dans la langue de  l utilksateur
$inst_sys = new SYS();
$inst_fuser = new modules_Req_fuser();
$inst_Query = new core_Query();  //--> class pour les reqêtes mysql

$ID_sys_users= 24;  //-> ID utilisateur présent dans ma base 
 $DATA_user = $inst_fuser->READ_fuser_by_ID_sys_users($ID_sys_users);  // > lecture des infos sur le user expéditeur
 
 $ID_sys_users_recipient= 17;  //-> ID utilisateur présent dans ma base 
 $DATA_userRecipient = $inst_fuser->READ_fuser_by_ID_sys_users($ID_sys_users_recipient);  // > lecture des infos sur le user recepteur moi
 
 // ci dessous le type de contenu de la notification
  $data['objetnotif'] =''. $DATA_user['users_prenom'].' '. $DATA_user['users_nom'].'  a mentionné'.constant('core_path::NOTIFY_sex_'. $DATA_user['users_gender'].'').' votre nom sur sa publication'; 
  $data['objet'] ='test de notification pour voir';
   $lien= ''.URL_RACINE.'?sys=qhapps.flx_medium&lg='. $_POST['lg'] .'&idzn=qhapps.flx_medium&modif_data=2891&t_notif=mentionname_flx&Sender_ID_sys_users='.$DATA_user['ID_sys_users'].'';                                                      
                          // echo $data['objet']; 

	function sendMessage(){

		//Titre de la notification à envoyer 
		$heading = array(
			"en" => "Talk AGriculture"
		);

		//Contenu de la notification
		$content = array(
			"en" => 'Notification envoyé depuis un script PHP',
			);

		$fields = array(
			//Id du projet créer sur OneSignal
			'app_id' => "c2eb8486-f28c-4dc4-8e9f-1a2df477caaf",
			//Token de l'utilisateur récepteur de la notification
			//Ce token est sauvegardé dans notre base de données
			//On le récupère puis le met dans l'array ci-dessous
			//Les deux que j'ai mis ici sont pour moi (Le 1er) et pour toi (le second)
			'include_player_ids' => array("71339a79-4a60-412c-bb64-dc68a7c82c12","1121d8c1-87f6-4fb6-8e7e-c154486c93f8"),

			//Méta données associées à la notification avec des clé et des values ["cle" => "valeur"]
			'data' => array("cle" => "valeur", "cle1" => "valeur1"),
			'contents' => $content,
			'headings' => $heading,
			//Nom du logo que j'ai défini dans le projet ANDROID
			'small_icon' => "talk_ag_logo",
			'large_icon' => "talk_ag_logo",
			//Lien de redirection lors du click sur la notification
			//Ce lien represente la variable $lien qui est plus haut
			"url"  => "https://www.talkag.com/ID/?lg=fr",
		);
		
		$fields = json_encode($fields);
    	print("\nJSON sent:\n");
    	print($fields);

    	$restApiKey = "MmM2N2RkYTctMmMwNi00Zjg2LThhZDktZTg3NDg1MGUyZGE2";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic '.$restApiKey));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		//Envoie de la notification 
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
	
	$response = sendMessage();
	$return["allresponses"] = $response;
	$return = json_encode( $return);
	//Partie optionnel
	//Permet d'afficher le rapport d'envoie de notification
	print("\n\nJSON received:\n");
	print($return);
	print("\n");                                                 
