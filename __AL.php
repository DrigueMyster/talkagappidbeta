<?php

function autoload_OBJ_core($classname){
    file_exists(''.realpath(dirname(__FILE__)).'/'.REP_CLASS.'/_OBJ_core/'.$classname.'.php') &&
    require ''.realpath(dirname(__FILE__)).'/'.REP_CLASS.'/_OBJ_core/'.$classname.'.php';
}

function autoload_OBJ_modules($classname){
    file_exists(''.realpath(dirname(__FILE__)).'/'.REP_CLASS.'/_OBJ_modules/'.$classname.'.php') &&
    require ''.realpath(dirname(__FILE__)).'/'.REP_CLASS.'/_OBJ_modules/'.$classname.'.php';
}

function autoload_OBJ_sys($classname){
    file_exists(''.realpath(dirname(__FILE__)).'/'.REP_CLASS.'/_OBJ_sys/'.$classname.'.php') &&
    require ''.realpath(dirname(__FILE__)).'/'.REP_CLASS.'/_OBJ_sys/'.$classname.'.php';
}


function autoload_OBJ_ihm($classname){
    file_exists(''.realpath(dirname(__FILE__)).'/'.REP_CLASS.'/_OBJ_ihm/'.$classname.'.php') &&
    require ''.realpath(dirname(__FILE__)).'/'.REP_CLASS.'/_OBJ_ihm/'.$classname.'.php';
}

function autoload_OBJ_api($classname){
    file_exists(''.realpath(dirname(__FILE__)).'/'.REP_CLASS.'/_OBJ_api/'.$classname.'.php') &&
    require ''.realpath(dirname(__FILE__)).'/'.REP_CLASS.'/_OBJ_api/'.$classname.'.php';
}


spl_autoload_register('autoload_OBJ_core');
spl_autoload_register('autoload_OBJ_modules');
spl_autoload_register('autoload_OBJ_sys');
spl_autoload_register('autoload_OBJ_ihm');
spl_autoload_register('autoload_OBJ_api');
require_once 'Facebook/autoload.php'; // change path as needed