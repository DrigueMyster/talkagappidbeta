<?php
//ini_set('display_errors','off');

ini_set('display_errors','off');
session_start();
header('Content-type: text/html; charset=UTF-8');

include '__config_data.php';
include '__AL.php';

$inst_sys = new SYS();
$inst_Query = new core_Query();
$inst_fste = new modules_Req_fste();
$inst_fuser = new modules_Req_fuser();
$core_Model = new core_model();
$core_Model->model_VAR();
 $inst_routines= new core_Routines();
$loop = FALSE;
$start_qhm=false;
$DATA_sys =$inst_sys->READ_SYS_process_by_Session_SID();



if(empty($DATA_sys['ID_sys_users'])){
                if($_GET['t_notif'] == 'aff_flx' or $_GET['t_notif'] == 'explorer' ){
                   //   header("Location: ".URL_RACINE_ID_m."?sys=sys.affflx&");
                   include 'aff_OFFflx.php';
                }else{
                      header("Location: ".URL_RACINE_ID_m."?sys=sys.ID");
                }

}else{
                $DATA_ste   =$inst_fste->READ_fste_by_ID($DATA_sys['ID_ste']);
                 $sys_geooptuse = $inst_sys->READ_ID_GEO($DATA_ste['Country']) ;
                   $DATA_ste['cont'] = $sys_geooptuse['cont'];
                $DATA_user = $inst_fuser->READ_fuser_by_ID_sys_users($DATA_sys['ID_sys_users']);
                setcookie("IDsysUsers",$DATA_sys['ID_ste'], time() + 365*24*3600); 
                setcookie("firstname",$DATA_user['users_prenom'], time() + 365*24*3600); 
                setcookie("name",$DATA_user['users_nom'], time() + 365*24*3600); 
                if($DATA_ste['city'] == 0){
                    $start_qhm = true;
                
                }
                
                
            
                $_GET['lg'] =$DATA_user['lg'];
                   include '../LG/'.$DATA_user['lg'].'/data_lg.php';
        //        include ''.REP_RACINE.''.REP_ROUTINES.'startCache.php';
              
                ?>
                    <!DOCTYPE html>
                    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=  $_GET['lg'] ?>" lang="<?=  $_GET['lg'] ?>"  >

                        <head>
                                <meta http-equiv="Content-Language" content="<?=  $_GET['lg'] ?>"/>
                                <meta http-equiv="Content-Type" content="text/html; charset=<?=  CHARSET ?>"/>
                                <meta name="viewport" content="width=device-width,initial-scale = 1.0 ,height=device-height">

                                <meta name="theme-color" content="#e40518">
                                <meta name="msapplication-navbutton-color" content="#e40518">
                                <meta name="apple-mobile-web-app-status-bar-style" content="#e40518">
                                
                                <meta property="og:site_name" content="TalkAG">
                                <meta property="og:description" content="<?= META_DESCRIPTION ?>">
                                <meta property="og:title" content="TalkAG parlons agriculture" />
                                <meta property="og:image" content="https://www.talkag.com/talkag/_IMG/fb_share_ta.jpg" />
                                <meta property="og:type" content="website" />
                                <meta property="og:url" content="https://www.talkag.com/" />
                                <meta property="fb:app_id" content="<?= FB_APP_ID ?>" />
                                <meta property="fb:pages" content="192238808048333">
                                

                                <meta http-equiv="imagetoolbar" CONTENT="no"/>
                                <link rel="shortcut icon" href="_IMG/talkag2.png" />
                                <link rel="icon" type="image/png" href="_IMG/talkag2.png" sizes="192x192">
                                <meta name="description" content=" TalkAG <?= WELCOME_SIGNUP ?>" />
                                <meta name="Distribution" content="TalkAG"/>
                                <meta name="Rating" content="General"/>
                                
                                
                                <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
                                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
                         
                                <script  type="text/javascript" src="<?=  ''.REP_MODULE_JS.'' ?>app_Scrollbar.js"></script>
                                <script id="facebook-jssdk" src="//connect.facebook.net/fr_FR/sdk.js" ></script>
                                
                                 
                                
                                <script   type="text/javascript" src="<?=  ''.REP_RACINE.''.REP_JS .'' ?>frmwIF_QHM.js"></script>
                                <script  type="text/javascript" src="<?=  ''.REP_RACINE.''.REP_JS .'' ?>frmw_QHM0001.js"></script>
                                <script   type="text/javascript" src="<?=  ''.REP_RACINE.''.REP_JS .'' ?>frmwIMG_QHM.js"></script>
                                <script  type="text/javascript" src="<?=  ''.REP_MODULE_JS.'' ?>module_js.js"></script>
                                <script type="text/javascript" src="<?=  REP_MODULE_JS ?>app_fb.js"></script>
                                <script  type="text/javascript" src="<?=  ''.REP_MODULE_JS.'' ?>module_lz.js"></script>
                             
                                <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=<?= GOOGLE_KEY ?>&lg=<?= $_GET['lg'] ?>"></script>
                                
                                <link href="https://vjs.zencdn.net/7.2.3/video-js.css" rel="stylesheet">
                                 <script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>
                           
                                <title>TalkAG</title>
                                  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                            
                                <link href="<?=  ''.REP_RACINE.''.REP_CSS.'' ?>form_data.css" 		rel="stylesheet" type="text/css"/>
                                <link href="<?=  ''.REP_RACINE.''.REP_MODULE_CSS.'' ?>module_head_m.css"         rel="stylesheet" type="text/css"/>
                                  <link href="<?=  ''.REP_RACINE.''.REP_MODULE_CSS.'' ?>mkhplayer.default.css"         rel="stylesheet" type="text/css"/>
                 
                                <link href="<?=  ''.REP_RACINE.''.REP_MODULE_CSS.'' ?>module_jq-ui.css"         rel="stylesheet" type="text/css"/>
                                <link href="<?=  ''.REP_RACINE.''.REP_MODULE_CSS.'' ?>app_Scrollbar.css"         rel="stylesheet" type="text/css"/>
                                <link href="<?=  ''.REP_RACINE.''.REP_CSS.'' ?>head.css"              rel="stylesheet"  type="text/css" />
                                <link href="<?=  ''.REP_RACINE.''.REP_CSS.'' ?>qharma.css"            rel="stylesheet" type="text/css" />
                                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700"            rel="stylesheet" type="text/css" />
                                <link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">
                                    
                                    
                          
                                    
                                    
   </head>
                       

<body      onLoad="javascript:geolloc();<?php  if($inst_routines->scanTYPEDEVICE() == 'mobile' ){ ?>javascript:rfnmobilet('<?=  $_GET['lg'] ?>');<?php }else{ ?>javascript:rfnt('<?=  $_GET['lg'] ?>');<?php } ?><?php   if($start_qhm == true){  ?>javascript:initint('<?=  URL_RACINE_DESK_m ?>','sys=<?= $_GET['sys'] ?>');<?php  }else{ ?>javascript:initint('<?=  URL_RACINE_DESK_m ?>','<?= $_SERVER['QUERY_STRING']?>');<?php }  ?> javascript:rson();"  >
    
                            <script type="text/javascript">inifb('<?= FB_APP_ID ?>');</script>
    
                            <form action="<?=  REP_RACINE ?>Query.php"  method="POST" name="Form_QHM" onSubmit="checkIFrame(); return false"  >
                                                    <input type="hidden" name="rQ"                          value="" />
                                                    <input type="hidden" name="sys"                         value="" />
                                                    <input type="hidden" name="lg"                          value="<?php  if(isset( $_GET['lg'] )){ ?><?= $_GET['lg'] ?><?php } ?>" />
                                                    <input type="hidden" name="level"                         value="" />
                                                    <input type="hidden" name="idzn"                         value="<?php  if(isset( $_GET['idzn'] )){ ?><?= $_GET['idzn'] ?><?php } ?>" />
                                                    <input type="hidden" name="zone_actu"                         value="<?php  if(isset( $_GET['zone'] )){ ?><?= $_GET['zone'] ?><?php } ?>" />

                                                    <input type="hidden" name="ibuiuri"                         value="<?= $_SERVER['HTTP_HOST'] ?><?= $_SERVER['REQUEST_URI'] ?>" />
                                                    <div id="wbaM"></div>
                                                     <?php
                                                                $inst_ihm = new modules_ihm_m();
                                                               if($start_qhm == true  ){
                                                                      
                                                                         //   $inst_ihm->HEAD_bar_startqhm($_GET['lg'], $DATA_sys);

                                                                }else{
                                                                      //    $inst_ihm->HEAD_bar($_GET['lg'], $DATA_ste, $DATA_sys);
                                                                                   
                                                                }     
                                                      ?>

                                                        <div id="master" class="form_qhm_format_right2" >
                                                                         <div id="frameMASTERcont" >  </div>
                                                         </div>

                                                         <div id="wbaM"></div>
                                               
                                                 
                                                 <script type="text/javascript">
                                                                    var geocoder;
                                                                    var map;
                                                                  
                                                                function auto_initialiserCarte(lat,long,zo) {
				   geocoder = new google.maps.Geocoder();
                                                                            var latlng = new google.maps.LatLng(lat, long);
                                                                            var mapOptions = {
						  zoom      : zo,
						center    : latlng,
						zoomControl: true,
						 mapTypeControl:false,
						     streetViewControl: false,
						mapTypeId : google.maps.MapTypeId.ROADMAP
					      } 
                                                                            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                                                                 }

                                                                 function findgeo(adr,field) {
                                                                       var adresse =adr;

                                                                       alert('test: ' + adresse);
                                                                         geocoder.geocode( { 'address': adresse}, function(results, status) {
                                                                           if (status == google.maps.GeocoderStatus.OK) {
                                                                                map.setCenter(results[0].geometry.location);
                                                                               // Récupération des coordonnées GPS du lieu tapé dans le formulaire
                                                                               var strposition = results[0].geometry.location+"";
                                                                               strposition=strposition.replace('(', '');
                                                                               strposition=strposition.replace(')', '');
                                                                           } else {
                                                                          //   alert('Adresse introuvable');
                                                                           }
                                                                         });
                                                                }
                                                        </script>
                                                     
                                                         
                                                        <script src="https://code.highcharts.com/highcharts.src.js"></script>
                                                        <script src="https://code.highcharts.com/modules/exporting.js"></script>
                                                        <script src="https://code.highcharts.com/modules/export-data.js"></script>
                                                        <script src="https://code.highcharts.com/modules/wordcloud.js"></script>
                                                        <script src="https://code.highcharts.com/maps/modules/data.js"></script>

                                                <div id="BKGRD_dialglobal" class="bkgrdglobal"  > </div>
                                                <div id="BKGRD_dialglobalblack" class="bkgrdglobalblack"  > </div>
                                                <div id="BKGRD_dialglobal2" class="bkgrdglobal2"  ><div class="cssload-spin-boxG"></div>   </div>
                                                <div id="BKGRD_dialglobalPGRS" class="bkgrdglobaPGRS "   ></div>
                                            
                                               
                                                <div id="BKGRD_dialglobal_content" class="bkgrdglobalcontent" onclick="javascript:Remove_winBoxAlertSEARCHw();"></div>
                                                <div id="frame_qhm"  ></div>
                                                <div id="QHM_bottompage"></div>
                                                  
                                </form>
                                                                                  
                               
                        </body>
                    </html>

                    <?php //  include''.REP_RACINE.''.REP_ROUTINES.'EndCache.php'; ?>
<?php } ?>
