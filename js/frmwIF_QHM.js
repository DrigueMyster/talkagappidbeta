var oIFrame = null;
function createIFrame() {
    var oIFrameElement = document.createElement("iframe");
    oIFrameElement.width=0;
    oIFrameElement.height=0;
    oIFrameElement.frameBorder=0;
    oIFrameElement.name = "hiddenFrame";
    oIFrameElement.id = "hiddenFrame";
    
    document.body.appendChild(oIFrameElement);
    oIFrame = frames["hiddenFrame"];
    document.getElementById("hiddenFrame" ).style.display="none";
}
function checkIFrame() {
   if (!oIFrame) {
      createIFrame();                
   } 
   setTimeout(function () { oIFrame.location = "routines/Proxyscript2.htm"; }, 10);
}
function formReady() {
   var oHiddenForm = oIFrame.document.forms[0];
   var oForm = document.forms[0];
   for (var i=0 ; i < oForm.elements.length; i++) {
            var oHidden = oIFrame.document.createElement("input");
            oHidden.type = "hidden";
            oHidden.name = oForm.elements[i].name;
            oHidden.value = oForm.elements[i].value;
            oHiddenForm.appendChild(oHidden);
    }
     oHiddenForm.action = oForm.action;
     oHiddenForm.submit();
}


