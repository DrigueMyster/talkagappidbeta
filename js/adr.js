   function TrouverAdresse(adr) {
                                  // Récupération de l'adresse tapée dans le formulaire
                         
                                      
                                  var adresse =adr;
                               //   alert('test: ' + adresse);
                                  geocoder.geocode( { 'address': adresse}, function(results, status) {
                                    if (status == google.maps.GeocoderStatus.OK) {
                                      map.setCenter(results[0].geometry.location);
                                      // Récupération des coordonnées GPS du lieu tapé dans le formulaire
                                      var strposition = results[0].geometry.location+"";
                                      strposition=strposition.replace('(', '');
                                      strposition=strposition.replace(')', '');
                                      // Affichage des coordonnées dans le <span>
                                     document.getElementById('text_latlng').innerHTML='Coordonnées : '+strposition;
                                      // Création du marqueur du lieu (épingle)
                                      var marker = new google.maps.Marker({
                                          map: map,
                                          position: results[0].geometry.location
                                       
                                      });
                                    } else {
                                      alert('Adresse introuvable: ' + status);
                                    }
                                  });
                                }