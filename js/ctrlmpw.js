// CtRl keysmpw

function chpw(obj) {
    if (obj.value.length ==0) {
        document.getElementById("text_err_passw_userSIGNUP").innerHTML = "";
        document.getElementById("text_err_passw_userSIGNUP").style.display = "none";
    } else if (obj.value.length <= 5) {
        document.getElementById("text_err_passw_userSIGNUP").innerHTML = "Trop court";
        document.getElementById("text_err_passw_userSIGNUP").style.display = "block";
    }else {
        document.getElementById("text_err_passw_userSIGNUP").innerHTML = "";
        document.getElementById("text_err_passw_userSIGNUP").style.display = "none";
    }
}

function chpw_account(obj) {
    if (obj.value.length ==0) {
        document.getElementById("text_err2_NEWpsswuser_account_pssw").innerHTML = "";
        document.getElementById("block_err2_NEWpsswuser_account_pssw").style.display = "none";
        document.getElementById("block_err3_NEWpsswuser_account_pssw").style.display = "none";
    } else if (obj.value.length <= 5) {
        document.getElementById("text_err2_NEWpsswuser_account_pssw").innerHTML = "Trop court";
        document.getElementById("block_err2_NEWpsswuser_account_pssw").style.display = "block";
    }else {
        document.getElementById("text_err2_NEWpsswuser_account_pssw").innerHTML = "";
        document.getElementById("block_err2_NEWpsswuser_account_pssw").style.display = "none";
    }
}

function COMPpw(obj) {    
    if ( obj.value.length == 0) {
         document.getElementById("text_err_passw_userSIGNUP").innerHTML = "";

    } else if ( document.Form_QHM.passw_userSIGNUP.value == obj.value) {
        document.getElementById("text_err_passw_userSIGNUP").innerHTML = "";
        document.getElementById("text_err_passw_userSIGNUP").style.display = "none";
        

    } else if ( document.Form_QHM.passw_userSIGNUP.value != obj.value) {
        document.getElementById("text_err_passw_userSIGNUP").innerHTML = "Les mots de passe ne sont pas identiques";
       
  }
}

function COMPpw2(obj) {
    if ( obj.value.length ==0) {
         document.getElementById("passwd_lv_txt3").innerHTML = "";

    } else if ( document.form_vd.cpt_pass1.value == obj.value) {
        document.getElementById("passwd_lv_txt3").innerHTML = "Mots de passe identiques";
        document.getElementById("passwd_lv_txt3").style.color = "#008000";

    } else if ( document.form_vd.cpt_pass1.value != obj.value) {
        document.getElementById("passwd_lv_txt3").innerHTML = "Les mots de passe ne sont pas identiques";
        document.getElementById("passwd_lv_txt3").style.color = "#C00000";
  }
}
//color -> worst
var initColor = new Array(208,16);
//color -> best
var endColor = new Array(16,208);
//
var composante3 = 16;
//messages
var infoSecure = "Fiabilité du mot de passe: "

var secureMsg = new Array("", infoSecure+"faible", infoSecure+"faible",infoSecure+"moyen",infoSecure+"élevé",infoSecure+"très élevé");
var coefColor = 13;

var valSecure = 0;

function Password_check(obj) {
    
  valSecure = 0;
  
  if (obj.value.match(/[a-z]/)) {
    valSecure++;
  }
  
  if (obj.value.match(/[A-Z]/)) {
    valSecure++;
  }
 
  if (obj.value.match(/\d+/)) {
    valSecure++;
  }

  if (obj.value.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
    valSecure+=3;
  }

  if (obj.value.match(/(\d.*\D)|(\D.*\d)/)) {
    valSecure+=3;
  }

  
  if (obj.value.length ==0) {
    valSecure = 0;
  } else if (obj.value.length <= 4) {
    valSecure=1;
  } else if (obj.value.length <= 7) {
    valSecure=2;
  } else {
    valSecure += 4;
  }

  getBar(valSecure);
}

function getBar(refColor) {

  var rgbColor = new Array();
  rgbColor = getColor(refColor);

  document.getElementById("passwd_lv").style.backgroundColor = "rgb("+ rgbColor[0] +"," + rgbColor[1] + "," + composante3 + ")";
  document.getElementById("passwd_lv").style.width = Math.round(100*refColor/coefColor) + "%";
  document.getElementById("passwd_lv_txt").innerHTML = secureMsg[Math.ceil(refColor/coefColor*(secureMsg.length-1))];
  document.getElementById("passwd_lv_txt").style.color = "rgb("+ rgbColor[0] +"," + rgbColor[1] + "," + composante3 + ")";
  document.getElementById("block_err3_NEWpsswuser_account_pssw").style.display = "block";
}

function getColor(coef) {

  var diffr = 0;
  var diffg = 0;
  var tabResult = new Array(0,0);
  var coefV1 = 1;
  var coefV2 = 1;

  if (endColor[0]>=initColor[0]) {
    diffr = endColor[0] - initColor[0];
    coefV1 = 1;
  } else {
    diffr = initColor[0] - endColor[0];
    coefV1 = -1;
  }

  if (endColor[1]>=initColor[1]) {
    diffg = endColor[1] - initColor[1];
    coefV2 = 1;
  } else {
    diffg = initColor[1] - endColor[1];
    coefV2 = -1;
  }

  var diffTotal = diffr + diffg;
  var v1 = diffTotal*coef/coefColor;


  if (initColor[0]>endColor[0]) {

    if (v1<=diffr) {
      tabResult[0] = Math.round(initColor[0]);
      tabResult[1] = Math.round(initColor[1]-(v1*coefV1));
    } else {
      tabResult[0] = Math.round(initColor[0]-(v1*coefV2)+(diffr*coefV2));
      tabResult[1] = Math.round(endColor[1]);
    }

  } else {

    if (v1<=diffr) {
      tabResult[0] = Math.round(initColor[0]+(v1*coefV1));
      tabResult[1] = Math.round(initColor[1]);
    } else {
      tabResult[0] = Math.round(endColor[0]);
      tabResult[1] = Math.round(initColor[1]+(v1*coefV2)-(diffr*coefV2));
    }

  }

  return tabResult;
}


