<?php 
$dossier_cache = '../cache/'; 
$secondes_cache = 60*60*12; // 12 heures 
  
$url_cache = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; 
$fichier_cache = $dossier_cache . md5($url_cache) . '.cache'; 
  
$fichier_cache_existe = ( @file_exists($fichier_cache) ) ? @filemtime($fichier_cache) : 0; 
  
if ($fichier_cache_existe > time() - $secondes_cache ) { 
  @readfile($fichier_cache); 
  exit(); 
  } 
ob_start(); 
?>