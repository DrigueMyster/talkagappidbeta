<?php
class core_Routines {   
    
function sendMessage($text,$Recipient_ID_sys_users,$t_notif){
 
                                            $inst_sys = new SYS();
                                            $TokenSender='';
                                            $TokenRecipient='';        
                                       
                                            $TokenRecipient = $inst_sys->READ_SYS_process_by_TYPEprocess('TOKEN_NOTIF',$Recipient_ID_sys_users);
		//Titre de la notification à envoyer 
		$heading = array(
			"en" => "".$text.""
		);

		//Contenu de la notification
		$content = array(
			"en" => ''.constant('core_path::NOTIFY_' .$t_notif. '') .'',
			);

		$fields = array(
			//Id du projet créer sur OneSignal
                                                                 'android_channel_id' => "".CHANNEL_ID."",
			'app_id' => "".NOTIF_APPSID."",
                                                              
			//Token de l'utilisateur récepteur de la notification
			//Ce token est sauvegardé dans notre base de données
			//On le récupère puis le met dans l'array ci-dessous
			//Les deux que j'ai mis ici sont pour moi (Le 1er) et pour toi (le second)
			'include_player_ids' => array("".$TokenRecipient.""),

			//Méta données associées à la notification avec des clé et des values ["cle" => "valeur"]
			'data' => array("cle" => "valeur", "cle1" => "valeur1"),
			'contents' => $content,
			'headings' => $heading,
			//Nom du logo que j'ai défini dans le projet ANDROID
			'small_icon' => "talk_ag_logo",
			'large_icon' => "talk_ag_logo",
			//Lien de redirection lors du click sur la notification
			//Ce lien represente la variable $lien qui est plus haut
			//"url"  => "".$lnk_button."",
		);
		
		$fields = json_encode($fields);
    	//print("\nJSON sent:\n");
    	//print($fields);
               //echo 'testnotif-'.constant('core_path::NOTIFY_' .$t_notif. '').'---'.$text;

    	$restApiKey = "".NOTIF_APIKEY."";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic '.$restApiKey));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		//Envoie de la notification 
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
}
        
function statcont2b($DATA_sys,$dataTimes,$ID_sys_users){
   //  ini_set('display_errors','off');
    
           $TotalView= 0;
           $where='';
           $DataSet='';
           $inst_fuser= new modules_Req_fuser();
           $inst_routines = new core_Routines();
           $inst_Query = new core_Query();
            
            if($_GET['DateStart'] == $_GET['DateEnd']){
                      $where =  ' AND  `'.SYS_PROCESS.'`.`date_creation`  = "'.$_GET['DateStart'].'  " ';
            }elseif($_GET['DateStart'] != $_GET['DateEnd']){
                                 $where =  ' AND  `'.SYS_PROCESS.'`.`date_creation`  >= "'.$_GET['DateStart'].'"  AND  `'.SYS_PROCESS.'`.`date_creation`  <= "'.$_GET['DateEnd'].'    " ';
             }
            
            if($dataTimes['type'] == 'days' and $dataTimes['value'] == 1 ){
     
                $gridcontainer2= "['00h', '02h', '04h', '06h','08h','10h','12h','14h','16h','18h','20h','23h59']";
                
            }else if(($dataTimes['type'] == 'days' and $dataTimes['value'] > 1 and $dataTimes['value'] < 31 ) or ($dataTimes['type'] == 'months' and $dataTimes['value'] == 1 ) ){
                          $gridcontainer2 = "[";
                          $dates=$inst_routines->getDatesFromRange($_GET['DateStart'], $_GET['DateEnd']);
                          foreach($dates as $datemap){
                                           $expdate = explode('-',$datemap);
                                           $datemap = $expdate[2].'/'.$expdate[1];
                                           $gridcontainer2 .=" '$datemap',";
                          }
                          $gridcontainer2 =substr($gridcontainer2, 0, -1);
                          $gridcontainer2 .= "]";

           }else if($dataTimes['type'] == 'months' and $dataTimes['value'] > 1  ){


           }
           
           if($DATA_sys['ID_sys_users'] != $ID_sys_users){
                $MyId = ' `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'" AND ';
           }else{
               $MyId='';
           }
            
           $DATA_user = $inst_fuser->READ_fuser_by_ID_sys_users($ID_sys_users);
           $where1 = '  `'.SYS_QHMNETWORK.'`.`ID_sys_qhmnetwork`  =   `'.SYS_PROCESS.'`.`VARrQ`  AND  LEFT(`'.SYS_PROCESS.'`.`TYPE_process`,8) = "READFLX_"  AND';       
           $where2 = 'AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND   `'.SYS_QHMNETWORK.'`.`type` != "answer" ';

           $Maps        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', '`'.SYS_PROCESS.'`.`ID_sys_users`','  '.$where1.'  '.$MyId.'   `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"    '.$where2.'  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                
           foreach ( $Maps as $data) {
              // ECHO 'TEST'.$data['ID_sys_users'];


                                $DataSet .= "{";
                                $DataSet .= "name: '".$DATA_user['users_nom']." -".$DATA_user['users_prenom']."',";
                                $DataSet .="data: [";

                               if($dataTimes['type'] == 'days' and $dataTimes['value'] == 1 ){
                                           $maps00h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','  '.$where1.'   '.$MyId.'   `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2`  AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "00:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "02:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'   GROUP BY   `'.SYS_PROCESS.'`.`ID_sys_users`');                                
                                           $datamaps00h['maptime']=0;
                                           foreach ( $maps00h as $datamaps00h) {  }
                                           if(empty($datamaps00h['maptime'])){
                                                    $DataSet .= "0, ";  
                                                    $TotalView = $TotalView + 0;
                                           }else{
                                                    $DataSet .= $datamaps00h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps00h['maptime'];
                                           }
                                
                                           $maps00h->closeCursor();

                                          $maps02h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','  '.$where1.'   '.$MyId.'    `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2`  AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"   AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "02:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "04:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                
                                          $datamaps02h['maptime']=0;
                                          foreach ( $maps02h as $datamaps02h) {  }
                                           if(empty($datamaps02h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps02h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps02h['maptime'];
                                           }
                                        
                                           $maps02h->closeCursor();

                                           $maps04h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','    '.$where1.'    '.$MyId.'    `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"     AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "04:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "06:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                
                                           $datamaps04h['maptime']=0;
                                           foreach ( $maps04h as $datamaps04h) {  }
                                           if(empty($datamaps04h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps04h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps04h['maptime'];
                                           }
                                        
                                           $maps04h->closeCursor();

                                           $maps06h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'    '.$MyId.'    `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND  `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"     AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "06:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "08:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                
                                           $datamaps06h['maptime']=0;
                                           foreach ( $maps06h as $datamaps06h) {  }
                                           if(empty($datamaps06h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps06h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps06h['maptime'];
                                           }
                                          
                                           $maps06h->closeCursor();

                                           $maps08h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','    '.$where1.'    '.$MyId.'    `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"     AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "08:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "10:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                    
                                          $datamaps08h['maptime']=0;
                                           foreach ( $maps08h as $datamaps08h) {  }
                                           if(empty($datamaps08h['maptime'])){
                                                   $DataSet .= "0, ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps08h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps08h['maptime'];
                                           }
                                           $maps08h->closeCursor();

                                           $maps10h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   '.$MyId.'    `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "10:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "12:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                    
                                           $datamaps10h['maptime']=0;
                                           foreach ( $maps10h as $datamaps10h) {  }
                                           if(empty($datamaps10h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps10h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps10h['maptime'];
                                           }  
                                          
                                           $maps10h->closeCursor();

                                           $maps12h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','    '.$where1.'    '.$MyId.'   `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "12:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "14:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                      
                                           $datamaps12h['maptime']=0;
                                           foreach ( $maps12h as $datamaps12h) {  }
                                           if(empty($datamaps12h['maptime'])){
                                                   $DataSet .= "0, ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps12h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps12h['maptime'];
                                           }    
                                           
                                           $maps12h->closeCursor();

                                           $maps14h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'    '.$MyId.'   `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"     AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "14:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "16:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                       
                                           $datamaps14h['maptime']=0;
                                           foreach ( $maps14h as $datamaps14h) {  }
                                           if(empty($datamaps14h['maptime'])){
                                                   $DataSet .= "0, ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps14h['maptime'].", ";  
                                                     $TotalView = $TotalView + $datamaps14h['maptime'];
                                           }  
                                         
                                           $maps14h->closeCursor();

                                           $maps16h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'    '.$MyId.'    `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"     AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "16:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "18:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                    
                                           $datamaps16h['maptime']=0;
                                           foreach ( $maps16h as $datamaps16h) {  }
                                           if(empty($datamaps16h['maptime'])){
                                                   $DataSet .= "0, ";  
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps16h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps16h['maptime'];
                                           }  
                                          
                                           $maps16h->closeCursor();

                                           $maps18h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'    '.$MyId.'   `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND  `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "18:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "20:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                     
                                           $datamaps18h['maptime']=0;
                                           foreach ( $maps18h as $datamaps18h) {  }
                                           if(empty($datamaps18h['maptime'])){
                                                   $DataSet .= "0, ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps18h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps18h['maptime'];
                                           }  
                                          
                                           $maps18h->closeCursor();

                                           $maps20h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   '.$MyId.'    `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"      AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "20:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "22:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                        
                                           $datamaps20h['maptime']=0;
                                           foreach ( $maps20h as $datamaps20h) {  }
                                           if(empty($datamaps20h['maptime'])){
                                                   $DataSet .= "0, ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps20h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps20h['maptime'];
                                           } 
                                          
                                           $maps20h->closeCursor();

                                           $maps22h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   '.$MyId.'    `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"      AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "22:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "23:59"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                    
                                           $datamaps22h['maptime']=0;
                                           foreach ( $maps22h as $datamaps22h) {  }
                                           if(empty($datamaps22h['maptime'])){
                                                   $DataSet .= "0 ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps22h['maptime']."";  
                                                    $TotalView = $TotalView + $datamaps22h['maptime'];
                                           } 
                                          
                                           $maps22h->closeCursor();

                                 }else if(($dataTimes['type'] == 'days' and $dataTimes['value'] > 1 and $dataTimes['value'] < 31 ) or ($dataTimes['type'] == 'months' and $dataTimes['value'] == 1 ) ){
                                           $dateReq=$inst_routines->getDatesFromRange($_GET['DateStart'], $_GET['DateEnd']);
                                           $datamaps22h['maptime']=0;
                                           foreach($dateReq as $datemapreq){
                                                                              $maps22h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'    '.$MyId.'    `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND `'.SYS_PROCESS.'`.`ID_sys_users`  = "'.$ID_sys_users.'"      AND  `'.SYS_PROCESS.'`.`date_creation`  = "'.$datemapreq.'"   AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`ID_sys_users` ');                                    
                                                                               foreach ( $maps22h as $datamaps22h) {  }
                                                                               if(empty($datamaps22h['maptime'])){
                                                                                      $DataSet .= "0,";     
                                                                                        $TotalView = $TotalView + 0;
                                                                               }else{
                                                                                      $DataSet .= $datamaps22h['maptime'].",";  
                                                                                        $TotalView = $TotalView + $datamaps22h['maptime'];
                                                                               } 
                                                                             
                                                                               $maps22h->closeCursor();
                                            }
                                           $DataSet =substr($DataSet, 0, -1);

                                 }

                               $DataSet .="]"; 
                      $DataSet .="},";
             }
             
             $DataSet =substr($DataSet, 0, -1);
        
             $datacont2['DataSet']        = $DataSet;
             $datacont2['TotalView']    = $TotalView;
             $datacont2['gridcontainer2']    =  $gridcontainer2;
             return $datacont2;       
             $Maps->closeCursor();
    
}
    
    
function statcont6($DATA_sys){
    //  ini_set('display_errors','off');
            $TotalView= 0;
            $where='';
            $inst_sys = new SYS();
            $inst_Query = new core_Query();
            $datacont2='';
            
           if($_GET['DateStart'] == $_GET['DateEnd']){
                      $where =  ' AND  `'.SYS_PROCESS.'`.`date_creation`  = "'.$_GET['DateStart'].'  " ';
            }elseif($_GET['DateStart'] != $_GET['DateEnd']){
                      $where =  ' AND  `'.SYS_PROCESS.'`.`date_creation`  >= "'.$_GET['DateStart'].'"  AND  `'.SYS_PROCESS.'`.`date_creation`  <= "'.$_GET['DateEnd'].'    " ';
            }
            
           $where1 = '  `'.SYS_QHMNETWORK.'`.`ID_sys_qhmnetwork`  =   `'.SYS_PROCESS.'`.`VARrQ`  AND  LEFT(`'.SYS_PROCESS.'`.`TYPE_process`,8) = "READFLX_"  AND';       
           $where2 = 'AND   `'.SYS_QHMNETWORK.'`.`type` != "answer" ';

/* ['iPhone', 58],
            ['Android', 13],
            ['Windows', 13],
            ['Mac', 3],
            ['Linux', 42],*/
           $DataSet='';
           
           $Maps   = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.SYS_PROCESS.'`', ' COUNT(*) AS OS,`'.SYS_PROCESS.'`.`OS_log`','  '.$where1.'  `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   '.$where2.'  '.$where.'  GROUP BY `'.SYS_PROCESS.'`.`OS_log`   ');                                
           foreach ( $Maps as $data) {
            
                   $DataSet .= "['".$data['OS_log']."', ".$data['OS']."],";
                  
            }
            
             $datacont2   = $DataSet;
           
            $Maps->closeCursor();
             return $datacont2;      
 }
 
function statcont8($DATA_sys){
 //   ini_set('display_errors','off');
            $TotalView= 0;
            $where='';
            $inst_fuser= new modules_Req_fuser();
            $inst_sys = new SYS();
            $inst_Query = new core_Query();
            $DATA_user = $inst_fuser->READ_fuser_by_ID_sys_users($DATA_sys['ID_sys_users']  );
            
            if($_GET['DateStart'] == $_GET['DateEnd']){
                      $where =  ' AND  `'.SYS_PROCESS.'`.`date_creation`  = "'.$_GET['DateStart'].'  " ';
            }elseif($_GET['DateStart'] != $_GET['DateEnd']){
                                 $where =  ' AND  `'.SYS_PROCESS.'`.`date_creation`  >= "'.$_GET['DateStart'].'"  AND  `'.SYS_PROCESS.'`.`date_creation`  <= "'.$_GET['DateEnd'].'    " ';
             }
             
             $clouditeltems ='';
                                $items = $inst_sys->READ_SYS_by_SYS_items(0,'',$DATA_user['lg']) ;  
                                  foreach ($items as $DATA_items){ 
                                     
                                           $items2='';
                                           $items = ' AND (';
                                          $exp2='';
                                           $e=0;
                                           $i=0;
                                          $exp2 = explode(' ',$DATA_items['options_label']);
                                                                               
                                           if(count($exp2) > 2){
                                                     $items .=  '  `'.SYS_QHMNETWORK.'`.`content`   LIKE "% #'.str_replace(' ','_',$DATA_items['options_label']).' %" OR  `'.SYS_QHMNETWORK.'`.`title`   LIKE "% #'.str_replace(' ','_',$DATA_items['options_label']).' %" OR ';
                                                      for($e = 0; ; $e++){
                                                                 if ($e > count($exp2)) {  break;  }
                                                                            if(!empty($exp2[$e])){
                                                                              
                                                                               // echo 'test-'.strlen($exp2[$e]);
                                                                                      if(strlen($exp2[$e]) > 2){
                                                                                         //   echo '['.$exp2[$e].']['.strlen($exp2[$e]).']-';
                                                                                                $items .=  '  `'.SYS_QHMNETWORK.'`.`content`   LIKE "% '.$exp2[$e].' %" OR  `'.SYS_QHMNETWORK.'`.`title`   LIKE "% '.$exp2[$e].' %" OR `'.SYS_QHMNETWORK.'`.`content`   LIKE "%#'.$exp2[$e].'%" OR  `'.SYS_QHMNETWORK.'`.`title`   LIKE "%#'.$exp2[$e].'%" OR ';
                                                                                                               
                                                                                      }else{
                                                                                          
                                                                                      }
                                                                          }
                                                                          $i ++;
                                                      }
                                            }else{
                                                      $items2 =  ' AND `'.SYS_QHMNETWORK.'`.`content`   LIKE "%#'.str_replace(' ','_',$DATA_items['options_label']).'%" OR  `'.SYS_QHMNETWORK.'`.`title`   LIKE "%#'.str_replace(' ','_',$DATA_items['options_label']).'%"   ';
                                            } 
                  
                                             if($i > 0){
                                                        $items2 = substr($items, 0, -3);
                                                        $items2 .= ')';
                                            }
                                            
                                      
		$Maps        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.SYS_PROCESS.'`', ' `'.SYS_QHMNETWORK.'`.`ID_sys_qhmnetwork`','  `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"  AND  `'.SYS_QHMNETWORK.'`.`ID_sys_qhmnetwork`  =   `'.SYS_PROCESS.'`.`VARrQ`  AND  LEFT(`'.SYS_PROCESS.'`.`TYPE_process`,8) = "READFLX_"   '.$where.' '.$items2.'  ');
		foreach ( $Maps as $data) { 
                                              if($data['ID_sys_qhmnetwork'] > 0){  $clouditeltems .= '#'.str_replace(' ','',str_replace('/',' ',$DATA_items['options_label'])).','; }
                                              
                                          }
                                          
                                        
                  }
          //echo '--['.$clouditeltems.']';
             $datacont2['DataSet']        = substr($clouditeltems, 0, -1);
            
            $Maps->closeCursor();
             return $datacont2;    
}    
    
function statcont3($DATA_sys,$dataTimes){
           $TotalView= 0;
            $where='';
           $DataSet='';

            $inst_routines = new core_Routines();
            $inst_Query = new core_Query();
            
            if($_GET['DateStart'] == $_GET['DateEnd']){
                      $where =  ' AND  `'.SYS_PROCESS.'`.`date_creation`  = "'.$_GET['DateStart'].'  " ';
            }elseif($_GET['DateStart'] != $_GET['DateEnd']){
                                 $where =  ' AND  `'.SYS_PROCESS.'`.`date_creation`  >= "'.$_GET['DateStart'].'"  AND  `'.SYS_PROCESS.'`.`date_creation`  <= "'.$_GET['DateEnd'].'    " ';
             }
            
            if($dataTimes['type'] == 'days' and $dataTimes['value'] == 1 ){
     
                $gridcontainer2= "['00h', '02h', '04h', '06h','08h','10h','12h','14h','16h','18h','20h','23h59']";
                
            }else if(($dataTimes['type'] == 'days' and $dataTimes['value'] > 1 and $dataTimes['value'] < 31 ) or ($dataTimes['type'] == 'months' and $dataTimes['value'] == 1 ) ){
                          $gridcontainer2 = "[";
                          $dates=$inst_routines->getDatesFromRange($_GET['DateStart'], $_GET['DateEnd']);
                          foreach($dates as $datemap){
                                           $expdate = explode('-',$datemap);
                                           $datemap = $expdate[2].'/'.$expdate[1];
                                           $gridcontainer2 .=" '$datemap',";
                          }
                          $gridcontainer2 =substr($gridcontainer2, 0, -1);
                          $gridcontainer2 .= "]";

           }else if($dataTimes['type'] == 'months' and $dataTimes['value'] > 1  ){


           }
            
 
           $where1 = '  `'.SYS_QHMNETWORK.'`.`ID_sys_qhmnetwork`  =   `'.SYS_PROCESS.'`.`VARrQ`  AND  LEFT(`'.SYS_PROCESS.'`.`TYPE_process`,13) = "PUBLISH_FAVO_"  AND';       
           $where2 = 'AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` ';

           $Maps        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', '`'.SYS_GEO.'`.`ID_geo`,`'.SYS_GEO.'`.`country`,`'.SYS_GEO.'`.`iso2`','  '.$where1.'  `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"  AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"   '.$where2.'  '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                
           foreach ( $Maps as $data) {


                                $DataSet .= "{";
                                $DataSet .= "name: '".$data['iso2']." -".$data['country']."',";
                                $DataSet .="data: [";

                               if($dataTimes['type'] == 'days' and $dataTimes['value'] == 1 ){
                                           $maps00h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','  '.$where1.'  `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'" AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "00:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "02:00"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                
                                           $datamaps00h['maptime']=0;
                                           foreach ( $maps00h as $datamaps00h) {  }
                                           if(empty($datamaps00h['maptime'])){
                                                    $TotalView = $TotalView + 0;
                                                    $DataSet .= "0, ";    
                                           }else{
                                                    $DataSet .= $datamaps00h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps00h['maptime'];
                                           }
                                          
                                           $maps00h->closeCursor();

                                          $maps02h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','  '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'" AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2`  AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "02:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "04:00"   '.$where.' GROUP BY   `'.SYS_GEO.'`.`ID_geo`');                                
                                          $datamaps02h['maptime'] = 0;
                                          foreach ( $maps02h as $datamaps02h) {  }
                                           if(empty($datamaps02h['maptime'])){
                                                   $DataSet .= "0, ";  
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps02h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps02h['maptime'];
                                           }
                                          
                                           $maps02h->closeCursor();

                                           $maps04h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','    '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'" AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "04:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "06:00"   '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                
                                          $datamaps04h['maptime'] =0;
                                           foreach ( $maps04h as $datamaps04h) {  }
                                           if(empty($datamaps04h['maptime'])){
                                                    $TotalView = $TotalView + 0;
                                                    $DataSet .= "0, ";    
                                           }else{
                                                   $DataSet .= $datamaps04h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps04h['maptime'];
                                           }
                                           
                                           $maps04h->closeCursor();

                                           $maps06h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'" AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND  `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "06:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "08:00"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                
                                           $datamaps06h['maptime'] = 0;
                                           foreach ( $maps06h as $datamaps06h) {  }
                                           if(empty($datamaps06h['maptime'])){
                                                   $DataSet .= "0, ";  
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps06h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps06h['maptime'];
                                           }
                                          
                                           $maps06h->closeCursor();

                                           $maps08h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','    '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"  AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"  AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "08:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "10:00"    '.$where.' GROUP BY   `'.SYS_GEO.'`.`ID_geo`');                                    
                                           $datamaps08h['maptime'] = 0;
                                           foreach ( $maps08h as $datamaps08h) {  }
                                           if(empty($datamaps08h['maptime'])){
                                                   $DataSet .= "0, ";  
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps08h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps08h['maptime'];
                                           }
                                           $maps08h->closeCursor();

                                           $maps10h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"  AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"  AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "10:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "12:00"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                    
                                           $datamaps10h['maptime'] = 0;
                                           foreach ( $maps10h as $datamaps10h) {  }
                                           if(empty($datamaps10h['maptime'])){
                                                   $DataSet .= "0, "; 
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps10h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps10h['maptime'];
                                           }  
                                           $maps10h->closeCursor();

                                           $maps12h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','    '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'" AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "12:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "14:00"    '.$where.' GROUP BY   `'.SYS_GEO.'`.`ID_geo`');                                      
                                           $datamaps12h['maptime'] =0;
                                           foreach ( $maps12h as $datamaps12h) {  }
                                           if(empty($datamaps12h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps12h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps12h['maptime'];
                                           }    
                                           $maps12h->closeCursor();

                                           $maps14h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'" AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "14:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "16:00"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                       
                                          $datamaps14h['maptime'] = 0;
                                           foreach ( $maps14h as $datamaps14h) {  }
                                           if(empty($datamaps14h['maptime'])){
                                                   $DataSet .= "0, ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps14h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps14h['maptime'];
                                           } 
                                           $maps14h->closeCursor();

                                           $maps16h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"  AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"  AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "16:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "18:00"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                    
                                           $datamaps16h['maptime'] = 0;
                                           foreach ( $maps16h as $datamaps16h) {  }
                                           if(empty($datamaps16h['maptime'])){
                                                   $DataSet .= "0, ";
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps16h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps16h['maptime'];
                                           }  
                                           $maps16h->closeCursor();

                                           $maps18h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'" AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND  `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "18:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "20:00"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                     
                                          $datamaps18h['maptime']=0;
                                           foreach ( $maps18h as $datamaps18h) {  }
                                           if(empty($datamaps18h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps18h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps18h['maptime'];
                                           }  
                                           $maps18h->closeCursor();

                                           $maps20h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'" AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "20:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "22:00"    '.$where.' GROUP BY   `'.SYS_GEO.'`.`ID_geo`');                                        
                                          $datamaps20h['maptime'] = 0;
                                           foreach ( $maps20h as $datamaps20h) {  }
                                           if(empty($datamaps20h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps20h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps20h['maptime'];
                                           } 
                                           $maps20h->closeCursor();

                                           $maps22h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"  AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"  AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "22:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "23:59"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                    
                                           $datamaps22h['maptime']=0;
                                           foreach ( $maps22h as $datamaps22h) {  }
                                           if(empty($datamaps22h['maptime'])){
                                                   $DataSet .= "0 ";    
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps22h['maptime']."";  
                                                   $TotalView = $TotalView + $datamaps22h['maptime'];
                                           } 
                                           $maps22h->closeCursor();

                                 }else if(($dataTimes['type'] == 'days' and $dataTimes['value'] > 1 and $dataTimes['value'] < 31 ) or ($dataTimes['type'] == 'months' and $dataTimes['value'] == 1 ) ){
                                           $dateReq=$inst_routines->getDatesFromRange($_GET['DateStart'], $_GET['DateEnd']);
                                           foreach($dateReq as $datemapreq){
                                                                              $datamaps22h['maptime']= 0;
                                                                              $maps22h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'" AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  `'.SYS_PROCESS.'`.`date_creation`  = "'.$datemapreq.'"     '.$where.'  GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                    
                                                                               foreach ( $maps22h as $datamaps22h) {  }
                                                                               if(empty($datamaps22h['maptime'])){
                                                                                      $DataSet .= "0,";     
                                                                               }else{
                                                                                      $DataSet .= $datamaps22h['maptime'].",";  
                                                                               } 
                                                                               $TotalView = $TotalView + $datamaps22h['maptime'];
                                                                               $maps22h->closeCursor();
                                            }
                                           $DataSet =substr($DataSet, 0, -1);

                                 }

                               $DataSet .="]"; 
                      $DataSet .="},";
             }
             
             $DataSet =substr($DataSet, 0, -1);
  
             $datacont2['DataSet']        = $DataSet;
             $datacont2['TotalView']    = $TotalView;
             $datacont2['gridcontainer2']    =  $gridcontainer2;
             return $datacont2;       
             $Maps->closeCursor();
    
}    
    
function statcont4($DATA_sys,$dataTimes){
            $TotalView= 0;
            $where='';
            $DataSet='';
            $gridcontainer2='';
            $inst_routines = new core_Routines();
            $inst_Query = new core_Query();
            
            if($_GET['DateStart'] == $_GET['DateEnd']){
                      $where =  ' AND  `'.SYS_NOTIFY.'`.`date_send`  = "'.$_GET['DateStart'].'  " ';
            }elseif($_GET['DateStart'] != $_GET['DateEnd']){
                                 $where =  ' AND  `'.SYS_NOTIFY.'`.`date_send`  >= "'.$_GET['DateStart'].'"  AND  `'.SYS_NOTIFY.'`.`date_send`  <= "'.$_GET['DateEnd'].'    " ';
             }
            
            if($dataTimes['type'] == 'days' and $dataTimes['value'] == 1 ){
     
                         $gridcontainer2= "['00h', '02h', '04h', '06h','08h','10h','12h','14h','16h','18h','20h','23h59']";
                
            }else if(($dataTimes['type'] == 'days' and $dataTimes['value'] > 1 and $dataTimes['value'] < 31 ) or ($dataTimes['type'] == 'months' and $dataTimes['value'] == 1 ) ){
                          $gridcontainer2 = "[";
                          $dates=$inst_routines->getDatesFromRange($_GET['DateStart'], $_GET['DateEnd']);
                          foreach($dates as $datemap){
                                           $expdate = explode('-',$datemap);
                                           $datemap = $expdate[2].'/'.$expdate[1];
                                           $gridcontainer2 .=" '$datemap',";
                          }
                          $gridcontainer2 =substr($gridcontainer2, 0, -1);
                          $gridcontainer2 .= "]";

           }else if($dataTimes['type'] == 'months' and $dataTimes['value'] > 1  ){


           }
            
     
           $where2 = '  `'.SYS_NOTIFY.'`.`t_notif`  =  "answer_flx"      AND `'.INTRA_STE.'`.`ID_ste` =   `'.SYS_NOTIFY.'`.`Sender_ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND   `'.SYS_NOTIFY.'`.`Recipient_ID_sys_users` = "'.$DATA_sys['ID_sys_users'].'"  ';

           $Maps        = $inst_Query->read_Query('`'.INTRA_STE.'`,`'.SYS_NOTIFY.'`,`'.SYS_GEO.'`', '`'.SYS_GEO.'`.`ID_geo`,`'.SYS_GEO.'`.`country`,`'.SYS_GEO.'`.`iso2`','   '.$where2.'  '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                
           foreach ( $Maps as $data) {


                                $DataSet .= "{";
                                $DataSet .= "name: '".$data['iso2']." -".$data['country']."',";
                                $DataSet .="data: [";

                               if($dataTimes['type'] == 'days' and $dataTimes['value'] == 1 ){
                                           $maps00h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`', 'COUNT(*) AS maptime','  '.$where2.' AND   `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  >= "00:00"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  < "02:00"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                
                                           $datamaps00h['maptime']=0;
                                           foreach ( $maps00h as $datamaps00h) {  }
                                           if(empty($datamaps00h['maptime'])){
                                                    $DataSet .= "0, ";    
                                                    $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps00h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps00h['maptime'];
                                           }
                                           $maps00h->closeCursor();

                                          $maps02h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`', 'COUNT(*) AS maptime','   '.$where2.' AND   `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  >= "02:00"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  < "04:00"    '.$where.' GROUP BY   `'.SYS_GEO.'`.`ID_geo`');                                
                                          $datamaps02h['maptime']=0; 
                                          foreach ( $maps02h as $datamaps02h) {  }
                                           if(empty($datamaps02h['maptime'])){
                                                    $DataSet .= "0, ";    
                                                    $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps02h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps02h['maptime'];
                                           }
                                           $maps02h->closeCursor();

                                           $maps04h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`', 'COUNT(*) AS maptime','   '.$where2.'  AND `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  >= "04:00"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  < "06:00"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                
                                           $datamaps04h['maptime']=0; 
                                           foreach ( $maps04h as $datamaps04h) {  }
                                           if(empty($datamaps04h['maptime'])){
                                                     $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps04h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps04h['maptime'];
                                           }
                                           $maps04h->closeCursor();

                                           $maps06h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`','COUNT(*) AS maptime','   '.$where2.'  AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  >= "06:00"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  < "08:00"   '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                
                                           $datamaps06h['maptime']=0; 
                                           foreach ( $maps06h as $datamaps06h) {  }
                                           if(empty($datamaps06h['maptime'])){
                                                     $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps06h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps06h['maptime'];
                                           }
                                           $maps06h->closeCursor();

                                           $maps08h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`', 'COUNT(*) AS maptime','  '.$where2.'  AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  >= "08:00"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  < "10:00"    '.$where.' GROUP BY   `'.SYS_GEO.'`.`ID_geo`');                                    
                                           $datamaps08h['maptime']=0; 
                                           foreach ( $maps08h as $datamaps08h) {  }
                                           if(empty($datamaps08h['maptime'])){
                                                     $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps08h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps08h['maptime'];
                                           }
                                           $maps08h->closeCursor();

                                           $maps10h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`','COUNT(*) AS maptime','  '.$where2.'  AND `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  >= "10:00"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  < "12:00"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                    
                                           $datamaps10h['maptime']=0; 
                                           foreach ( $maps10h as $datamaps10h) {  }
                                           if(empty($datamaps10h['maptime'])){
                                                     $DataSet .= "0, ";  
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps10h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps10h['maptime'];
                                           }  
                                           $maps10h->closeCursor();

                                           $maps12h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`', 'COUNT(*) AS maptime','     '.$where2.'  AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  >= "12:00"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  < "14:00"    '.$where.' GROUP BY   `'.SYS_GEO.'`.`ID_geo`');                                      
                                          $datamaps12h['maptime']=0; 
                                           foreach ( $maps12h as $datamaps12h) {  }
                                           if(empty($datamaps12h['maptime'])){
                                                     $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps12h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps12h['maptime'];
                                           }  
                                           $maps12h->closeCursor();

                                           $maps14h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`', 'COUNT(*) AS maptime','   '.$where2.'  AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  >= "14:00"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  < "16:00"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                       
                                          $datamaps14h['maptime']=0; 
                                           foreach ( $maps14h as $datamaps14h) {  }
                                           if(empty($datamaps14h['maptime'])){
                                                     $DataSet .= "0, ";  
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                     $DataSet .= $datamaps14h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps14h['maptime'];
                                           }  
                                           $maps14h->closeCursor();

                                           $maps16h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`', 'COUNT(*) AS maptime','   '.$where2.'  AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  >= "16:00"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  < "18:00"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                    
                                           $datamaps16h['maptime']=0; 
                                           foreach ( $maps16h as $datamaps16h) {  }
                                           if(empty($datamaps16h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps16h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps16h['maptime'];
                                           }  
                                           $maps16h->closeCursor();

                                           $maps18h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`', 'COUNT(*) AS maptime','   '.$where2.'  AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  >= "18:00"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  < "20:00"   '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                     
                                           $datamaps18h['maptime']=0; 
                                           foreach ( $maps18h as $datamaps18h) {  }
                                           if(empty($datamaps18h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps18h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps18h['maptime'];
                                           }  
                                           $maps18h->closeCursor();

                                           $maps20h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`', 'COUNT(*) AS maptime','   '.$where2.'  AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  >= "20:00"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  < "22:00"   '.$where.' GROUP BY   `'.SYS_GEO.'`.`ID_geo`');                                        
                                           $datamaps20h['maptime']=0; 
                                           foreach ( $maps20h as $datamaps20h) {  }
                                           if(empty($datamaps20h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps20h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps20h['maptime'];
                                           } 
                                           $maps20h->closeCursor();

                                           $maps22h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`', 'COUNT(*) AS maptime','   '.$where2.'  AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  >= "22:00"  AND  LEFT(`'.SYS_NOTIFY.'`.`time_send`,5)  < "23:59"    '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                    
                                            $datamaps22h['maptime']=0; 
                                           foreach ( $maps22h as $datamaps22h) {  }
                                           if(empty($datamaps22h['maptime'])){
                                                   $DataSet .= "0 ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps22h['maptime']."";  
                                                   $TotalView = $TotalView + $datamaps22h['maptime'];
                                           } 
                                           $maps22h->closeCursor();

                                 }else if(($dataTimes['type'] == 'days' and $dataTimes['value'] > 1 and $dataTimes['value'] < 31 ) or ($dataTimes['type'] == 'months' and $dataTimes['value'] == 1 ) ){
                                           $dateReq=$inst_routines->getDatesFromRange($_GET['DateStart'], $_GET['DateEnd']);
                                           foreach($dateReq as $datemapreq){
                                                                              $maps22h        = $inst_Query->read_Query('`'.SYS_NOTIFY.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`', 'COUNT(*) AS maptime','  '.$where2.'  AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  `'.SYS_NOTIFY.'`.`date_send`  = "'.$datemapreq.'"     '.$where.'  GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                    
                                                                               foreach ( $maps22h as $datamaps22h) {  }
                                                                               if(empty($datamaps22h['maptime'])){
                                                                                      $DataSet .= "0,";     
                                                                               }else{
                                                                                      $DataSet .= $datamaps22h['maptime'].",";  
                                                                               } 
                                                                               $TotalView = $TotalView + $datamaps22h['maptime'];
                                                                               $maps22h->closeCursor();
                                            }
                                           $DataSet =substr($DataSet, 0, -1);

                                 }

                               $DataSet .="]"; 
                      $DataSet .="},";
             }
             
             $DataSet =substr($DataSet, 0, -1);
 // echo 'test-'.$where;
             $datacont2['DataSet']        = $DataSet;
             $datacont2['TotalView']    = $TotalView;
             $datacont2['gridcontainer2']    =  $gridcontainer2;
             return $datacont2;       
             $Maps->closeCursor();
    
}
    

function statcont1($DATA_sys,$dataTimes){

            $inst_routines = new core_Routines();
            $inst_Query = new core_Query();
            
             if($_GET['DateStart'] == $_GET['DateEnd']){
                      $where =  ' AND  `'.SYS_QHMNETWORK.'`.`date_send`  = "'.$_GET['DateStart'].'  " ';
            }elseif($_GET['DateStart'] != $_GET['DateEnd']){
                                 $where =  ' AND  `'.SYS_QHMNETWORK.'`.`date_send`  >= "'.$_GET['DateStart'].'"  AND  `'.SYS_QHMNETWORK.'`.`date_send`  <= "'.$_GET['DateEnd'].'    " ';
             }
             
            if($dataTimes['type'] == 'days' and $dataTimes['value'] == 1 ){
     
                $gridcontainer2= "['00h', '02h', '04h', '06h','08h','10h','12h','14h','16h','18h','20h','23h59']";
                
            }else if(($dataTimes['type'] == 'days' and $dataTimes['value'] > 1 and $dataTimes['value'] < 31 ) or ($dataTimes['type'] == 'months' and $dataTimes['value'] == 1 ) ){
                          $gridcontainer2 = "[";
                          $dates=$inst_routines->getDatesFromRange($_GET['DateStart'], $_GET['DateEnd']);
                          foreach($dates as $datemap){
                                           $expdate = explode('-',$datemap);
                                           $datemap = $expdate[2].'/'.$expdate[1];
                                           $gridcontainer2 .=" '$datemap',";
                          }
                          $gridcontainer2 =substr($gridcontainer2, 0, -1);
                          $gridcontainer2 .= "]";

           }else if($dataTimes['type'] == 'months' and $dataTimes['value'] > 1  ){


           }
          
           $groupby = 'GROUP BY  `'.SYS_QHMNETWORK.'`.`statut`';
           
           $Maps        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', '`'.SYS_QHMNETWORK.'`.`statut`,`'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`','  `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"  '.$where.'   '.$groupby.' ');                                   
     
          
             foreach ( $Maps as $data) {
                
                     
                             $DataSet .= "{";
                                  $DataSet .= "name: '". constant('core_path::DataSetName_'.$data['statut'].'')."',";
                                $DataSet .="data: [";

                               if($dataTimes['type'] == 'days' and $dataTimes['value'] == 1 ){
                                
                                           $maps00h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND  `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  >= "00:00"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  < "02:00"    '.$where.'  '.$groupby.' ');                                
                                           $datamaps00h['maptime']=0;
                                           foreach ( $maps00h as $datamaps00h) {  }
                                           if(empty($datamaps00h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps00h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps00h['maptime'];
                                           }
                                           $maps00h->closeCursor();

                                          $maps02h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  >= "02:00"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  < "04:00"    '.$where.'   '.$groupby.' ');                                 
                                          $datamaps02h['maptime']=0; 
                                          foreach ( $maps02h as $datamaps02h) {  }
                                           if(empty($datamaps02h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps02h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps02h['maptime'];
                                           }
                                           $maps02h->closeCursor();

                                           $maps04h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"  AND   LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  >= "04:00"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  < "06:00"    '.$where.'   '.$groupby.' ');                                        
                                           $datamaps04h['maptime']=0; 
                                           foreach ( $maps04h as $datamaps04h) {  }
                                           if(empty($datamaps04h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps04h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps04h['maptime'];
                                           }
                                           
                                           $maps04h->closeCursor();

                                           $maps06h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  >= "06:00"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  < "08:00"    '.$where.'  '.$groupby.' ');                                        
                                           $datamaps06h['maptime']=0; 
                                           foreach ( $maps06h as $datamaps06h) {  }
                                           if(empty($datamaps06h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps06h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps06h['maptime'];
                                           }
                                           $maps06h->closeCursor();

                                           $maps08h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  >= "08:00"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  < "10:00"    '.$where.'   '.$groupby.' ');                                           
                                           $datamaps08h['maptime']=0; 
                                           foreach ( $maps08h as $datamaps08h) {  }
                                           if(empty($datamaps08h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps08h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps08h['maptime'];
                                           }
                                           $maps08h->closeCursor();

                                           $maps10h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  >= "10:00"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  < "12:00"   '.$where.'   '.$groupby.' ');                                                 
                                          $datamaps10h['maptime']=0; 
                                           foreach ( $maps10h as $datamaps10h) {  }
                                           if(empty($datamaps10h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps10h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps10h['maptime'];
                                           }  
                                           $maps10h->closeCursor();

                                           $maps12h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  >= "12:00"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  < "14:00"    '.$where.'   '.$groupby.' ');                                                 
                                          $datamaps12h['maptime']=0; 
                                           foreach ( $maps12h as $datamaps12h) {  }
                                           if(empty($datamaps12h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps12h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps12h['maptime'];
                                           }    
                                           $maps12h->closeCursor();

                                           $maps14h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  >= "14:00"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  < "16:00"    '.$where.'   '.$groupby.' ');                                                  
                                           $datamaps14h['maptime']=0; 
                                           foreach ( $maps14h as $datamaps14h) {  }
                                           if(empty($datamaps14h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                    $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps14h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps14h['maptime'];
                                           }  
                                           $maps14h->closeCursor();

                                           $maps16h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  >= "16:00"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  < "18:00"    '.$where.'    '.$groupby.' ');                                            
                                           $datamaps16h['maptime']=0; 
                                           foreach ( $maps16h as $datamaps16h) {  }
                                           if(empty($datamaps16h['maptime'])){
                                                   $DataSet .= "0, ";   
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps16h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps16h['maptime'];
                                           }  
                                           $maps16h->closeCursor();

                                           $maps18h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  >= "18:00"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  < "20:00"    '.$where.'   '.$groupby.' ');                                               
                                           $datamaps18h['maptime']=0; 
                                           foreach ( $maps18h as $datamaps18h) {  }
                                           if(empty($datamaps18h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps18h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps18h['maptime'];
                                           }  
                                           $maps18h->closeCursor();

                                           $maps20h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  >= "20:00"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  < "22:00"   '.$where.'   '.$groupby.' ');                                          
                                           $datamaps20h['maptime']=0; 
                                           foreach ( $maps20h as $datamaps20h) {  }
                                           if(empty($datamaps20h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps20h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps20h['maptime'];
                                           } 
                                           $maps20h->closeCursor();

                                           $maps22h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  >= "22:00"  AND  LEFT(`'.SYS_QHMNETWORK.'`.`time_send`,5)  < "23:59"    '.$where.'   '.$groupby.' ');                                                      
                                           $datamaps22h['maptime']=0; 
                                           foreach ( $maps22h as $datamaps22h) {  }
                                           if(empty($datamaps22h['maptime'])){
                                                   $DataSet .= "0 ";  
                                                     $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps22h['maptime']."";  
                                                    $TotalView = $TotalView + $datamaps22h['maptime'];
                                           } 
                                           $maps22h->closeCursor();

                                 }else if(($dataTimes['type'] == 'days' and $dataTimes['value'] > 1 and $dataTimes['value'] < 31 ) or ($dataTimes['type'] == 'months' and $dataTimes['value'] == 1 ) ){
                                           $dateReq=$inst_routines->getDatesFromRange($_GET['DateStart'], $_GET['DateEnd']);
                                          $datamaps22h['maptime']=0;
                                           foreach($dateReq as $datemapreq){
                                                                              $maps22h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`', 'COUNT(*) AS maptime','    `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_QHMNETWORK.'`.`statut` ="'.$data['statut'].'"    AND  `'.SYS_QHMNETWORK.'`.`date_send`  = "'.$datemapreq.'"     '.$where.'   '.$groupby.' ');                                                     
                                                                               foreach ( $maps22h as $datamaps22h) {  }
                                                                               if(empty($datamaps22h['maptime'])){
                                                                                      $DataSet .= "0,";     
                                                                               }else{
                                                                                      $DataSet .= $datamaps22h['maptime'].",";  
                                                                               } 
                                                                               $TotalView = $TotalView + $datamaps22h['maptime'];
                                                                               $maps22h->closeCursor();
                                            }
                                           $DataSet =substr($DataSet, 0, -1);

                                 }

                               $DataSet .="]"; 
                      $DataSet .="},";  
                     
             }
          
             $DataSet =substr($DataSet, 0, -1);
 
             $datacont2['DataSet']        = $DataSet;
             $datacont2['TotalView']    = $TotalView;
             $datacont2['gridcontainer2']    =  $gridcontainer2;
             return $datacont2;      
             $Maps->closeCursor();
    
}

function statcont2($DATA_sys,$dataTimes){
           $TotalView= 0;
           $where='';
           $DataSet='';

            $inst_routines = new core_Routines();
            $inst_Query = new core_Query();
            
            if($_GET['DateStart'] == $_GET['DateEnd']){
                      $where =  ' AND  `'.SYS_PROCESS.'`.`date_creation`  = "'.$_GET['DateStart'].'  " ';
            }elseif($_GET['DateStart'] != $_GET['DateEnd']){
                                 $where =  ' AND  `'.SYS_PROCESS.'`.`date_creation`  >= "'.$_GET['DateStart'].'"  AND  `'.SYS_PROCESS.'`.`date_creation`  <= "'.$_GET['DateEnd'].'    " ';
             }
            
            if($dataTimes['type'] == 'days' and $dataTimes['value'] == 1 ){
     
                $gridcontainer2= "['00h', '02h', '04h', '06h','08h','10h','12h','14h','16h','18h','20h','23h59']";
                
            }else if(($dataTimes['type'] == 'days' and $dataTimes['value'] > 1 and $dataTimes['value'] < 31 ) or ($dataTimes['type'] == 'months' and $dataTimes['value'] == 1 ) ){
                          $gridcontainer2 = "[";
                          $dates=$inst_routines->getDatesFromRange($_GET['DateStart'], $_GET['DateEnd']);
                          foreach($dates as $datemap){
                                           $expdate = explode('-',$datemap);
                                           $datemap = $expdate[2].'/'.$expdate[1];
                                           $gridcontainer2 .=" '$datemap',";
                          }
                          $gridcontainer2 =substr($gridcontainer2, 0, -1);
                          $gridcontainer2 .= "]";

           }else if($dataTimes['type'] == 'months' and $dataTimes['value'] > 1  ){


           }
            
 
           $where1 = '  `'.SYS_QHMNETWORK.'`.`ID_sys_qhmnetwork`  =   `'.SYS_PROCESS.'`.`VARrQ`  AND  LEFT(`'.SYS_PROCESS.'`.`TYPE_process`,8) = "READFLX_"  AND';       
           $where2 = 'AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND   `'.SYS_QHMNETWORK.'`.`type` != "answer" ';

           $Maps        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', '`'.SYS_GEO.'`.`ID_geo`,`'.SYS_GEO.'`.`country`,`'.SYS_GEO.'`.`iso2`','  '.$where1.'  `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'" AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"    '.$where2.'  '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                
           foreach ( $Maps as $data) {


                                $DataSet .= "{";
                                $DataSet .= "name: '".$data['iso2']." -".$data['country']."',";
                                $DataSet .="data: [";

                               if($dataTimes['type'] == 'days' and $dataTimes['value'] == 1 ){
                                           $maps00h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','  '.$where1.'  `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"  AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "00:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "02:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                
                                           $datamaps00h['maptime']=0;
                                           foreach ( $maps00h as $datamaps00h) {  }
                                           if(empty($datamaps00h['maptime'])){
                                                    $DataSet .= "0, ";  
                                                    $TotalView = $TotalView + 0;
                                           }else{
                                                    $DataSet .= $datamaps00h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps00h['maptime'];
                                           }
                                      
                                           $maps00h->closeCursor();

                                          $maps02h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','  '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"  AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2`  AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "02:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "04:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY   `'.SYS_GEO.'`.`ID_geo`');                                
                                          $datamaps02h['maptime']=0;
                                          foreach ( $maps02h as $datamaps02h) {  }
                                           if(empty($datamaps02h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps02h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps02h['maptime'];
                                           }
                                        
                                           $maps02h->closeCursor();

                                           $maps04h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','    '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"  AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"    AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "04:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "06:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                
                                           $datamaps04h['maptime']=0;
                                           foreach ( $maps04h as $datamaps04h) {  }
                                           if(empty($datamaps04h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps04h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps04h['maptime'];
                                           }
                                        
                                           $maps04h->closeCursor();

                                           $maps06h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'" AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND  `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "06:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "08:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                
                                           $datamaps06h['maptime']=0;
                                           foreach ( $maps06h as $datamaps06h) {  }
                                           if(empty($datamaps06h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps06h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps06h['maptime'];
                                           }
                                          
                                           $maps06h->closeCursor();

                                           $maps08h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','    '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"  AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"  AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "08:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "10:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY   `'.SYS_GEO.'`.`ID_geo`');                                    
                                          $datamaps08h['maptime']=0;
                                           foreach ( $maps08h as $datamaps08h) {  }
                                           if(empty($datamaps08h['maptime'])){
                                                   $DataSet .= "0, ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps08h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps08h['maptime'];
                                           }
                                           $maps08h->closeCursor();

                                           $maps10h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"  AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "10:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "12:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                    
                                           $datamaps10h['maptime']=0;
                                           foreach ( $maps10h as $datamaps10h) {  }
                                           if(empty($datamaps10h['maptime'])){
                                                   $DataSet .= "0, ";    
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps10h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps10h['maptime'];
                                           }  
                                          
                                           $maps10h->closeCursor();

                                           $maps12h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','    '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"  AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "12:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "14:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY   `'.SYS_GEO.'`.`ID_geo`');                                      
                                           $datamaps12h['maptime']=0;
                                           foreach ( $maps12h as $datamaps12h) {  }
                                           if(empty($datamaps12h['maptime'])){
                                                   $DataSet .= "0, ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps12h['maptime'].", ";  
                                                   $TotalView = $TotalView + $datamaps12h['maptime'];
                                           }    
                                           
                                           $maps12h->closeCursor();

                                           $maps14h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"  AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "14:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "16:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                       
                                           $datamaps14h['maptime']=0;
                                           foreach ( $maps14h as $datamaps14h) {  }
                                           if(empty($datamaps14h['maptime'])){
                                                   $DataSet .= "0, ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps14h['maptime'].", ";  
                                                     $TotalView = $TotalView + $datamaps14h['maptime'];
                                           }  
                                         
                                           $maps14h->closeCursor();

                                           $maps16h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"  AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "16:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "18:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                    
                                           $datamaps16h['maptime']=0;
                                           foreach ( $maps16h as $datamaps16h) {  }
                                           if(empty($datamaps16h['maptime'])){
                                                   $DataSet .= "0, ";  
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps16h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps16h['maptime'];
                                           }  
                                          
                                           $maps16h->closeCursor();

                                           $maps18h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND  `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"   AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "18:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "20:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                     
                                           $datamaps18h['maptime']=0;
                                           foreach ( $maps18h as $datamaps18h) {  }
                                           if(empty($datamaps18h['maptime'])){
                                                   $DataSet .= "0, ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps18h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps18h['maptime'];
                                           }  
                                          
                                           $maps18h->closeCursor();

                                           $maps20h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"   AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "20:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "22:00"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY   `'.SYS_GEO.'`.`ID_geo`');                                        
                                           $datamaps20h['maptime']=0;
                                           foreach ( $maps20h as $datamaps20h) {  }
                                           if(empty($datamaps20h['maptime'])){
                                                   $DataSet .= "0, ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps20h['maptime'].", ";  
                                                    $TotalView = $TotalView + $datamaps20h['maptime'];
                                           } 
                                          
                                           $maps20h->closeCursor();

                                           $maps22h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"     AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"  AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  >= "22:00"  AND  LEFT(`'.SYS_PROCESS.'`.`time_creation`,5)  < "23:59"  AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.' GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                    
                                           $datamaps22h['maptime']=0;
                                           foreach ( $maps22h as $datamaps22h) {  }
                                           if(empty($datamaps22h['maptime'])){
                                                   $DataSet .= "0 ";   
                                                   $TotalView = $TotalView + 0;
                                           }else{
                                                   $DataSet .= $datamaps22h['maptime']."";  
                                                    $TotalView = $TotalView + $datamaps22h['maptime'];
                                           } 
                                          
                                           $maps22h->closeCursor();

                                 }else if(($dataTimes['type'] == 'days' and $dataTimes['value'] > 1 and $dataTimes['value'] < 31 ) or ($dataTimes['type'] == 'months' and $dataTimes['value'] == 1 ) ){
                                           $dateReq=$inst_routines->getDatesFromRange($_GET['DateStart'], $_GET['DateEnd']);
                                           $datamaps22h['maptime']=0;
                                           foreach($dateReq as $datemapreq){
                                                                              $maps22h        = $inst_Query->read_Query('`'.SYS_QHMNETWORK.'`,`'.INTRA_STE.'`,`'.SYS_GEO.'`,`'.SYS_USERS.'`,`'.SYS_PROCESS.'`', 'COUNT(*) AS maptime','   '.$where1.'   `'.SYS_QHMNETWORK.'`.`Sender_ID_sys_users`  = "'.$DATA_sys['ID_sys_users'].'"   AND    `'.SYS_PROCESS.'`.`ID_sys_users` !=  "'.$DATA_sys['ID_sys_users'].'"  AND `'.SYS_USERS.'`.`ID_sys_users` = `'.SYS_PROCESS.'`.`ID_sys_users`    AND `'.SYS_USERS.'`.`ID_ste` =   `'.INTRA_STE.'`.`ID_ste` AND   `'.INTRA_STE.'`.`Country` =   `'.SYS_GEO.'`.`iso2` AND  `'.INTRA_STE.'`.`Country` ="'.$data['iso2'].'"    AND  `'.SYS_PROCESS.'`.`date_creation`  = "'.$datemapreq.'"   AND   `'.SYS_QHMNETWORK.'`.`type` != "answer"  '.$where.'  GROUP BY  `'.SYS_GEO.'`.`ID_geo`');                                    
                                                                               foreach ( $maps22h as $datamaps22h) {  }
                                                                               if(empty($datamaps22h['maptime'])){
                                                                                      $DataSet .= "0,";     
                                                                                        $TotalView = $TotalView + 0;
                                                                               }else{
                                                                                      $DataSet .= $datamaps22h['maptime'].",";  
                                                                                        $TotalView = $TotalView + $datamaps22h['maptime'];
                                                                               } 
                                                                             
                                                                               $maps22h->closeCursor();
                                            }
                                           $DataSet =substr($DataSet, 0, -1);

                                 }

                               $DataSet .="]"; 
                      $DataSet .="},";
             }
             
             $DataSet =substr($DataSet, 0, -1);
  
             $datacont2['DataSet']        = $DataSet;
             $datacont2['TotalView']    = $TotalView;
             $datacont2['gridcontainer2']    =  $gridcontainer2;
             return $datacont2;       
             $Maps->closeCursor();
    
}

 function getDatesFromRange($startDate, $endDate) {
                   $return = array($startDate);
                   $start = $startDate;
                   $i = 1;
                   if (strtotime($startDate) < strtotime($endDate)) {
                       while (strtotime($start) < strtotime($endDate)) {
                           $start = date('Y-m-d', strtotime($startDate . '+' . $i . ' days'));
                           $return[] = $start;
                           $i++;
                       }
                   }

                   return $return;
   }   
    
 function home_ibui($lg, $time,$sDay,$city,$ID_sys_users,$Country ) {
                                $inst_fuser= new modules_Req_fuser();
                                $DATA_user = $inst_fuser->READ_fuser_by_ID_sys_users($ID_sys_users);
                                $name = $DATA_user['users_prenom'];
                 if($sDay == 'AM'){	   	
		if($time == 12  ){	
		      $home = ''.ucfirst(HELLO).' <strong>'.$name.'</strong> '.THINGS_EARLY.'';
		    
		 }elseif($time < 6){   
		     $home= ''.ucfirst(GOODDAY).' <strong>'.$name.'</strong> '.THINGS_EARLY.'';
		    
		}elseif($time < 11){
		    $home = ''.ucfirst(HELLO).' <strong>'.$name.'</strong> '.BEGINNINGDAY;
		    
		}else{
		     $home= ''.ucfirst(HELLO).' <strong>'.$name.'</strong> '.WRITE_HERE.'' ;
		
		}
		
		
	}elseif($sDay == 'PM'){
		if($time == 12 or $time == 1){ 
			$home= ''.ucfirst(ENJOYMEAL).' <strong>'.$name.'</strong> '.MAYBAY_RESTAURANT ;    
		 }elseif($time > 1  and $time < 5   ){    
			$home = ''.ucfirst(HELLO).' <strong>'.$name.'</strong> '.AFTERNOON     ;
		}elseif($time > 5){ 
		    $home = ''.ucfirst(GOODEVENING).' <strong>'.$name.'</strong> ' ;  
		    if($time > 6 and $time < 8 ){	$home .= END_THEDAY; }
		    if($time >  8 ){	$home .= ''; }
		}else{
		   $home = ''.ucfirst(HELLO).' <strong>'.$name.'</strong> '     ;
		}		
	}
	
	//NOUVEL AN
	if(date('m-d') == '01-01'){
	    ?><script>  parent.document.getElementById('listimg_home').style.backgroundImage = 'url(https://www.talkag.com/img_flx_cont/ny.gif)';</script><?php
	      $home = ''.ucfirst(NEWYEAR).'  <strong> '.date('Y').'  '.$name.'</strong> !!' ;  
	}
	
	//NOEL
	if(date('m-d') > '12-21'  and date('m-d') < '12-29'){
	          ?><script>  parent.document.getElementById('listimg_home').style.backgroundImage = 'url(https://www.talkag.com/img_flx_cont/Christmas_21.png)';</script><?php
	       $home = ''.ucfirst(HAPPYHOLIDAYS).' <strong>'.$name.'</strong>!!' ;  
	}
	if(date('m-d') == '12-25' ){
	         ?><script>  parent.document.getElementById('listimg_home').style.backgroundImage = 'url(https://www.talkag.com/img_flx_cont/Christmas_21.png)';</script><?php
	     $home = ''.ucfirst(MERRY_CHRISTMAS).' <strong>'.$name.'</strong> !!' ;  
	 }elseif( date('m-d') == '12-24' and $sDay == 'PM' and $time > 6 and $time < 8 ){ 
	          ?><script>  parent.document.getElementById('listimg_home').style.backgroundImage = 'url(https://www.talkag.com/img_flx_cont/Christmas_21.png)';</script><?php
	     $home = ''.ucfirst(MERRY_CHRISTMAS2).' <strong>'.$name.'</strong> !!' ;  
	}
	//FÊTEs
	if(date('m-d') == '02-14' ){
	      ?><script>  parent.document.getElementById('listimg_home').style.backgroundImage = 'url(https://www.talkag.com/img_flx_cont/valentin.png)';</script><?php
	      $home = ''.ucfirst(VALENTINE).'' ;  	      
	}
        
        	
	
	if(date('m-d') == '12-13' ){
	     $home = ''.HOME8.'' ;  
	}elseif(date('m-d') == '12-16'  ){
	         ?><script>  parent.document.getElementById('listimg_home').style.backgroundImage = 'url(https://www.talkag.com/img_flx_cont/Christmas_6.png)';</script><?php
	      $home = ''.HOME9.'' ;  
        
	}elseif(date('m-d') == '05-11'  ){
	         ?><script>  parent.document.getElementById('listimg_home').style.backgroundImage = 'url(https://www.talkag.com/img_flx_cont/Panda.png)';</script><?php
	      $home = ''.HOME10.'' ;                
	
        	}elseif(date('m-d') == '10-30' or date('m-d') == '10-31'   ){
	         ?><script>  parent.document.getElementById('listimg_home').style.backgroundImage = 'url(https://www.talkag.com/img_flx_cont/halloween_11.png)';</script><?php
	      $home = ''.HOME11.'' ;                
	}
      
                if(date('m-d') == '06-10' and  $city == '13172'){
                        ?><script>  parent.document.getElementById('listimg_home').style.backgroundImage = 'url(https://www.talkag.com/img_flx_cont/ibui-rainbow.png)';</script><?php
                }
                
                 if(date('m-d') == '11-20'){
                         $home = ''.ucfirst(CHILDREN).'' ;  
                        ?><script>  parent.document.getElementById('listimg_home').style.backgroundImage = 'url(https://www.talkag.com/img_flx_cont/children.jpg)';</script><?php
                }
	return $home;
 }   
 
    
function autoSelectLanguage($aLanguages, $sDefault = 'fr') {
	if(!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
	  $aBrowserLanguages = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
	  foreach($aBrowserLanguages as $sBrowserLanguage) {
	    $sLang = strtolower(substr($sBrowserLanguage,0,2));
	    if(in_array($sLang, $aLanguages)) {
	      return $sLang;
	    }
	  }
	}
	return $sDefault;
}

function getNomDeDomaine($url) {
    
    $hostname = parse_url($url, PHP_URL_HOST);
    $hostParts = explode('.', $hostname);
    $numberParts = sizeof($hostParts);
    $domain='';
    
    // Domaine sans tld (ex: http://server/page.php)
    if(1 === $numberParts) {
        $domain = current($hostParts);
    }
    // Domaine avec tld (ex: http://fr.php.net/parse-url)
    elseif($numberParts>=2) {
        $hostParts = array_reverse($hostParts);
        $domain = $hostParts[1] .'.'. $hostParts[0];
    }
    return $domain;
}

 function format_CONTENTnotcnx($content,$zone,$statut,$modif_data){
        $inst_routines= new core_Routines();
         $newcontent='';
         $onclick='';
         $exp = explode('*',$modif_data);
         
                                                                   //DETECTION DES HASHTAG
                                                                   $tab_hashtag = $inst_routines->ReadHashtags($content) ; 
                                                                
                                                                  if($tab_hashtag != false){                                                                                       
                                                                                        for ($mp = 0; $mp <=  count( $tab_hashtag); $mp++) { 
                                                                                           if(!empty($tab_hashtag[$mp])){   
                                                                                               $in[$mp] = $tab_hashtag[$mp];
                                                                                   /*   https://www.talkag.com/talkag/?sys=qhapps.flx_medium&lg=<?= $_GET['lg']  ?>&idzn=qhapps.flx_medium&t_notif=aff_flx&precpte=<?= $_GET['precpte'] ?>&Sender_ID_sys_users=<?= $_GET['Sender_ID_sys_users'] ?>&type_flx=ITEMS*<?= $DATA_items['ID_masteritems'] ?>_<?= $DATA_items['ID_items'] ?> */
                                                                                                 if( $_GET['type_flx']  == 'ncnxmediumexplorer'  or substr($_GET['type_flx'], 0,5) == 'hexpl' ){      
                                                                                                               if(!empty($DATA_QHM_U)){ $onclick='OnClick="javascript:Remove_winBoxAlertQHMw(\''.$_GET['lg'].'\');javascript: initint(\''.URL_RACINE_DESK_m.'\',\'sys=qhapps.flx_medium&lg=fr&idzn=qhapps.flx_medium&ncnx=true&t_notif=explorer&lg='.$_GET['lg'].'&type_flx=hexpl*'.str_replace('#','',str_replace('_','+',$tab_hashtag[$mp])).'\');"'; }    
                                                                                        
                                                                                                 }else{
                                                                                                                $onclick='https://www.talkag.com/talkag/?sys=qhapps.flx_medium&idzn=qhapps.flx_medium&t_notif=aff_flx&precpte='.$exp[0].'&Sender_ID_sys_users='.$exp[1].'&lg='.$exp[2].'&type_flx=hasht*'.str_replace('#','',str_replace('_','+',$tab_hashtag[$mp])).''; 
                                                                                          //        if(!empty($DATA_QHM_U)){ $onclick='OnClick="javascript: initint(\''.URL_RACINE_DESK_m.'\',\'idzn=planet&type_flx=hasht#'.$tab_hashtag[$mp].'\');"'; }    
                                                                                                 }
                                                                                               $out[$mp] = '  <a href=" '.$onclick.'"    class="link_CONTENT">'.$tab_hashtag[$mp].'</a>';
                                                                                          }  
                                                                                         
                                                                                        }         
                                                                                       $newcontent = str_replace($in , $out,  $content); 
                                                                    }else{
                                                                         $newcontent = $content;
                                                                    }     
                                                                    
                                                                    //DETECTION DES LIENS HTTP
                                                            
                                                           $tab_URL = $inst_routines->ReadURL($content) ; 
                                                                    if($tab_URL != false){                                                                                       
                                                                                       for ($mpURL = 0; $mpURL <=  count($tab_URL); $mpURL++) { 
                                                                                           if(!empty($tab_URL[$mpURL])){ 
                                                                                              
                                                                                                           $inURL[$mpURL] = $tab_URL[$mpURL];           
					$exp_url = '';
                                                                                                           $hosturl='';
                                                                                                           $exp_url2='';
                                                                                                           $exp_url = explode('/', $tab_URL[$mpURL]);
                                                                                                           $exp_url2 = explode('//', $tab_URL[$mpURL]);
                                                                                                            
                                                                                                           if(substr($exp_url[0], -2) == 's:'){
                                                                                                                 $hosturl= 'https://';
                                                                                                           }else{
                                                                                                                   $hosturl= 'http://';
                                                                                                           }
                                                                                                           
                                                                                                           $outURL[$mpURL] = '  <a href="'.$hosturl.''.$exp_url2[1].'"  class="link_CONTENT" target="_blank"  >'.$exp_url[2].'</a> ';
                                                                                                          
                                                                                          }  
                                                                                         
                                                                                        }         
                                                                                     $newcontent = str_replace($inURL , $outURL,   $newcontent); 
                                                                 }else{
                                                            
                                                                     $newcontent = $newcontent;
                                                             }              
                                                             
                                                            $expcontent = explode(' ', $newcontent);
                                                            if(substr($expcontent[0], 0,5 ) == 'http:' or substr($expcontent[0], 0,6 ) == 'https:' ) {
                                                                $exp_url3 = explode('/', $expcontent[0]);
                                                                $firstlink='';
                                                                $firstlink =   '  <a href="'.$expcontent[0].'"  class="link_CONTENT" target="_blank"  >'.$exp_url3[2].'</a> ';
                                                                $newcontent = str_replace($expcontent[0], $firstlink, $newcontent);
                                                            }                                           
                                                                 //DETECTION DES EMOTICONS   
                                                                 $newcontent=   $inst_routines->INS_smileys($newcontent);
                                            
                                                        
                                                         return    $newcontent;
                                                              
    }
    
    


    function format_CONTENT($content,$zone,$statut,$DATA_QHM_U){
        $inst_routines= new core_Routines();
         $newcontent='';
         $onclick='';
         
                                                                   //DETECTION DES HASHTAG
                                                                   $tab_hashtag = $inst_routines->ReadHashtags($content) ; 
                                                                
                                                                  if($tab_hashtag != false){                                                                                       
                                                                                        for ($mp = 0; $mp <=  count( $tab_hashtag); $mp++) { 
                                                                                           if(!empty($tab_hashtag[$mp])){   
                                                                                               $in[$mp] = $tab_hashtag[$mp];
                                                                               
                                                                                          // echo '-'.$_GET['type_flx'];
                                                                                               if($_GET['type_flx']  == 'ncnxexplorer' or substr($_GET['type_flx'], 0,5) == 'hexpl' ){
                                                                                                     if(!empty($DATA_QHM_U)){ $onclick='OnClick="javascript: initint(\''.URL_RACINE_DESK_m.'\',\'sys=qhapps.flx_medium&lg=fr&idzn=qhapps.flx_medium&ncnx=true&t_notif=explorer&lg='.$_GET['lg'].'&type_flx=hexpl*'.str_replace('#','',str_replace('_','+',$tab_hashtag[$mp])).'\');"'; }          
                                                                                              
                                                                                              }else if($_GET['type_flx']  == 'explorer' or substr($_GET['type_flx'], 0,5) == 'hexpl' ){
                                                                                                     if(!empty($DATA_QHM_U)){ $onclick='OnClick="javascript: initint(\''.URL_RACINE_DESK_m.'\',\'sys=desktop.home_explorer&type_flx=hexpl*'.str_replace('#','',str_replace('_','+',$tab_hashtag[$mp])).'\');"'; }    
                                                                                                     
                                                                                              }elseif( $_GET['type_flx']  == 'ncnxmediumexplorer' ){      
                                                                                                               if(!empty($DATA_QHM_U)){ $onclick='OnClick="javascript:Remove_winBoxAlertQHMw(\''.$_GET['lg'].'\');javascript: initint(\''.URL_RACINE_DESK_m.'\',\'sys=qhapps.flx_medium&lg=fr&idzn=qhapps.flx_medium&ncnx=true&t_notif=explorer&lg='.$_GET['lg'].'&type_flx=hexpl*'.str_replace('#','',str_replace('_','+',$tab_hashtag[$mp])).'\');"'; }    
                                                                                                         
                                                                                             }elseif( $_GET['type_flx']  == 'mediumexplorer' ){      
                                                                                                      if(!empty($DATA_QHM_U)){ $onclick='OnClick="javascript:Remove_winBoxAlertQHMw(\''.$_GET['lg'].'\');javascript: initint(\''.URL_RACINE_DESK_m.'\',\'sys=desktop.home_explorer&type_flx=hexpl*'.str_replace('#','',str_replace('_','+',$tab_hashtag[$mp])).'\');"'; }    
                                                                                               }elseif(!empty($_GET['WordMedium']) ){
                                                                                                     if(!empty($DATA_QHM_U)){ $onclick='OnClick="javascript:Remove_winBoxAlertQHMw(\''.$_GET['lg'].'\');javascript: initint(\''.URL_RACINE_DESK_m.'\',\'type_flx=hasht*'.str_replace('#','',str_replace('_','+',$tab_hashtag[$mp])).'\');"'; }    
                                                                                             
                                                                                              }else{
                                                                                                     if(!empty($DATA_QHM_U)){ $onclick='OnClick="javascript: initint(\''.URL_RACINE_DESK_m.'\',\'type_flx=hasht*'.str_replace('#','',str_replace('_','+',$tab_hashtag[$mp])).'\');"'; }    
                                                                                               }
                                                                                             
                                                                                          //        if(!empty($DATA_QHM_U)){ $onclick='OnClick="javascript: initint(\''.URL_RACINE_DESK_m.'\',\'idzn=planet&type_flx=hasht#'.$tab_hashtag[$mp].'\');"'; }    
                                                                                               $out[$mp] = '  <a href=" #TalkAG"  '.$onclick.'   class="link_CONTENT">'.$tab_hashtag[$mp].'</a>';
                                                                                          }  
                                                                                         
                                                                                        }         
                                                                                       $newcontent2 = str_replace($in , $out,  $content); 
                                                                    }else{
                                                                         $newcontent2 = $content;
                                                                    }     
                                                                    
                                                                $tabuser='';
                                                                $mpuser=0;
                                                                $tabuser = $inst_routines->Readuser($content) ; 
                                                                
                                                                if($tabuser != false){                                                                                       
                                                                                        for ($mpuser = 0; $mpuser <=  count( $tabuser); $mpuser++) { 
                                                                                           if(!empty($tabuser[$mpuser])){   
                                                                                               $in[$mpuser] = $tabuser[$mpuser];
                                                                         
                                                                                                     if(!empty($DATA_QHM_U)){ $onclick='OnClick="javascript: result_maccount(\''.$_GET['lg'].'\',\''.$tabuser[$mpuser].'\');"'; }    
                                                                                               
                                                                                               $out[$mpuser] = '  <a href=" #TalkAG"  '.$onclick.'   class="link_CONTENT">'.$tabuser[$mpuser].'</a>';
                                                                                          }  
                                                                                         
                                                                                        }         
                                                                                       $newcontent = str_replace($in , $out,  $newcontent2); 
                                                                }else{
                                                                         $newcontent = $newcontent2;
                                                                 } 
                                                                    //DETECTION DES LIENS HTTP
                                                            
                                                           $tab_URL = $inst_routines->ReadURL($content) ; 
                                                                    if($tab_URL != false){                                                                                       
                                                                                       for ($mpURL = 0; $mpURL <=  count($tab_URL); $mpURL++) { 
                                                                                           if(!empty($tab_URL[$mpURL])){ 
                                                                                              
                                                                                                           $inURL[$mpURL] = $tab_URL[$mpURL];           
					$exp_url = '';
                                                                                                           $hosturl='';
                                                                                                           $exp_url2='';
                                                                                                           $exp_url = explode('/', $tab_URL[$mpURL]);
                                                                                                           $exp_url2 = explode('//', $tab_URL[$mpURL]);
                                                                                                            
                                                                                                           if(substr($exp_url[0], -2) == 's:'){
                                                                                                                 $hosturl= 'https://';
                                                                                                           }else{
                                                                                                                   $hosturl= 'http://';
                                                                                                           }
                                                                                                           
                                                                                                           $outURL[$mpURL] = '  <a href="'.$hosturl.''.$exp_url2[1].'"  class="link_CONTENT" target="_blank"  >'.$exp_url[2].'</a> ';
                                                                                                          
                                                                                          }  
                                                                                         
                                                                                        }         
                                                                                     $newcontent = str_replace($inURL , $outURL,   $newcontent); 
                                                                 }else{
                                                            
                                                                     $newcontent = $newcontent;
                                                             }              
                                                             
                                                            $expcontent = explode(' ', $newcontent);
                                                            if(substr($expcontent[0], 0,5 ) == 'http:' or substr($expcontent[0], 0,6 ) == 'https:' ) {
                                                                $exp_url3 = explode('/', $expcontent[0]);
                                                                $firstlink='';
                                                                $firstlink =   '  <a href="'.$expcontent[0].'"  class="link_CONTENT" target="_blank"  >'.$exp_url3[2].'</a> ';
                                                                $newcontent = str_replace($expcontent[0], $firstlink, $newcontent);
                                                            }                                           
                                                                 //DETECTION DES EMOTICONS   
                                                                 $newcontent=   $inst_routines->INS_smileys($newcontent);
                                            
                                                        
                                                         return    $newcontent;
                                                              
    }
    
  
               
               
    
      function ReadURL($string) {  
                        $URLtags= FALSE;  
                  //      $text = preg_replace('@([^>"])(https?://[a-z0-9\./+,%#_-]+)@i', '$1<a href="$2">$2</a>', $text);
                        //@([^>"])(https?://[a-z0-9\./+,%#_-]+)@i
                        //#(((https?|ftp)://(w{3}\.)?)(?<!www)(\w+-?)*\.([a-z]{2,4}))#
                        //#(?:(?:(?:https?|ftp)://)?(?:www\.)?\w+(?:(?:/|\.)\w+)*\.\w{2,4})(?:\?\w+=\w+(?:\&\w+=\w+)*)?#i
                        //#(?:(?:(?:https?|ftp)://)?(?:www\.)?\w+(?:/\w+)*\.\w{2,4})(?:\?\w+=\w+(?:\&\w+=\w+)*)?#i
                        preg_match_all('@([^>"])(https?://[a-z0-9\./+,%#_-]+)@i',$string,$matches);  
                        if ($matches) {
                            $URLtagsArray = array_count_values($matches[0]);
                            $URLtags = array_keys($URLtagsArray);
                        }else{
                            $URLtags = false;
                        }
                     //  if(!$hashtag != false){ 
                            return $URLtags;
                     //  }
        }
        
          function ReadHashtags($string) {  
                        $taghashtags= FALSE;  
                           preg_match_all('/#([^\s]+)/', $string, $matches);
                     //   preg_match_all("/(#\w+)/u", $string, $matches);  
                        if ($matches) {
                            $taghashtagsArray = array_count_values($matches[0]);
                             $taghashtags = array_keys($taghashtagsArray);
                        }else{
                             $taghashtags = false;
                        }
                     // if(!$hashtag != false){ 
                            return  $taghashtags;
                   //    }
        }
        
           function Readuser($string) {  
                        $taghashtags= FALSE;  
                        preg_match_all('/@([^\s]+)/', $string, $matches);
                      //  preg_match_all("/(@\w+)/u", $string, $matches);  
                        if ($matches) {
                            $taghashtagsArray = array_count_values($matches[0]);
                             $taghashtags = array_keys($taghashtagsArray);
                        }else{
                             $taghashtags = false;
                        }
                     // if(!$hashtag != false){ 
                            return  $taghashtags;
                   //    }
        }
        
           function Readurlhttps($string) {  
                        $taghashtags= FALSE;  
                        preg_match_all('/http([^\s]+)/', $string, $matches);
                      //  preg_match_all("/(@\w+)/u", $string, $matches);  
                        if ($matches) {
                            $taghashtagsArray = array_count_values($matches[0]);
                             $taghashtags = array_keys($taghashtagsArray);
                        }else{
                             $taghashtags = false;
                        }
                     // if(!$hashtag != false){ 
                            return  $taghashtags;
                   //    }
        }
        

    function INS_smileys($texte){
             $in=array(
                        "o:)",   "O:)",           
                       ":-)" ,  ":-))",   ":)" ,  ":))",
                       ":-(",    ":-((",    ":(",    ":((",  
                       ":-o",  ":-O",  ":-0",   ":o",  ":O",  ":0",    
                       "8-)",  "8-))",     "8)",  "8))",
                       ":-|",   ":|", 
                       ":-D",   ":D",  
                       "8-|",   "8|",
                       ":-p",     ":-P",                           
                       "<3",
                       ";-)",  ";)",
                       ":diable:",   ":evil:",
                       ">)",
                       ":vador:",":vader:",
                       "xxp","XXp",
                       ":flower:",
                       ":bobafett:",
                       ":stormtrooper:",
                       ":poop:",":caca:",
                       ":shark:","(^^^)",
                       ":cat:",
                       ":phone:",
                       ":plane:",
                       ":car:",
                       ":eiffel:",
                       ":plane:",
                       ":sunbed:",
                       ":island:",
                       ":swimsuit:",
                       ":glass:",
                       ":ibui:",
                       ":coffee:",":café:",
                       ":pizza:",
                       ":restaurant:",
                       ":beauty:",
                        ":rain:",":pluie:",
                        ":roadtrip:",
                       ":sun:",
                       ":yeah:",
                       ":train:",
                        ":store:",
                        ":beer:",
                        ":rock:",
                        ":book:",
                        ":storm:",
                        ":rainbow:",
                        ":run:",":runner:",":running:",
	     ":like:",
                   ":gaypride:"
                 
                 );

            $out=array(
                  '<img src="https://www.talkag.com/img_flx_cont/smile14.png" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile14.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile1.png" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile1.png" alt="" />',   '<img src="https://www.talkag.com/img_flx_cont/smile1.png" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile1.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile2.png" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile2.png" alt="" />',  '<img src="https://www.talkag.com/img_flx_cont/smile2.png" alt="" />',  '<img src="https://www.talkag.com/img_flx_cont/smile2.png" alt="" />', 
                  '<img src="https://www.talkag.com/img_flx_cont/smile3.png" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile3.png" alt="" />',   '<img src="https://www.talkag.com/img_flx_cont/smile3.png" alt="" />',   '<img src="https://www.talkag.com/img_flx_cont/smile3.png" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile3.png" alt="" />',   '<img src="https://www.talkag.com/img_flx_cont/smile3.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile5.png" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile5.png" alt="" />',   '<img src="https://www.talkag.com/img_flx_cont/smile5.png" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile5.png" alt="" />',   
                  '<img src="https://www.talkag.com/img_flx_cont/smile6.png" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile6.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile7.png" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile7.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile8.png" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile8.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile9.png" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile9.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile12.png" alt="" />',               
                  '<img src="https://www.talkag.com/img_flx_cont/smile10.png" alt="" />',  '<img src="https://www.talkag.com/img_flx_cont/smile10.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile4.png" alt="" />',  '<img src="https://www.talkag.com/img_flx_cont/smile4.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile13.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile15.png" alt="" />',  '<img src="https://www.talkag.com/img_flx_cont/smile15.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile16.png" alt="" />',  '<img src="https://www.talkag.com/img_flx_cont/smile16.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile17.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile18.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile19.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile20.gif" alt="" />', '<img src="https://www.talkag.com/img_flx_cont/smile20.gif" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile21.png" alt="" />',  '<img src="https://www.talkag.com/img_flx_cont/smile21.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile22.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile23.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile24.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile25.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile26.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile27.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile28.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile29.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile30.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile31.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile32.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile33.png" alt="" />',  '<img src="https://www.talkag.com/img_flx_cont/smile33.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile34.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile35.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile36.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile37.png" alt="" />',  '<img src="https://www.talkag.com/img_flx_cont/smile37.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile38.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile39.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile40.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile41.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile42.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile43.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile44.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile45.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile46.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile47.png" alt="" />',
                  '<img src="https://www.talkag.com/img_flx_cont/smile48.png" alt="" />',  '<img src="https://www.talkag.com/img_flx_cont/smile48.png" alt="" />',  '<img src="https://www.talkag.com/img_flx_cont/smile48.png" alt="" />',
    '<img src="https://www.talkag.com/img_flx_cont/smile49.png" alt="" />','<img src="https://www.talkag.com/img_flx_cont/gaypride.png" alt="" />'			
                       );
       

                       return str_replace($in,$out,$texte);
    }
    
 function scanDEVICE(){
                  $detect = new core_Mobile_Detect;
 
            // Any mobile device (phones or tablets).
            if ( $detect->isMobile() ) {
                          $device =  'mobile';
            }

            // Any tablet device.
            if( $detect->isTablet() ){
              $device = 'tablet';
            }

            // Exclude tablets.
            if( !$detect->isMobile() && !$detect->isTablet() ){
                    $device = 'desktop';
            }
            
            return $device;
 }
 
    function scanSYSTEM(){
                  $detect = new core_Mobile_Detect;
 
            // Any mobile device (phones or tablets).
            if (  $detect->version('Android')) {
                          $system =  'android';
            }
            if (  $detect->version('iPhone')) {
                          $system =  'ios';
            }
           if ( $detect->version('iPad')) {
                          $system =  'ios';
            }
            
            
            return $system;
 }
    
    
   function scanTYPEDEVICE(){
                  $detect = new core_Mobile_Detect;
 
            // Any mobile device (phones or tablets).
            if ( $detect->isMobile() ) {
                          $device =  'mobile';
            }

            // Any tablet device.
            if( $detect->isTablet() ){
              $device = 'tablet';
            }

            // Exclude tablets.
            if( !$detect->isMobile() && !$detect->isTablet() ){
                    $device = 'desktop';
            }
            
            return $device;        
    }
    
 
    
    function searched_words($needle, $haystack) {
    $words = explode('*', $needle);
 
    foreach( $words as $k => $v )
        $words_replace[$k] = '<span style="font-weight:bold;">'.$v.'</span>';
 
    // Remplacement
    return str_ireplace($words, $words_replace, $haystack);
}
    
    function MettreMotEnGras($mot, $phrase) {
            $mot = strtolower(strtr( $mot, "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ", "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn" ));
            $recherche = array("a","e","i","o","u","y","c","n" );
            $remplacement = array("[a|à|á|â|ã|ä|å]{1}","[e|è|é|ê|ë]{1}","[i|ì|í|î|ï]{1}","[o|ò  |ó|ô|õ|ö|ø]{1}","[u|ù|ú|û|ü]{1}","[y|y]{1}","[c|ç]{1}","[n|ñ]{1}" );
            $mot = str_replace($recherche, $remplacement, $mot);
            return eregi_replace("($mot)", "<strong><b></strong>\\1<strong></b></strong>", $phrase);
          }
    

    
    function rgb2hsv ($RGB){
                $var_R = $RGB[0]/255;
                $var_G = $RGB[1]/255;
                $var_B = $RGB[2]/255;
                $var_Min = min($var_R, $var_G, $var_B);
                $var_Max = max($var_R, $var_G, $var_B);
                $del_Max = $var_Max - $var_Min;

                $V = $var_Max;

                if ($del_Max == 0)
                {
                  $H = 0;
                  $S = 0;  
                }
                else
                {
                  $S = $del_Max / $var_Max;
                  $del_R = ((($var_Max - $var_R)/6) + ($del_Max/2)) / $del_Max;
                  $del_G = ((($var_Max - $var_G)/6) + ($del_Max/2)) / $del_Max;
                  $del_B = ((($var_Max - $var_B)/6) + ($del_Max/2)) / $del_Max;

                  if      ($var_R == $var_Max) $H = $del_B - $del_G;
                  else if ($var_G == $var_Max) $H = (1/3) + $del_R - $del_B;
                  else if ($var_B == $var_Max) $H = (2/3) + $del_G - $del_R;

                  if      ($H<0) $H++;
                  else if ($H>1) $H--;
                }

                return array($H*255, $S*255, $V*255);
}

// norme euclidienne
function distance($col1, $col2)
{
  $distance = 0;
  for($i=0 ; $i<3 ; $i++)
  {
    $diff[$i] = $col1[$i]/255-$col2[$i]/255;
    $distance += $diff[$i]*$diff[$i];
  }
  if ($distance == 0) $distance = 0.01; // pour ne pas avoir l'inverse de 0
  return sqrt($distance);
}

// fonctionne mère
function get_main_color($img, $points, $colors){
    $inst_routines= new core_Routines();
  $width = imagesx($img);
  $height = imagesy($img);
  $scale = intval(max($width,$height)/$points);
  
  // Pour chaque base on convertir en HSV et on initialise le compteur
  foreach ($colors as $name => $base)
  {
    $results[$name] = 0;
    $colors[$name] = $inst_routines->rgb2hsv($base);
  }
  
  $colors_grey = 0; // Compteur pour les pixels gris (R=G=B)
  $pixel_count = 0; // Compteur du nombre total de pixels analysés
      
  for ($j=0; $j<$height; $j+=$scale)
  {
    for ($i=0; $i<$width; $i+=$scale)
    {
      // Couleur du pixel
      $color = imagecolorat($img, $i, $j);
      $r = ($color >> 16) &0xFF;
      $g = ($color >> 8) &0xFF;
      $b = $color &0xFF;
      
      if ($r==$g and $g==$b)
      {
        $colors_grey++;
      }
      else
      {
        // On incrémente le compteur de chaque base 
        // de l'inverse de la distance à la couleur du pixel
        foreach ($colors as $name => $base)
        {
          $color = $inst_routines->rgb2hsv(array($r, $g, $b));
          $results[$name] += 1 / $inst_routines->distance($color, $base);
        }
      }
      
      $pixel_count++;
    }
  }

  if ($colors_grey == $pixel_count)
  {
    return '#0084b4';
  }
  else
  {
    asort($results);
    $results = array_keys($results);
    return $results[count($results)-1];
  }
}

function MasterColor($urlimg){
 
    $inst_routines= new core_Routines();
 /*   'red' => array(255,0,0),
  'orange' => array(255,127,0),
  'yellow' => array(255,255,0),
  'green' => array(0,255,0),
  'blue' => array(0,0,255),
  'purple' => array(127,0,255),
  'pink' => array(255,0,255),*/    
    $colors = array(
                '#eb3f79' => array(255,0,0),
                '#e95f5c' => array(255,255,0),
                '#f8a724' => array(255,127,0),
                '#378d3b' => array(0,255,0),
                '#00abc0' => array(0,0,255),
                '#7d56c1' => array(127,0,255),
                '#eb3f79' => array(255,0,255),
        
            );
           //#606060
           $img = imagecreatefromjpeg($urlimg);
            return   $inst_routines->get_main_color($img, 50, $colors);
}

    
                function extFile($file){
                    $info = new SplFileInfo($file);
                    return $info->getExtension();
                }
                
                function lg(){
                  $lg=strtolower(substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0,2));
                  return $lg;
                }   
                
                function plvideo($width,$height,$file){
                    $inst_routines= new core_Routines();
                        if($inst_routines-> checkurlFileplVideo2($file) != false)  {        
                           
                                $code =' <embed type="application/x-shockwave-flash" src="'.URLSSL_MASTER.'_ADDON/plvideo2/player.swf?v1.3.5" width="'.$width.'" id="f" name="f" height="'.$height.'" flashvars="skin='.URLSSL_MASTER.'_ADDON/plvideo2/skins/mySkin.swf&thumbnail=thumbnail.jpg&video='.$file.'" allowScriptAccess="always" allowfullscreen="true" bgcolor="#000000"/>
                                                    <noembed>
                                                    You need Adobe Flash Player to watch this video. <br> 
                                                    <a href="http://get.adobe.com/flashplayer/">Download it from Adobe.</a>
                                                    </noembed>';
                        }else{
                                 $code = false;
                        }
                        return $code;
                }
                    
                
                function plaudio($width,$height,$typeplayer,$file){
                    $code ='    <object type="application/x-shockwave-flash" data="'.URLSSL_MASTER.'_ADDON/plaudio2/template_'.$typeplayer.'/player_mp3_'.$typeplayer.'.swf" width="'.$width.'" height="30"> 
                                            <param name="movie" value="'.URLSSL_MASTER.'_ADDON/plaudio2/template_'.$typeplayer.'/player_mp3_'.$typeplayer.'.swf" /> 
                                            <param name="wmode" value="transparent" /> <param name="FlashVars" value="mp3='.$file.'&amp;width='.$width.'&amp;height='.$height.'&amp;loop=1&amp;autoload=1&amp;showstop=1&amp;showinfo=1&amp;showvolume=1&amp;showloading=always&amp;buttonwidth=30&amp;sliderwidth=15&amp;sliderheight=12&amp;volumewidth=65&amp;volumeheight=10&amp;loadingcolor=1cabeb&amp;bgcolor=ffffff&amp;bgcolor1=ddecf4&amp;bgcolor2=ddecf4&amp;slidercolor1=1cabeb&amp;slidercolor2=1cabeb&amp;buttoncolor=1cabeb&amp;buttonovercolor=0d4d6a&amp;textcolor=0d4d6a" /> 
                                    </object>';
                    return $code;
                }
    
    
    	
                
                
    	function calendrier($lg,$mois,$annee,$id,$id_title){
                            $inst_routines= new core_Routines();
                            $jours_short['en'] = array('sun.', 'mon.', 'tue.', 'wed.', 'thu.', 'fri.', 'sat.');            
                            $jours_short['fr'] = array('dim.', 'lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.');
      
                      //  $lesMois 	= array("Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre");
	   //global $lesMois;
		//On recupere le jour courant
		$date_du_jours=getdate();
		
		//On recupere le premier du mois 
                                      
		$pm=getdate(mktime(0,0,0,$mois,1,$annee)); 
		
		//On recupere le numero du premier jour dans la semaine exprimee de 1 a 7 
		$num_premier_jour=$pm["wday"]; 
		
		//Attention un mois peu s'afficher sur 6 semaines
		$maxSem=5;
		if ($num_premier_jour==0) {
			$num_premier_jour=7;
			$maxSem=5;
		} ?>
		<table border="0"   cellspacing="0" cellpadding="3" width="100%">
		<tr class="titremois2 backgroundGREYelement">
                                <td class="titremois2" align="center"><?= ucfirst($jours_short[$lg][1] ) ?></td>
		<td class="titremois2"align="center"><?= ucfirst($jours_short[$lg][2] ) ?></td>
		<td class="titremois2" align="center"><?= ucfirst($jours_short[$lg][3] ) ?></td>
                                <td class="titremois2"  align="center"><?= ucfirst($jours_short[$lg][4] ) ?></td>
		<td class="titremois2" align="center"><?= ucfirst($jours_short[$lg][5] ) ?></td>
		<td class="titremois2" align="center"><?= ucfirst($jours_short[$lg][6] ) ?></td> 
		<td class="titremois2" align="center"><?= ucfirst($jours_short[$lg][0] ) ?></td>
		</tr>
		<?php
		$test=0;
	
		for($sem=0; $sem<=$maxSem; $sem++) { ?>
			<tr class="titreweek2">
			<?php for($j=1; $j<=7; $j++) { 
				$jourDuMois=1-$num_premier_jour+$sem*7+$j;
				$jj=getdate(mktime(0,0,0,$mois,$jourDuMois,$annee)); 
				$leJour=$jj["mday"];
				if($leJour==1) {
					$test=$test+1;
					if ($test>=2) $maxSem--;
				}
				//$leMois=$lesMois[$mois-1];
				if ($test==1) {
					if($jj["yday"]==$date_du_jours["yday"] && $annee==$date_du_jours["year"]){
						?><td align="center" bgcolor="#e3001a" class="liens2 " onclick="javascript:chk_eventaff('<?=  $annee.'-'.str_pad( $mois, 2, "0", STR_PAD_LEFT).'-'.str_pad( $leJour, 2, "0", STR_PAD_LEFT) ?>','<?= ucfirst( $inst_routines->dateQHM( $annee.'-'.str_pad( $mois, 2, "0", STR_PAD_LEFT).'-'.str_pad( $leJour, 2, "0", STR_PAD_LEFT),$lg,'norm')) ?>', '<?= $id ?>', '<?= $id_title ?>')">
                                                                                                <strong><?= $leJour ?></strong><?php
					}else{           
                                                                                                ?><td align="center" class="tabnormalJOUR " onMouseOver="this.className='taboverJOUR'" onMouseOut="this.className='tabnormalJOUR'" onclick="javascript:chk_eventaff('<?=  $annee.'-'.str_pad( $mois, 2, "0", STR_PAD_LEFT).'-'.str_pad( $leJour, 2, "0", STR_PAD_LEFT) ?>','<?= ucfirst( $inst_routines->dateQHM( $annee.'-'.str_pad( $mois, 2, "0", STR_PAD_LEFT).'-'.str_pad( $leJour, 2, "0", STR_PAD_LEFT),$lg,'norm')) ?>', '<?= $id ?>', '<?= $id_title ?>')">
                                                                                                <?= $leJour ?><?php 
					}
				} else { ?>
					<td align=center>
					
				<?php } ?>
					</td>
			<?php } ?>
				</tr>
		<?php } 
		?></table><?php 
	}           
    
    
     function DetectClasseChar($char){    
         $motif='`[aeiouy]`';          
         if( preg_match($motif,$char)){
             return APOSTROPHE;
         }else{
             return false;
         }
     }

    function GetContentWebSite($url){
        $inst_routines= new core_Routines();
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);                        
                        $result = curl_exec ($ch);
                                   
                        curl_close($ch);      
                        
                   //    preg_match( "/<title>(.*)<\/title>/i",  $result, $match );
                        //$DATA['title'][0] =  strip_tags($match[0]);
                        $tagsmeta['description']='';
                        $tagsmeta = get_meta_tags($url);
                        /*echo $tags['author'];       
                        echo $tags['keywords'];     
                        echo $tags['description'];  
                        echo $tags['geo_position']; */
                       // OpenGraph                    
                        $graph = api_fb_OpenGraph::fetch($url);
                         $imgOG['image']='';
                         
                        foreach ($graph as $key => $value) {
                            $imgOG[$key] = $value;      
                        }                    
                        
                       if(!empty($tagsmeta['description'])){  $DATA['description'][0]=  $tagsmeta['description'];      }else{   $DATA['description'][0]='';}
                       
                         if(!empty($imgOG['url'] )){ $DATA['url'][0]= $imgOG['url']  ;  }
                         //Ã
                         if(stripos($imgOG['title'], 'Ã')== false){
                              $DATA['title'][0] = $imgOG['title']; 
                         }else{
                               $DATA['title'][0] = utf8_decode($imgOG['title']);                                         
                        }
                        // echo'-'.$imgOG['title'];
                       //  $DATA['title'][0] = $imgOG['title']; 
                        //
                      //utf8_decode(
                        //-----extraction images
                         $exp_img= explode('?', $imgOG['image'] );
                         if($inst_routines-> checkurlFileImg($exp_img[0] ) != false){    //si balise image openGraph valid
                                $DATA['img'][0]= $exp_img[0]  ;   
                               
                          }else{
                                $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/Ui', $result, $matches);
                                 $e = 0;
                                 for ($i = 0; $i < $output; $i++) {                          
                                     if (substr($matches[1][$i], 0, 7) == 'http://' || substr($matches[1][$i], 0, 8) == 'https://') {
                                         $exp_img= explode('?', $matches[1][$i] );
                                          if($inst_routines-> checkurlFileImg($exp_img[0] ) != false){    
                                              $DATA['img'][$e]=$exp_img[0]  ;  
                                                // echo '--'. $DATA['img'][$e] ;
                                              $e++;
                                          }
                                     } else {
                                         if($inst_routines-> checkurlFileImg( ''.$inst_routines->parse_url($url, 'host').''.$matches[1][$i].'' ) != false){ 
                                            $DATA['img'][$e] = ''.$inst_routines->parse_url($url, 'host').''.$matches[1][$i].'';
                                            if(!empty($DATA['img'][$e] )){
                                                    if(substr($DATA['img'][$e] ,0,4) != 'http'){
                                                        if(substr($DATA['img'][$e] , 0, 8) == 'https://'){
                                                                $DATA['img'][$e] = 'https://'.$inst_routines->parse_url($url, 'host').''.$matches[1][$i].'';
                                                        }else{
                                                                $DATA['img'][$e] = 'http://'.$inst_routines->parse_url($url, 'host').''.$matches[1][$i].'';
                                                        }
                                                    }
                                            }
                                           // echo '--'. $DATA['img'][$e] ;
                                             $e++;
                                         }
                                     }

                                 }    
                         }
                         if(empty( $DATA['description'][0])){  $DATA['description'][0] = ''; }
                         if(empty( $DATA['title'][0])){  $DATA['title'][0] = ''; }
                        if(empty( $DATA['img'][0])){  $DATA['img'][0] = ''; }
                         
        return $DATA;
    }
      
    

    
    function SnapShotWebsite ($url, $width, $height){     
           $ContentWebsite['img'] ="http://s.wordpress.com/mshots/v1/".urlencode($url)."?w=".$width."&amp;h=".$height."";      
           // $ContentWebsite['img'] ="http://www.robothumb.com/src/http://".$url."@".$width."x".$height.".jpg";              
            return $ContentWebsite;
    }
	
    function GetImgWebSite($url){

        //$content=file_get_contents($url); 
       // return $content;
       if (isset($url)) {
                   //on veut recuperer le chemin, et la page
                   //on sépare donc les parties delimité par des /
                   $chemin_ex = explode("/", $url);
                   //on recupere le nombre de parties séparés
                   $nb = count($chemin_ex);
                   //si nb<=3 il n'yavait pas de séparateur / autre que dans http://
                   if ($nb <= 3) {
                       //le chemin est donc l'url complète.
                       $chemin = $url;
                       $page = '';
                   } else {
                       //le chemin correspond à l'url complete moins /dernier element, la page au dernier element
                       $page = $chemin_ex[$nb - 1];
                       $chemin_ex[$nb - 1] = '';
                       $chemin = implode("/", $chemin_ex);
                   }
                   //on récupère le html de la page que l'on veut avec file_get_contents()
                   $html = file_get_contents($chemin . $page);
                          //title
                         preg_match( "/<title>(.*)<\/title>/i", $html, $match );
                         $ContentWebSite['title'][0]  =   strip_tags($match[0]);

                   //l'expression régulière permettant de trouver les balise images
                   $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/Ui', $html, $matches);
                   //pour chacuns des element trouvés
                   for ($i = 0; $i < $output; $i++) {
                       //on affiche le contenus de la balise src.            
                       //et on affiche l'image correspondante.
                       //si ca commence pat http on affiche directement
                       if (substr($matches[1][$i], 0, 7) == 'http://' || substr($matches[1][$i], 0, 8) == 'https://') {
                       // $image== $matches[1][$i];
                             $ContentWebSite['img'][$i]  = $matches[1][$i];
                          // echo '<img src="' . $matches[1][$i] . '">';
                       } else {
                           //si l'image commence par / on doit supprimer le premier repertoire du chemin
                           if (substr($matches[1][$i], 0, 1) == '/') {
                               $image_tab = explode('/', $matches[1][$i]);
                               $image_tab[0]='';
                               $image_tab[1]='';
                               //$image=implode("/", $image_tab);
                                $ContentWebSite['img'][$i] =implode("/", $image_tab);
                           } else {
                               //sinon on la garde telle quelle et on rajoute le chemin general du site recherché
                              //$image=$matches[1][$i];
                               $ContentWebSite['img'][$i]  =$matches[1][$i];
                           }
                           //echo '<img src="' . $chemin . $image . '">';
                           //$ContentWebSite[$i] =''.$chemin.''.$image.'';
                          
                       }                     
                      
                   }
                   return $ContentWebSite;
        } else {
                   //par defaut remplir le formulaire avec l'url du blog
               //   $url = 'http://www.chezneg.fr/leblog/chezneg-leblog.php';
            return false;
       }     
    }
    
    
   function connectCrossPlatformSound($url){
                        $inst_routines= new core_Routines();
                        $type='';
                        $id = -1;
                        $titre = "";
                        $description = "";
                        $code = "";
                        $img = "";
                       
                        
                        if(preg_match("#soundcloud#",$url)) { 
                                    $client = new api_Services_Soundcloud(SD_APP_ID, SD_SECRET);
                                    $client->setCurlOptions(array(CURLOPT_FOLLOWLOCATION => 1));        
                                if(preg_match("#<iframe#",$url) ){                                     
                                     $type="soundcloudcode";   
                                }else{
                                 
                                    $type="soundcloud";
                                    
                                }   

                        }else if(preg_match("#8tracks#",$url)) { 
                           /*     if(eregi("<iframe",$url) ){                                     
                                     $type="8trackscode";   
                                }else{
                                 
                                    $type="8tracks";
                                    
                                }                     */            
                                
                        }elseif(preg_match("#grooveshark.com#",$url)) {
                            if(preg_match("#grooveshark.com/s/#",$url)) { //http://grooveshark.com/s/Newjack/7MH9U?src=5 -->  tracks
                                $type="grooveshark";
                               
                            }else if(preg_match("#grooveshark.com/profile/#",$url)) {     // //http://grooveshark.com/profile/Justice/22531697  -->artiste
                                 $type="groovesharkartiste";
                            }   
                            
                        }elseif(preg_match("#spotify#",$url)) { 
                                if(preg_match("#<iframe#",$url) ){                                     
                                     $type="spotifycode";  
                                }else if(preg_match("#open.spotify.com/track/#",$url) ){    
                                    $type="spotify";            
                                }else if(preg_match("#spotify:track:#",$url) ){    
                                    $type="spotifyuri";     
                                }else if(preg_match("#play.spotify.com/track/#",$url) ){    
                                    $type="spotifyplay";           
                                }                                   
                                
                        }else if(preg_match("#deezer#",$url)) {     
                           
                                if(preg_match("#<iframe#",$url) ){                                     
                                     $type="deezercode";                                    
                                }else if(preg_match("#www.deezer.com/track/#",$url) ){                   
                                    $type="deezer";
                                }else if(preg_match("#www.deezer.com/album/#",$url) ){                   
                                    $type="deezeralbum";         
                                }else if(preg_match("#www.deezer.com/playlist/#",$url) ){                   
                                    $type="deezerplaylist";     
                                }else if(preg_match("#www.deezer.com/artist/#",$url) ){                   
                                    $type="deezerartist";                                      
                                }
                                
                        } else{
                            return false;
                        }
                
                     if($type=="soundcloudcode") {
                                $debut_id = explode("src=",$url,2);                               
                                $p = explode('"', $debut_id[1]);                                  
                                $p2 = explode('/tracks/', $p[1]);
                                $p3 = explode('&', $p2[1]);
                                $id =  $p3[0];                                      
                              
                        }else  if($type=="soundcloud") {     
                               $track = json_decode($client->get('resolve', array('url' => $url)));
                               $id= $track->id;
                               
                        }else  if($type=="8trackscode") {                                  
                            /*    $debut_id = explode("src=",$url,2);                               
                                $p = explode('"', $debut_id[1]);                                  
                                $p2 = explode('/tracks/', $p[1]);
                                $p3 = explode('&', $p2[1]);
                                $id =  $p3[0];  */    
                        }else  if($type=="8tracks") {     
                            //<iframe src="http://8tracks.com/mixes/6722924/player_v3_universal" width="200" height="322" style="border: 0px none;"></iframe><p class="_8t_embed_p" style="font-size: 11px; line-height: 12px;"><a href="http://8tracks.com/headvches/all-i-wanna-be-is-whites-in-waves?utm_medium=trax_embed">all i wanna be is whites in waves</a> from <a href="http://8tracks.com/headvches?utm_medium=trax_embed">headvches</a> on <a href="http://8tracks.com?utm_medium=trax_embed">8tracks Radio</a>.</p>

                       //       $track = json_decode($client->get('resolve', array('url' => $url)));
                          //     $id= $track->id;                               
                              
                        }elseif($type =='spotify'){
                             $exp_url = explode('/', $url);
                             $id =$exp_url[4];
                             
                        }elseif($type =='spotifyuri'){
                             $exp_url = explode(':', $url);
                             $id =$exp_url[2];        
                        }elseif($type =='spotifyplay'){//https://play.spotify.com/track/2e6K7SrA9sKIJh7g4cu1li
                                     $exp_url = explode('/', $url);
                             $id =$exp_url[4];                            
                            
                    }elseif($type =='spotifycode'){   
                               $debut_id = explode("src=",$url,2);                               
                               $p = explode('"', $debut_id[1]);    
                               $id = $p[1];    
                                
                         }elseif($type =='deezer' or $type =='deezeralbum' or $type =='deezerplaylist' or $type =='deezerartist'){                                
                                $exp_url = explode('/', $url);
                                $id =$exp_url[4];
                                
                         }elseif( $type=="grooveshark"){                                
                                $exp_url = explode('/', $url);
                                $id =$exp_url[4];
                                                                
                                
                        }elseif($type =='deezercode'   ){             
                   
                               $debut_id = explode("src=",$url,2);                               
                                $p = explode('"', $debut_id[1]);                                  
                                $p2 = explode('/tracks/', $p[1]);
                                $p3 = explode('&', $p2[1]);
                                $id =  $p3[0];     
                                 $typeplay = 'tracks';
                                $p3 = explode('&', $p2[1]);
                                $id =  $p3[0];  
                            //    $type= 'deezer';
                         }

                        //Analyse et stockage des informations sur la piste audio
                        if($type=="soundcloud" or $type=="soundcloudcode" ) {   
                                            $json = file_get_contents('http://api.soundcloud.com/tracks/'.$id.'.json?client_id='.SD_APP_ID.'');
                                            $tracks_info = json_decode($json);  
                                     
                                            $exp_img = explode('?', $tracks_info->{'artwork_url'});          
                                            $img =   str_replace('-large', '-t500x500', $exp_img[0]);

                                            $titre = $tracks_info->{'title'};         
                                            $track_url  =  $tracks_info->{'permalink_url'};                       
                                            $description = $tracks_info->{'description'};                                     
                                            $embed_info = json_decode($client->get('oembed', array('url' => $track_url)));
                                            $code= $embed_info->html;    
                                   
                       }elseif($type =='spotify' or $type =='spotifycode' or  $type =='spotifyuri' or  $type =='spotifyplay'){      
                                    if($type =='spotifycode' ){
                                             $code ='<iframe src="'.$id.'" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>';
                                             
                                    }else{        
                                            $json =  $inst_routines->curl_decodeJSON( "https://embed.spotify.com/oembed/?url=spotify:track:".$id."&format=json");
                                            if(!empty($json->title)){ $titre = $json->title;  } else{ $titre = ''; }              
                                            if(!empty($json->thumbnail_url)){ $thumbnail_url = $json->thumbnail_url; }else{ $thumbnail_url = ''; }
                                            $content_html = '<img  alt="'.$titre.'"  src="'.stripslashes($thumbnail_url).'" style="-moz-border-radius: 3px; -webkit-border-radius: 3px;  border-radius:3px;cursor:pointer;position:relative;left:0px;margin-left:0px;width:95%;height:auto;">';
                                            $track_url = 'http://open.spotify.com/track/'.$id.'';  
                                            $code ='<iframe src="https://embed.spotify.com/?uri=spotify:track:'.$id.'" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>';
                                    }
                                    
                       }elseif($type == 'deezer' or  $type == 'deezeralbum'  or $type == 'deezerplaylist'  or $type == 'deezerartist' ){ 
                          
                               if($type == 'deezer'){
                                        
                                                $config = array(
                                                  "app_id"        => DE_APP_ID,
                                                  "app_secret"    => DE_SECRET,
                                                  "my_url"        => URL_RACINE_QHM
                                          );

                                          $dz = new api_Deezer($config); 
                                          $trackData = $dz->getTrack($id); 
                                          $titre = $trackData->artist->name.' - '.$trackData->title;
                                          $track_url =$trackData->link;
                                          
                                          $content_html = '<img  alt="'.$titre.'"  src="'.str_replace('http://', 'https://', stripslashes($trackData->artist->picture)).'" style="-moz-border-radius: 3px; -webkit-border-radius: 3px;  border-radius:3px;cursor:pointer;position:relative;left:0px;margin-left:0px;width:50%;height:auto;">';
                                          $code = '<iframe scrolling="no" frameborder="0" allowTransparency="true" src="https://www.deezer.com/plugins/player?format=square&autoplay=true&playlist=true&width=300&height=240&cover=true&type=tracks&id='.$id.'&title=" width="300" height="300"></iframe>';
                                    //     $code = '<iframe scrolling="no" frameborder="0" allowTransparency="true" src="http://www.deezer.com/plugins/player?format=square&autoplay=false&playlist=false&width=300&height=300&color=1990DB&layout=dark&size=medium&type=tracks&id=3159880&title=" width="300" height="300"></iframe>';
                                   }else if($type == 'deezeralbum'){                                   
                                         $json = file_get_contents('https://api.deezer.com/album/'.$id.'');
                                         $tracks_info = json_decode($json);  
                                         $titre = $tracks_info->{'artist'}->{'name'} .' - '.$tracks_info->{'title'};        
                                         $track_url  = stripslashes($tracks_info->{'link'});     
                                         $content_html = '<img  alt="'.$titre.'"  src="'.str_replace('http://', 'https://',stripslashes($tracks_info->{'cover'})).'" style="-moz-border-radius: 3px; -webkit-border-radius: 3px;  border-radius:3px;cursor:pointer;position:relative;left:0px;margin-left:0px;width:50%;height:auto;">';
                                         $code = '<iframe scrolling="no" frameborder="0" allowTransparency="true" src="https://www.deezer.com/plugins/player?format=square&autoplay=false&playlist=true&width=300&height=300&cover=true&type=album&id='.$id.'&title=" width="300" height="300"></iframe>';
                                         
                                  }else if($type == 'deezerplaylist'){                                   
                                         $json = file_get_contents('https://api.deezer.com/playlist/'.$id.'');
                                         $tracks_info = json_decode($json);  
                                         $titre = $tracks_info->{'creator'}->{'name'} .' - '.$tracks_info->{'title'};        
                                         $track_url  = stripslashes($tracks_info->{'link'});     
                                         $content_html = '<img  alt="'.$titre.'"  src="'.str_replace('http://', 'https://',stripslashes($tracks_info->{'picture'})).'" style="-moz-border-radius: 3px; -webkit-border-radius: 3px;  border-radius:3px;cursor:pointer;position:relative;left:0px;margin-left:0px;width:50%;height:auto;">';
                                         $code = '<iframe scrolling="no" frameborder="0" allowTransparency="true" src="https://www.deezer.com/plugins/player?format=square&autoplay=false&playlist=true&width=300&height=300&cover=true&type=playlist&id='.$id.'&title=" width="300" height="300"></iframe>';      
                                         
                                  }else if($type == 'deezerartist'){                                   
                                         $json = file_get_contents('https://api.deezer.com/artist/'.$id.'');
                                         $tracks_info = json_decode($json);  
                                         $titre = $tracks_info->{'name'};        
                                         $track_url  = stripslashes($tracks_info->{'link'});     
                                         $content_html = '<img  alt="'.$titre.'"  src="'.sstr_replace('http://', 'https://',stripslashes($tracks_info->{'picture'})).'" style="-moz-border-radius: 3px; -webkit-border-radius: 3px;  border-radius:3px;cursor:pointer;position:relative;left:0px;margin-left:0px;width:50%;height:auto;">';
                                         $code =  $content_html ;                                                          
                                   }

                                                          
                        }
                        if(empty($code)){
                            return false;
                        }else{
                           return array("id"=>$id,"type"=>$type,"titre"=>$titre,"description"=>  $description,"img"=>$img, "content_html"=>$content_html,  "code"=>$code,    'track_url' => $track_url);  
                        }
                            
                          
   }                    
                                function curl_decodeJSON($url){
                                                                        $ch = curl_init();
                                                                    curl_setopt($ch, CURLOPT_URL,$url);
                                                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                                    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; curl)");
                                                                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                                                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                                                    $json = curl_exec($ch);
                                                                    curl_close($ch);

                                                                    $json  = json_decode($json);
                                                                    return $json;
                                }
    
                               
                                function curl_get($url) {
                                        $curl = curl_init($url);
                                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                                        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                                        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
                                        $return = curl_exec($curl);
                                        curl_close($curl);
                                        return $return;
                                }

    function connectCrossPlatformVideo($url){
        $inst_routines= new core_Routines();
        if(empty($url_player)){ $url_player = ''; }
                        $type = "";
                        $id = -1;
                        $titre = "no title";
                        $description = "no description";
                        $code = "no code";
                        $img = "no image";
                        
                         if(preg_match("#youtube#",$url) ){  
                                if(preg_match("|<object|",$url) ){
                                     $type="youtubecodev1";
                                   
                                }else if(preg_match("#<iframe#",$url) ){      
                                     $type="youtubecodev2";
                                    
                                }else{
                                     $type="youtube";
                               }
                           
                        }else if(preg_match("#youtu.be#",$url) ){                                   
                            $type="youtubeSHARE";     
                            
                        }else if(preg_match("#vine.co#",$url) ){                                   
                                if(preg_match("#<iframe#",$url) ){                                     
                                     $type="vinecode";                                    
                                }else{
                                    $type="vine";
                                }                              
                            
                        }else if(preg_match("#dailymotion#",$url)){
                                if(preg_match("#<iframe#",$url) ){                                     
                                     $type="dailymotioncode";                                    
                                }else{
                                    $type="dailymotion";
                                }
                        }else if(preg_match("#dai.ly#",$url)){
                            $type="dailymotionSHARE";                            
                            
                        }else if(preg_match("#googleTEST#",$url)){
                           // $type="googleTEST";
                            
                        }else if(preg_match("#vimeo#",$url)) {
                                if(preg_match("#<object#",$url) ){
                                     $type="vimeocodev1";
                                   
                                }else if(preg_match("#<iframe#",$url) ){      
                                     $type="vimeocodev2";
                                    
                                }else{
                                     $type="vimeo";
                               }                            
                
                                
                        } else{
                            return false;
                        }
             
               
                   if(substr($url, 0,17) == 'https://youtu.be/'){   
                   
                         $id = str_replace('https://youtu.be/', '', $url);
                       
                    }elseif($type=="youtube"){
                            $debut_id = explode("v=",$url,2);
                             $id_et_fin_url = explode("&",$debut_id[1],2);
                             $id = $id_et_fin_url[0];                            
                               
                   
                            
                        } else if($type=="youtubecodev1"){        
                             $p1 = explode('param', $url);
                             $p2 = explode('=',  $p1[1]);
                             $p3 = explode('?',  $p2[2]);
                             $o= str_replace('"', '', $p3[0]);      
                             $debut_id = explode("v/", $o,2);
                             $id_et_fin_url = explode("&",$debut_id[1],2);
                             $id = $id_et_fin_url[0];
                             $url = 'https://www.youtube.com/watch?v='.$id.'';
                             
                        } else if($type=="youtubecodev2"){  
                             $debut_id = explode("src=",$url,2);                               
                             $p = explode('"', $debut_id[1]);                                  
                             $p2 = explode('/embed/', $p[1]);
                             $id =  $p2[1];   
                            $url = 'https://www.youtube.com/watch?v='.$id.'';
                            
                        } else if($type=="youtubeSHARE"){        
                             $id = str_replace('http://youtu.be/', '', $url);                 
                           
                       } else if($type=="dailymotion"){
                             $debut_id = explode("/video/",$url,2);
                             $id_et_fin_url = explode("_",$debut_id[1],2);
                             $id = $id_et_fin_url[0];        
                             
                        } else if($type=="vine"){                       
                             $debut_id = explode("/v/",$url,2);                           
                             $id = str_replace('/embed', '', $debut_id[1]);                               
                            
                        } else if($type=="dailymotionSHARE"){
                             $id = str_replace('http://dai.ly/', '', $url);   
                             
                         } else if($type=="dailymotioncode"){                                 
                              $debut_id = explode("src=",$url,2);                                 
                              $p = explode('"', $debut_id[1]);                              
                              $p2 = explode('/embed/video/', $p[1]);
                              $p3 = explode('?', $p2[1]);
                              $id =  $p3[0];                                
                              $url = 'http://www.dailymotion.com/video/'.$id.'';
                            
                         } else if($type=="vinecode"){                                       
                              $debut_id = explode("src=",$url,2);                                 
                              $p = explode('"', $debut_id[1]);                      
                              $p2 = explode('/', $p[1]);                                                   
                              $id =  $p2[4];                                                  
                                             
                        } else if($type=="vimeo"){
                            //https://vimeo.com/235709194
                         //   $ids = explode("/",$url);   
                           // $id = $ids[3];
                                   
                       //     echo '-'.$id;
                            $l_id= preg_match("([0-9]+)$",$url,$id);
                            $id = $lid[0];
                              
                        } else if($type=="vimeocodev1"){                              
                            $code = "no code";
                            
                        } else if($type=="vimeocodev2"){                            
                             $debut_id = explode("src=",$url,2);                               
                             $p = explode('"', $debut_id[1]);                                  
                             $p2 = explode('/video/', $p[1]);
                             $p3 = explode('?', $p2[1]);
                             $id =  $p3[0];           
                             $type ='vimeo';
                            
                        }

                        //Analyse et stockage des informations de la vidéo
                        if($type=="youtube" or $type=="youtubeSHARE" or $type=="youtubecodev1"   or $type=="youtubecodev2"){
                          //https://youtu.be/558GspwfUPU
                            $json = file_get_contents("http://www.youtube.com/oembed?url=".$url."&format=json");
                        
                              $video_info = json_decode($json);  
                              $url_ext = $url;
                             //   $code = stripslashes($video_info->{"html"});   
                              $titre =  stripslashes($video_info->{"title"});   
                              $img =  stripslashes($video_info->{"thumbnail_url"});
                              $code = '<iframe  src="//www.youtube-nocookie.com/embed/'.$id.'?controls=1&showinfo=0&autohide=1&wmode=transparent" frameborder="0" wmode="Opaque"  style="position:relative;left:0px;margin-left:0px;width:98%;height:415px;display:block;"></iframe>';
                                
                        } else if ($type=="dailymotion" or $type=="dailymotionSHARE" or $type=="dailymotioncode" ){
                                    $json = file_get_contents("http://www.dailymotion.com/services/oembed?url=".$url."");
                                     $video_info = json_decode($json);  
                                   
                                      $code = '<iframe frameborder="0"  src="//www.dailymotion.com/embed/video/'.$id.'?info=0" allowfullscreen  style="position:relative;left:0px;margin-left:0px;width:98%;height:415px;display:block;"></iframe>';
                                  //  $code = stripslashes($video_info->{"html"});   
                                      $img =  stripslashes($video_info->{"thumbnail_url"});
                                      $url_ext = 'http://www.dailymotion.com/video/'.$id.'';
                                      $titre =  stripslashes($video_info->{"title"});   
                                    // $description =  $video_info->{'description'};  
                                      
                        } else if ( $type=="vinecode" or  $type=="vine"){                                 
                                     $contentWebSite =$inst_routines->GetContentWebSite("https://vine.co/v/".$id."");                             
                                      $code = '<iframe frameborder="0"  src="https://vine.co/v/'.$id.'/embed/simple" allowfullscreen  style="position:relative;left:0px;margin-left:0px;width:98%;height:415px;display:block;"></iframe><script src="https://platform.vine.co/static/scripts/embed.js"></script>';
                
                                      $img = $contentWebSite['img'][0];
                                     
                                      $url_ext = 'https://vine.co/v/'.$id.'';
                                      $description = ' '.str_replace('"', '', $contentWebSite['description'][0]).'';
                                    //  $titre =  $description;
                                 
                                      
                                      
                                   
                     } else if ($type=="vimeo"){                                    
                                  $json = file_get_contents('http://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/'.$id.'&width=560');
                                     $video_info = json_decode($json);  
                                 //    $code= stripslashes($video_info->{'html'});
                                     $code ='<iframe src="https://player.vimeo.com/video/'.$id.'"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen  style="position:relative;left:0px;margin-left:0px;width:98%;height:415px;display:block;"></iframe>';
                                     $img =  stripslashes($video_info->{'thumbnail_url'});
                                     $url_ext = 'http://vimeo.com/channels/staffpicks/'.$id.'';
                                     $titre =  stripslashes($video_info->{'title'});   
                                     $description =  $video_info->{'description'};  

                        }
                        if($code == "no code"){
                            return false;
                        }else{
                            return array("id"=>$id,"type"=>$type,"titre"=>$titre,"description"=>  $description,"img"=>$img,"code"=>$code,  'url_player' => $url_player ,"url_ext"=>$url_ext);
                        }
}


    
    
    function parse_url($url,$index){
        /*
         *     [scheme] => http
    [host] => hostname
    [user] => username
    [pass] => password
    [path] => /path
    [query] => arg=value
    [fragment] => anchor
         * */
         $url_link =  parse_url($url );       
         return $url_link[$index];
    }
    
	 function encode_url_qhm($chaine) {
	
	
		$chaine = trim($chaine);
	
		$chaine = htmlentities($chaine, ENT_NOQUOTES, 'UTF-8');
	
		
		$chaine = preg_replace('#&([A-Za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $chaine);
	
		
		$chaine = preg_replace('#&([A-Za-z]{2})(?:lig);#', '\1', $chaine); 
	
	
		$chaine = preg_replace('#&[^;]+;#', '', $chaine);
	
		
		$chaine = preg_replace('/([^a-z0-9\/]+)/i', '-', $chaine);
	
				$chaine = strtolower($chaine);
	
	
		return $chaine;
	}
	
	 function format_img($img,$l,$h,$val){
                                                        $inst_routines= new core_Routines();
		 	$info_img = $inst_routines->imginfo($img);
		 	$l = $info_img1['largeur'];
		 	if($info_img['largeur'] > $val) $l = $val;
		 	$h = $val;
		 	$reformat_img = $inst_routines-> resizeIMG($img, $l, $h);	 
		 	return $reformat_img;
	 }
         
	
	
	function downloadurlFileImg($url,$file){
		
		$ch = curl_init($url);
		$fp = fopen($file, 'wb');
		
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);		
                
	}
	
	function checkurlFileImg($url)	{
                				//Vérification de l'extention du fichier
                				$verif = mb_ereg("^(.+)\.(.+)$", $url, $items);
                                                                                       if(empty($items[1])){ $items[1] = ''; }
                                                                                       if(empty($items[2])){ $items[2] = ''; }
                				//ici l'url sans l'extension du fichier
                				$sans_extension=$items[1] ;
                				//ici l'extension du fichier
								$avec_extension=$items[2] ;           				
               				$extension_autorise = array("png","gif","jpg","jpeg","JPG","bmp"); 
                			
                				if (!empty($url) && !(in_array($avec_extension, $extension_autorise))){
										return false;
								}else{
										return $avec_extension;
								}
	}
        
        	function checkurlFileplVideo2($url)	{
                				//Vérification de l'extention du fichier
                				$verif = mb_ereg("^(.+)\.(.+)$", $url, $items);
                                                                                       if(empty($items[1])){ $items[1] = ''; }
                                                                                       if(empty($items[2])){ $items[2] = ''; }
                				//ici l'url sans l'extension du fichier
                				$sans_extension=$items[1] ;
                				//ici l'extension du fichier
								$avec_extension=$items[2] ;           				
               					$extension_autorise = array("mp4","flv","mov"); 
                			
                				if (!empty($url) && !(in_array($avec_extension, $extension_autorise))){
										return false;
								}else{
										return $avec_extension;
								}
	}
        
        
	function checkurlFileAudio($file)	{
                				//Vérification de l'extention du fichier
                				$verif = mb_ereg("^(.+)\.(.+)$", $file, $items);
                				//ici l'url sans l'extension du fichier
                				$sans_extension=$items[1] ;
                				//ici l'extension du fichier
					$avec_extension=$items[2] ;           				
               					$extension_autorise = array("mp3"); 
                			
                				if (!empty($url) && !(in_array($avec_extension, $extension_autorise))){
						return false;
					}else{
						return $avec_extension;
					}
	}        
	
	function AuPluriel($chiffre) {
		if($chiffre>1) {
			return ''.PLURIEL_AFF.'';
		};
	}	
        
	function Word_fr_Pluriel($chiffre,$word) {
		
                                                        if($word == 'la'){
                                                                    if($chiffre>1) {
                                                                     return 'les';
                                                                    }elseif($chiffre== 1) {
                                                                        return 'la';
                                                                    }
                                                           
                                                        }elseif($word == "La" ){
                                                                       if($chiffre>1) {
                                                                     return 'Les';
                                                                    }elseif($chiffre== 1) {
                                                                        return 'La';
                                                                    }
                                                      
                                                        
                                                        }elseif($word == "Une" ){
                                                                       if($chiffre>1) {
                                                                     return 'Des';
                                                                    }elseif($chiffre== 1) {
                                                                        return 'Une';
                                                                    }
                                                        }                                                        
	
	}        
	
	function auto_sizeWimg($width,$height,$img){
                                    $inst_routines= new core_Routines();
		$size_img = $inst_routines->ratio_size_img($width,$height,''.$img.'');
		$W=$width;
		if($size_img ['H'] < $height){
			$P = $width - $size_img ['H'];
			$W = ($width + 15) + $P;
		}		
		$data['width'] = $W;		
		return $W;		
	}
	
             function calctimeSTAT($date_now,$date_start){    
            
                                           $inst_routines= new core_Routines();
                                           $formatter = new IntlDateFormatter(''.$_GET['lg'].'_'.CONFIGDB_SERVER_COUNTRY.'',IntlDateFormatter::MEDIUM, IntlDateFormatter::NONE, 'Europe/Paris',IntlDateFormatter::GREGORIAN );
		$dateFrom = new DateTime(''.$date_start.'');
		$dateNow = new DateTime(''.$date_now.'');
		$interval = $dateNow->diff($dateFrom);
		//return $interval->format('%y ans %m mois %d jours %h heures %i minutes %s secondes');
		$sec =  $interval->format('%s');
		$min =  $interval->format(' %i ');
		$heure =  $interval->format('%h');
		$jour =  $interval->format(' %d');
		$mois =  $interval->format(' %m');
		$year =  $interval->format(' %y');
		
		if($year > 0){
                                                                $DATAtime['type'] = 'year';
                                                                $DATAtime['value'] = $year;
                                                                return $DATAtime;
				
		}elseif($mois > 0 ){
                                                            
                                                                $DATAtime['type'] = 'months';
                                                                $DATAtime['value'] = $mois;
                                                                return $DATAtime;
				
		}elseif($jour > 0){
                                                        
                                                                $DATAtime['type'] = 'days';
                                                                $DATAtime['value'] = $jour;
                                                                return $DATAtime;	
                        
		}elseif($heure > 0){
				return  ' '.$heure.' '.TIME.'';		
		}elseif($min > 0){
				return ' '.$min.' '.MINUTE.'';
		}elseif($min > 0){
				return  ' '.$sec.' '.SECOND.'';				
		}else{
                                                                $DATAtime['type'] = 'days';
                                                                $DATAtime['value'] = 1;
			return $DATAtime;
		}		
	}
        
                       function calctime($date_now,$date_start){    
            
                                    $inst_routines= new core_Routines();
                                    $formatter = new IntlDateFormatter(''.$_GET['lg'].'_'.CONFIGDB_SERVER_COUNTRY.'',IntlDateFormatter::MEDIUM, IntlDateFormatter::NONE, 'Europe/Paris',IntlDateFormatter::GREGORIAN );
		$dateFrom = new DateTime(''.$date_start.'');
		$dateNow = new DateTime(''.$date_now.'');
		$interval = $dateNow->diff($dateFrom);
		//return $interval->format('%y ans %m mois %d jours %h heures %i minutes %s secondes');
		$sec =  $interval->format('%s');
		$min =  $interval->format(' %i ');
		$heure =  $interval->format('%h');
		$jour =  $interval->format(' %d');
		$mois =  $interval->format(' %m');
		$year =  $interval->format(' %y');
		
		if($year > 0){
				return ' '.$year. ' '.YEAR_DATE.''.$inst_routines->AuPluriel($year).' '.AGO_DATE.'';
		}elseif($mois > 0 ){
                                                                return ' '.$formatter->format($dateFrom).''; //  10 july 2015 to 8:22 p.m.
                                                            
				//return 'il y a '.$mois.' mois';
		}elseif($jour > 0){
                                                            if($jour > 5){        
                                                               // str_replace(':', SHUT_TIME_DATE, $dateFrom->format(''.FORMAT_TIME24_LG.':i '.FORMAT_TIMEAMPM_LG.''));
                                                                return ' '.$formatter->format($dateFrom).''; //  7 August 2015 at 2:27 p.m.  date("F j, Y, g:i a")                                                                
                                                          
                                                            }  else {
                                                                return ' '.$jour.' '.DAYS.''.$inst_routines->AuPluriel($jour).'';  // two days ago at 8:38
                                                            }
                                                           
				
		}elseif($heure > 0){
				return  ' '.$heure.' '.TIME.'';		
		}elseif($min > 0){
				return ' '.$min.' '.MINUTE.'';
		}elseif($min > 0){
				return  ' '.$sec.' '.SECOND.'';				
		}else{
			return ' '.INSTANTLY.'';
		}		
	}
        
                function convert1224TIME($time){        
                               // $formatter = new IntlDateFormatter(''.$_GET['lg'].'_FR',IntlDateFormatter::LONG, IntlDateFormatter::NONE, 'Europe/Paris',IntlDateFormatter::GREGORIAN );
		$TimeFrom = new DateTime(''.$time.'');
                                return  $TimeFrom->format('H:i');
                              //  return str_replace(':', SHUT_TIME_DATE, $TimeFrom->format(''.FORMAT_TIME24_LG.':i '.FORMAT_TIMEAMPM_LG.''));
                }
        
                function ListTIME($time){        
                               // $formatter = new IntlDateFormatter(''.$_GET['lg'].'_'.CONFIGDB_SERVER_COUNTRY.'',IntlDateFormatter::LONG, IntlDateFormatter::NONE, 'Europe/Paris',IntlDateFormatter::GREGORIAN );
		$TimeFrom = new DateTime(''.$time.'');
                                return  $TimeFrom->format(''.FORMAT_TIME24_LG.':i '.FORMAT_TIMEAMPM_LG.'');
                }
	
              function ListMois($lg,$format,$num_mois) {
                                 $formatter = new IntlDateFormatter(''.$lg.'_'.CONFIGDB_SERVER_COUNTRY.'',IntlDateFormatter::MEDIUM, IntlDateFormatter::NONE, 'Europe/Paris',IntlDateFormatter::GREGORIAN );    
                             /*
                              * ‘MMMM’ : nom de mois complet (comme dans l’exemple)
                                    ‘MMM’ : nom de mois raccourci, par exemple Jan, Fev…
                              */
                                 if($format == 'norm'){
                                          $formatter->setPattern('MMMM'); 
                                 }else{
                                           $formatter->setPattern('MMM'); 
                                 }
                                                                                                        
                                 $mois = $formatter->format(mktime(0, 0, 0, $num_mois, 1, 1970));    
                                 return    htmlspecialchars($mois);
               }
                
               
            function dateQHM($date,$lg,$format) {
               
                      if($format == 'norm'){
                                $formatter = new IntlDateFormatter(''.$lg.'_'.CONFIGDB_SERVER_COUNTRY.'',IntlDateFormatter::FULL, IntlDateFormatter::NONE, 'Europe/Paris',IntlDateFormatter::GREGORIAN );    
                                 $dateFrom = new DateTime(''.$date.'');   
                                 return $formatter->format($dateFrom);
                      }elseif($format == 'mini'){
                                $formatter = new IntlDateFormatter(''.$lg.'_'.CONFIGDB_SERVER_COUNTRY.'',IntlDateFormatter::LONG, IntlDateFormatter::NONE, 'Europe/Paris',IntlDateFormatter::GREGORIAN );    
                                 $dateFrom = new DateTime(''.$date.'');   
                                 return $formatter->format($dateFrom);
                      }elseif($format =='short'){
                                $formatter = new IntlDateFormatter(''.$lg.'_'.CONFIGDB_SERVER_COUNTRY.'',IntlDateFormatter::MEDIUM, IntlDateFormatter::NONE, 'Europe/Paris',IntlDateFormatter::GREGORIAN );   
                                 $dateFrom = new DateTime(''.$date.'');   
                                 return $formatter->format($dateFrom);
                      }elseif($format == 'time'){
                                $formatter = new IntlDateFormatter(''.$lg.'_'.CONFIGDB_SERVER_COUNTRY.'',IntlDateFormatter::LONG, IntlDateFormatter::NONE, 'Europe/Paris',IntlDateFormatter::GREGORIAN );  
                                 $dateFrom = new DateTime(''.$date.'');   
                                //str_replace(':', SHUT_TIME_DATE, $dateFrom->format(''.FORMAT_TIME24_LG.':i '.FORMAT_TIMEAMPM_LG.''))
                                 return str_replace(':', SHUT_TIME_DATE, $dateFrom->format(''.FORMAT_TIME24_LG.':i '.FORMAT_TIMEAMPM_LG.''));                            
                      }
                    
                }
    
	
        
	function imginfo($chemin){
		if(!empty($chemin)){
			$infos_image = @getImageSize($chemin); // info sur la dimension de l'image
			
			// '@' est placé devant la fonction getImageSize()pour empecher l'affichage
			// des erreurs si l'image est absente.
			 
			//dimension
			$imginfo['largeur'] = $infos_image[0]; // largeur de l'image
			$imginfo['hauteur'] = $infos_image[1]; // hauteur de l'image
			$imginfo['type']    = $infos_image[2]; // Type de l'image
			$imginfo['html']    = $infos_image[3]; // info html de type width="468" height="60"
			
			return $imginfo;
			
		}else{
			return false;
		}
			
		/*echo $largeur; // affiche la hauteur
		echo $hauteur; // affiche la largeur
		echo $type; // Type de l'image 1 = GIF, 2 = JPG,3 = PNG, 4 = SWF, 5 = PSD,
		// 6 = BMP, 7 = TIFF, 8 = TIFF, 9 = JPC, 10 = JP2, 11 = JPX,
		// 12 = JB2, 13 = SWC, 14 = IFF....*/
	}
	
	
	function ratio_size_img($W_max,$H_max,$img){
                                        $inst_routines= new core_Routines();
		$info_img = $inst_routines->imginfo(''.$img.'');
		$W_Src = $info_img['largeur'];
		$H_Src =$info_img['hauteur'];
		if ($W_max!=0 && $H_max!=0) {
			$ratiox = $W_Src / $W_max; // ratio en largeur
			$ratioy = $H_Src / $H_max; // ratio en hauteur
			$ratio = max($ratiox,$ratioy); // le plus grand
			$W = $W_Src/$ratio;
			$H = $H_Src/$ratio;
			$condition = ($W_Src>$W) || ($W_Src>$H); // 1 si vrai (true)
		}
		// ------------------------
		// B- HAUTEUR maxi fixe
		if ($W_max==0 && $H_max!=0) {
			$H = $H_max;
			$W = $H * ($W_Src / $H_Src);
			$condition = ($H_Src > $H_max); // 1 si vrai (true)
		}
		// ------------------------
		// C- LARGEUR maxi fixe
		if ($W_max!=0 && $H_max==0) {
			$W = $W_max;
			$H = $W * ($H_Src / $W_Src);
			$condition = ($W_Src > $W_max); // 1 si vrai (true)
		}
		$data['W'] = $W;
		$data['H'] = $H;
		return $data;
	}
	
	function resizeIMG_data($img_Src, $W_max, $H_max) {
	
		//	if (file_exists($img_Src)) {
		// ---------------------
		// Lit les dimensions de l'image source
		$img_size = getimagesize($img_Src);
		$W_Src = $img_size[0]; // largeur source
		$H_Src = $img_size[1]; // hauteur source
		// ---------------------
		if(!$W_max) {
			$W_max = 0;
		}
		if(!$H_max) {
			$H_max = 0;
		}
		// ---------------------
		// Teste les dimensions tenant dans la zone
		$W_test = round($W_Src * ($H_max / $H_Src));
		$H_test = round($H_Src * ($W_max / $W_Src));
		// ---------------------
		// si l'image est plus petite que la zone
		if($W_Src<$W_max && $H_Src<$H_max) {
			$W = $W_Src;
			$H = $H_Src;
			// sinon si $W_max et $H_max non definis
		} elseif($W_max==0 && $H_max==0) {
			$W = $W_Src;
			$H = $H_Src;
			// sinon si $W_max libre
		} elseif($W_max==0) {
			$W = $W_test;
			$H = $H_max;
			// sinon si $H_max libre
		} elseif($H_max==0) {
			$W = $W_max;
			$H = $H_test;
			// sinon les dimensions qui tiennent dans la zone
		} elseif($H_test > $H_max) {
			$W = $W_test;
			$H = $H_max;
		} else {
			$W = $W_max;
			$H = $H_test;
		}
		// ---------------------
		//	} else { // si le fichier image n existe pas
		//		$W = 0;
		//		$H = 0;
		//	}
		// ---------------------------------------------------
		// Affiche : src="..." width="..." height="..." pour la balise img
		//echo ' src="'.$img_Src.'" width="'.$W.'" height="'.$H.'"';
		$data['W'] = $W;
		$data['H'] = $H;
		
		return $data;
		// ---------------------------------------------------
	}	
	
	function resizeIMG($img_Src, $W_max, $H_max) {
	
	//	if (file_exists($img_Src)) {
			// ---------------------
			// Lit les dimensions de l'image source
			$img_size = getimagesize($img_Src);
			$W_Src = $img_size[0]; // largeur source
			$H_Src = $img_size[1]; // hauteur source
			// ---------------------
			if(!$W_max) {
				$W_max = 0;
			}
			if(!$H_max) {
				$H_max = 0;
			}
			// ---------------------
			// Teste les dimensions tenant dans la zone
			$W_test = round($W_Src * ($H_max / $H_Src));
			$H_test = round($H_Src * ($W_max / $W_Src));
			// ---------------------
			// si l'image est plus petite que la zone
			if($W_Src<$W_max && $H_Src<$H_max) {
				$W = $W_Src;
				$H = $H_Src;
				// sinon si $W_max et $H_max non definis
			} elseif($W_max==0 && $H_max==0) {
				$W = $W_Src;
				$H = $H_Src;
				// sinon si $W_max libre
			} elseif($W_max==0) {
				$W = $W_test;
				$H = $H_max;
				// sinon si $H_max libre
			} elseif($H_max==0) {
				$W = $W_max;
				$H = $H_test;
				// sinon les dimensions qui tiennent dans la zone
			} elseif($H_test > $H_max) {
				$W = $W_test;
				$H = $H_max;
			} else {
				$W = $W_max;
				$H = $H_test;
			}
			// ---------------------
	//	} else { // si le fichier image n existe pas
	//		$W = 0;
	//		$H = 0;
	//	}
		// ---------------------------------------------------
		// Affiche : src="..." width="..." height="..." pour la balise img
		echo ' src="'.$img_Src.'" width="'.$W.'" height="'.$H.'"';
		// ---------------------------------------------------
	}
	
	function Pourcentage($Nombre, $Total) {
		return $Nombre * 100 / $Total;
	}
	
	function MontantTTC($valeur, $taux, $arrondi){
		$MontantTTC = round($valeur + $valeur * $taux / 100, $arrondi);
		return $MontantTTC;
	}
	function MontantHT($valeur, $taux, $arrondi){
		$MontantHT = round($valeur / (1 + $taux / 100), $arrondi);
		return $MontantHT;
	}
	
	
	function expl_field($label,$field){
		$expl = explode('<'.$label.'>',$field);
		return $expl[0];
	}
	
	function strchar($char,$string){
		$pos = strpos($string, $char);		
		if($pos === false){
			return false;
			
		}else{
			return $pos;

		}		
	}
	
    function opendir_tools($chemin,$ope){       
           unlink($chemin); // On efface.
    }

    function uploadImage($fileName, $maxSize, $maxW, $fullPath, $relPath, $colorR, $colorG, $colorB, $front_name, $maxH = null ){
          
               $folder = $relPath;
            $maxlimit = $maxSize;
            $allowed_ext = "jpg,jpeg,gif,png,bmp";
         
            $match = "";
            $filesize = $_FILES[$fileName]['size'];

            if($filesize > 0){
            		
                    $filename = strtolower($_FILES[$fileName]['name']);
                    $filename = preg_replace('/\s/', '_', $filename);
                  
                    if($filesize < 1){
                            $errorList[] = "File size is empty.";
                    }
                    if($filesize > $maxlimit){
                            $errorList[] = "File size is too big.";
                    }

             if(count($errorList)<1){
                            $file_ext = preg_split("/\./",$filename);
                            $allowed_ext = preg_split("/\,/",$allowed_ext);
                            foreach($allowed_ext as $ext){
                                    if($ext==end($file_ext)){
                                            $match = "1"; // File is allowed
                                            $NUM = time();                                            
                                           // $sub_name = "_".substr($file_ext[0], 0, 15)."";
                                            $newfilename = $front_name."qhm".$NUM.".".end($file_ext); //-- namefile
                                          
                                          //  $newfilename = $front_name.".jpg"; //-- namefile
                                            $filetype = end($file_ext);
                                            
                                            $save = $folder.$newfilename;
                                          
                                            if(!file_exists($save)){
                                              
                                                    list($width_orig, $height_orig) = getimagesize($_FILES[$fileName]['tmp_name']);
                                                    
                                                    if($maxH == null){
                                                            if($width_orig < $maxW){
                                                                    $fwidth = $width_orig;
                                                            }else{
                                                                    $fwidth = $maxW;
                                                            }
                                                            $ratio_orig = $width_orig/$height_orig;
                                                            $fheight = $fwidth/$ratio_orig;

                                                            $blank_height = $fheight;
                                                            $top_offset = 0;

                                                    }else{
                                                            if($width_orig <= $maxW && $height_orig <= $maxH){
                                                                    $fheight = $height_orig;
                                                                    $fwidth = $width_orig;
                                                            }else{
                                                                    if($width_orig > $maxW){
                                                                            $ratio = ($width_orig / $maxW);
                                                                            $fwidth = $maxW;
                                                                            $fheight = ($height_orig / $ratio);
                                                                            if($fheight > $maxH){
                                                                                    $ratio = ($fheight / $maxH);
                                                                                    $fheight = $maxH;
                                                                                    $fwidth = ($fwidth / $ratio);
                                                                            }
                                                                    }
                                                                    if($height_orig > $maxH){
                                                                            $ratio = ($height_orig / $maxH);
                                                                            $fheight = $maxH;
                                                                            $fwidth = ($width_orig / $ratio);
                                                                            if($fwidth > $maxW){
                                                                                    $ratio = ($fwidth / $maxW);
                                                                                    $fwidth = $maxW;
                                                                                    $fheight = ($fheight / $ratio);
                                                                            }
                                                                    }
                                                            }
                                                            if($fheight == 0 || $fwidth == 0 || $height_orig == 0 || $width_orig == 0){
                                                                    die("FATAL ERROR REPORT ERROR CODE ");
                                                            }
                                                            if($fheight < 45){
                                                                    $blank_height = 45;
                                                                    $top_offset = round(($blank_height - $fheight)/2);
                                                            }else{
                                                                    $blank_height = $fheight;
                                                            }
                                                    }
                                                      
                                                    $image_p = imagecreatetruecolor($fwidth, $blank_height);
                                                    $white = imagecolorallocate($image_p, $colorR, $colorG, $colorB);
                                                    imagefill($image_p, 0, 0, $white);
                                                    switch($filetype){
                                                            case "gif":
                                                                    $image = @imagecreatefromgif($_FILES[$fileName]['tmp_name']);
                                                            break;
                                                            case "jpg":
                                                                    $image = @imagecreatefromjpeg($_FILES[$fileName]['tmp_name']);
                                                            break;
                                                            case "jpeg":
                                                                    $image = @imagecreatefromjpeg($_FILES[$fileName]['tmp_name']);
                                                            break;
                                                            case "png":
                                                                    $image = @imagecreatefrompng($_FILES[$fileName]['tmp_name']);
                                                            break;
                                                    }
                                                @imagecopyresampled($image_p, $image, 0, $top_offset, 0, 0, $fwidth, $fheight, $width_orig, $height_orig);
                                                  
                                                    switch($filetype){
                                                            case "gif":
                                                                    if(!@imagegif($image_p, $save)){
                                                                            $errorList[]= "PERMISSION DENIED [GIF]";
                                                                    }
                                                            break;
                                                            case "jpg":
                                                                    if(!@imagejpeg($image_p, $save, 100)){
                                                                            $errorList[]= "PERMISSION DENIED [JPG]";
                                                                    }
                                                            break;
                                                            case "jpeg":
                                                                    if(!@imagejpeg($image_p, $save, 100)){
                                                                            $errorList[]= "PERMISSION DENIED [JPEG]";
                                                                    }
                                                            break;
                                                            case "png":
                                                                    if(!@imagepng($image_p, $save, 0)){
                                                                            $errorList[]= "PERMISSION DENIED [PNG]";
                                                                    }
                                                            break;
                                                    }
                                                    @imagedestroy($filename);
                                            }else{
                                                    $errorList[]= "CANNOT MAKE IMAGE IT ALREADY EXISTS";
                                            }
                                    }
                            }
                    }
            }else{
                    $errorList[]= "NO FILE SELECTED";
            }
            if(!$match){
                    $errorList[]= "File type isn't allowed: $filename";
            }
            if(sizeof(@$errorList) == 0){
                    return $fullPath.$newfilename;
            }else{
                    $eMessage = array();
                    for ($x=0; $x<sizeof($errorList); $x++){
                            $eMessage[] = $errorList[$x];
                    }
                    return $eMessage;
            }
    }

    function remove_accents($str, $charset='utf-8'){
        $str = htmlentities($str, ENT_NOQUOTES, $charset);

        $str = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères

        return $str;
    }

        function _removeAccents ($text) {
                $alphabet = array(
                    'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
                    'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
                    'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
                    'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
                    'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e','ẽ'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
                    'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
                    'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y',','=>'','-'=>'','.' => '', 'ƒ'=>'f',
                );
         
                $text = strtr ($text, $alphabet);
         
                // replace all non letters or digits by -
                $text = preg_replace('/\W+/', '-', $text);
         
                return $text;
            }
    
    function stripAccents($string){
        /*    return strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝÑñ',
                         'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUYNn');*/
                         
        $texte = strtr(
            $string, 
            '@ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
            'aAAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy'
        );                 
        return $texte;        
    }

    function trimOver($char){
    	$tabCar = array(" ", "\t", "\n", "\r", "\0", "\x0B", "\xA0");
    	$trimovert = str_replace($tabCar, array(), $char);
        return $trimovert;
    }
    




    function type_browser(){
        $agent = $_SERVER['HTTP_USER_AGENT'];

            if(preg_match('/Firefox/i',$agent)) $config_browser = 'Firefox'; 
            elseif(preg_match('/Mac/i',$agent)) $config_browser = 'Mac';
            elseif(preg_match('/Chrome/i',$agent)) $config_browser = 'Chrome'; 
            elseif(preg_match('/Opera/i',$agent)) $config_browser = 'Opera'; 
            elseif(preg_match('/MSIE/i',$agent)) $config_browser = 'IE'; 
            else $config_browser = 'Unknown';        
        
            
  
        return $config_browser;
    }

    
    function type_OS(){
        
        if( preg_match("@Windows Phone@", getenv("HTTP_USER_AGENT"))  ){
             $os = "windowsPhone";
        }elseif (stristr($_SERVER['HTTP_USER_AGENT'], "iPhone") || strpos($_SERVER['HTTP_USER_AGENT'], "iPod")) {  
                $os = "iOS";
        }elseif (preg_match("@iPad@", getenv("HTTP_USER_AGENT"))  ){
                	$os = "iPad";                
        }elseif (preg_match("@blackberry@", getenv("HTTP_USER_AGENT")) ){
                $os = "blackberry";
        }elseif (preg_match("/phone/i", getenv("HTTP_USER_AGENT"))){
                $os = "WindowsPhone";                
        }elseif (preg_match("@Win@", getenv("HTTP_USER_AGENT"))){
                $os = "Windows";
        }elseif ((preg_match("@Mac@", getenv("HTTP_USER_AGENT"))) || (preg_match("@PPC@", getenv("HTTP_USER_AGENT")))){
                $os = "Mac";
        }elseif (preg_match("@Android@", getenv("HTTP_USER_AGENT"))){
                $os = "Android";
        }elseif (preg_match("@Linux@", getenv("HTTP_USER_AGENT"))){
                $os = "Linux";
        }elseif (preg_match("@FreeBSD@", getenv("HTTP_USER_AGENT"))){
                $os = "FreeBSD";
        }elseif (preg_match("@SunOS@", getenv("HTTP_USER_AGENT"))){
                $os = "SunOS";
        }elseif (preg_match("@IRIX@", getenv("HTTP_USER_AGENT"))){
                $os = "IRIX";
        }elseif (preg_match("@BeOS@", getenv("HTTP_USER_AGENT"))){
                $os = "BeOS";
        }elseif (preg_match("@OS/2@", getenv("HTTP_USER_AGENT"))){
                $os = "OS/2";

        }elseif (preg_match("@AIX@", getenv("HTTP_USER_AGENT"))){
                $os = "AIX";
        }else{
                $os = "Autre";
        }
         return $os;
    }
    
    function send_mail ($destinataire,$contents,$type,$from,$strfrom,$from_mail,$files) {	
                    $strfrom= '';
		/* destinataire */
	$to  = "<$destinataire>";
		/* sujet */
	$subject = "$from";
                  //   $headers='';
		/* MESSAGE */
                    if ($files == true){
                                   include("$contents");
                    }else{
                        $message = $contents;

                    }
		/*  format HTML. */
	if ($type == "html"){
                                        $headers = array( 'MIME-Version'=>'1.0', 'Content-type'=>'text/html; charset='.CHARSET, 'From' => 'bonjour@talkag.com', 'Reply-To'=> 'bonjour@talkag.com','X-Mailer' => 'PHP/' . phpversion());
			//$headers  = "MIME-Version: 1.0\r\n";
		//	$headers .= "Content-type: text/html; charset=".CHARSET."; \r\n";
	}else{
		/* D'autres en-t�tes */
                                        $headers = array( 'From' => 'bonjour@talkag.com', 'Reply-To'=> 'bonjour@talkag.com','X-Mailer' => 'PHP/' . phpversion());
            }
			//$headers .= "To: <$destinataire>\r\n";
		//	$headers .= "From: $from_mail '.$from_mail.'\r\n";
                                             //                   $headers .=  "Reply-to: noreply@srp.cloud";
                                                              //  $headers .="X-Mailer:";
			/*$headers .= "Cc: \r\n";*/
			/*$headers .= "Bcc: \r\n";*/

	mail($to, $subject, $message, $headers);
    }



    function send_mailATT($destinataire, $contents, $type, $from, $str_from, $from_mail, $files, $format, $rep) {
        //--> destinataire
        $to = "<" . $destinataire . ">";
        //--> sujet
        $subject = "$from";
        //--> format HTML.
        if ($type == "html") {
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1; \r\n";
        }
        //--> fichier.
        if ($files != "") {

            $headers .= 'Content-Type: ' . $rep . '' . $format . '; name="' . $files . '.' . $format . '"' . "\n";
            $headers .= 'Content-Transfer-Encoding: base64' . "\n";
            $headers .= 'Content-Disposition:attachement; filename="' . $files . '.' . $format . '"' . "\n\n";
            $headers .= chunk_split(base64_encode(file_get_contents('' . $files . '.' . $format . ''))) . "\n";
        }

        $headers .= "To: <" . $destinataire . ">\r\n";
        $headers .= "From: " . $str_from . " <" . $from_mail . ">\r\n";

        mail($to, $subject, $message, $headers);
    }

    function test_formatmail($email) {
        $atom = '[-a-z0-9!#$%&\'*+\\/=?^_`{|}~]';
        $domain = '([a-z0-9]([-a-z0-9]*[a-z0-9]+)?)';
        $regex = '/^' . $atom . '+' .
                '(\.' . $atom . '+)*' .
                '@' .
                '(' . $domain . '{1,63}\.)+' .
                $domain . '{2,63}$/i';

        // test de l'adresse e-mail
        if (preg_match($regex, $email)) {
            return true;
        } else {
            return false;
        }
    }

   function GenerationCle($Texte,$CleDEncryptage){
          $CleDEncryptage = md5($CleDEncryptage);
          $Compteur=0;
          $VariableTemp = "";
          for ($Ctr=0;$Ctr<strlen($Texte);$Ctr++){
                if ($Compteur==strlen($CleDEncryptage))
                $Compteur=0;
                $VariableTemp.= substr($Texte,$Ctr,1) ^ substr($CleDEncryptage,$Compteur,1);
                $Compteur++;
          }
          return $VariableTemp;
    }

    function Crypte($Texte,$Cle){
          srand((double)microtime()*1000000);
          $CleDEncryptage = md5(rand(0,32000) );
          $Compteur=0;
          $VariableTemp = "";
          for ($Ctr=0;$Ctr<strlen($Texte);$Ctr++){
                if ($Compteur==strlen($CleDEncryptage))
                $Compteur=0;
                $VariableTemp.= substr($CleDEncryptage,$Compteur,1).(substr($Texte,$Ctr,1) ^ substr($CleDEncryptage,$Compteur,1) );
                $Compteur++;
          }
          return base64_encode($this->GenerationCle($VariableTemp,$Cle) );
          
    }

    function Decrypte($Texte,$Cle) {
          $Texte = $this->GenerationCle(base64_decode($Texte),$Cle);
          $VariableTemp = "";
          for ($Ctr=0;$Ctr<strlen($Texte);$Ctr++){
                $md5 = substr($Texte,$Ctr,1);
                $Ctr++;
                $VariableTemp.= (substr($Texte,$Ctr,1) ^ $md5);
          }
          return $VariableTemp;
     }

function check_connect($host,$path){
//verifie la validite de l'adresse, c'est a dire on regarde si le site existe
// bien...
//on rend dans un tableau :
// "statut" : 0 si KO, 1 si redirect ou bien pour faire passer en local, 2 si OK
// "code" : code HTTP
// "message" : message

   /* if(!TEST_URL) {
    //si on travaille en local, on ne se connecte pas
        $tab_return["statut"] = 1;
        $tab_return["code"] = 0;
        $tab_return["message"] = "Pas de test de connexion\n";
        return $tab_return;
    }*/
    $connect = 0;
    $no_code = 0;
    //connexion par socket
    if ($fp = @fsockopen($host,80)) {
        //traitement du path
        if(substr($path,strlen($path)-1) != '/') {
            if(!ereg("\.",$path))
                $path .= "/";
        }
        //envoi de la requete HTTP
        fputs($fp,"GET ".$path." HTTP/1.1\r\n");
        fputs($fp,"Host: ".$host."\r\n");
        fputs($fp,"Connection: close\r\n\r\n");
        //on lit le fichier
        $line = fread($fp,255);
        $en_tete = $line;
        //on lit tant qu'on n'est pas la fin du fichier ou
        // qu'on trouve le debut du code html...
        while (!feof($fp) && !ereg("<",$line) ) {
            $en_tete .= $line;
            $line = fread($fp,255);
        }
        fclose($fp);
        //on switch sur le code HTTP renvoye
        $no_code = substr($en_tete,9,3);
       
        switch ($no_code){
            // 2** la page a été trouvée
            case 200 :    $message = "OK";
                        $color = "#33cc00";
                        $connect = 2;
                        break;
            case 204 :    $message = "Cette page ne contient rien! (204)";
                        $color = "#ff9966";
                        break;
            case 206 :    $message = "Contenu partiel de la page! (206)";
                        $color = "#ff9966";
                        break;
            // 3** il y a une redirection
            case 301 : $message = "La page a été déplacéé définitivement!(301)";
                        $message .= seek_redirect_location($en_tete);
                        $color = "#ff9966";
                        $connect = 1;
                        break;
            case 302 :  $message = "La page a été déplacéé momentanément!(302)";
                        $message .= seek_redirect_location($en_tete);
                        $color = "#ff9966";
                        $connect = 1;
                        break;
            // 4** erreur du coté du client
            case 400 :    $message = "Erreur dans la requête HTTP! (400)";
                        $color = "#ff0000";
                        break;
            case 401 :    $message = "Authentification requise! (401)";
                        $color = "#ff0000";
                        break;
            case 402 :    $message = "L'accès à la page est payant! (402)";
                        $color = "#ff0000";
                        break;
            case 403 :    $message = "Accès à la page refusé! (403)";
                        $color = "#ff0000";
                        
                        break;
            case 404 :    $message = "Page inexistante! (404)";
            			
                        $color = "#ff0000";
                        break;
            // 5** erreur du coté du serveur
            case 500 :    $message = "Erreur interne au serveur! (500)";
                        $color = "#ff0000";
                        $connect = 1;
                        break;
            case 502 :  $message = "Erreur cause passerelle du serveur! (502)";
                        $color = "#ff0000";
                        break;
            // cas restant
            default : $message = "Erreur non traitée -> numéro est : $no_code!";
                        $color = "#000000";
                        break;
        }
    }else{
	 	$message = "Impossible de se connecter sur le site. Vérifiez l'adresse.";
       // $message = "Impossible de se connecter par socket";
        $color = "#ff0000";
    }
  //creation du tableau avec les valeurs a rendre
 
  $data_return["statut"] 	= $connect; //la page est OK ou KO (200 => OK sinon KO)
  $data_return["code"] 		= $no_code; //code HTTP renvoye
  $data_return["message"] 	= $message;  
  return $data_return;
}

}
            ?>
