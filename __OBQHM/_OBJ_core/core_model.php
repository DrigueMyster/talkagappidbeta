<?php
class core_model {
    
             public  function model_dataSYSnotif($dataModel) {
                      if( array_key_exists('t_link2', $dataModel)  == false or isset($dataModel['t_link2'])  == false){ $dataModel['t_link2'] = 0; }
                       if( array_key_exists('ID_sys_notify', $dataModel)  == false or isset($dataModel['ID_sys_notify'])  == false){ $dataModel['ID_sys_notify'] = 0; }
                       if( array_key_exists('t_notif', $dataModel)  == false or isset($dataModel['t_notif'])  == false){ $dataModel['t_notif'] = ''; }
                       if( array_key_exists('Recipient_ID_sys_users', $dataModel)  == false or isset($dataModel['Recipient_ID_sys_users'])  == false){ $dataModel['Recipient_ID_sys_users'] = 0; }
                       if( array_key_exists('Recipient_ID_ste', $dataModel)  == false or isset($dataModel['Recipient_ID_ste'])  == false){ $dataModel['Recipient_ID_ste'] = 0; }
                       if( array_key_exists('Recipient_email', $dataModel)  == false or isset($dataModel['Recipient_email'])  == false){ $dataModel['Recipient_email'] = ''; }
                       if( array_key_exists('Sender_ID_sys_users', $dataModel)  == false or isset($dataModel['Sender_ID_sys_users'])  == false){ $dataModel['Sender_ID_sys_users'] = 0; }
                       if( array_key_exists('Sender_ID_ste', $dataModel)  == false or isset($dataModel['Sender_ID_ste'])  == false){ $dataModel['Sender_ID_ste'] = 0; }
                       if( array_key_exists('Sender_email', $dataModel)  == false or isset($dataModel['Sender_email'])  == false){ $dataModel['Sender_email'] = ''; }
                       if( array_key_exists('t_link', $dataModel)  == false or isset($dataModel['t_link'])  == false){ $dataModel['t_link'] = ''; }
                       if( array_key_exists('state', $dataModel)  == false or isset($dataModel['state'])  == false){ $dataModel['state'] = 0; }
                       if( array_key_exists('object', $dataModel)  == false or isset($dataModel['object'])  == false){ $dataModel['object'] = ''; }
                    //   if( array_key_exists('date_send', $dataModel)  == false or isset($dataModel['date_send'])  == false){ $dataModel['date_send'] = ''; }
                       if( array_key_exists('time_send', $dataModel)  == false or isset($dataModel['time_send'])  == false){ $dataModel['time_send'] = ''; }
                    //   if( array_key_exists('date_read', $dataModel)  == false or isset($dataModel['date_read'])  == false){ $dataModel['date_read'] = ''; }
                       if( array_key_exists('time_read', $dataModel)  == false or isset($dataModel['time_read'])  == false){ $dataModel['time_read'] = ''; }                      
                       if( array_key_exists('cc_email', $dataModel)  == false or isset($dataModel['cc_email'])  == false){ $dataModel['cc_email'] = 0; }  
                       if( array_key_exists('view', $dataModel)  == false or isset($dataModel['view'])  == false){ $dataModel['view'] = 0; }  
                       
                       return $dataModel;
             }
    
         public  function model_dataGEOstreet($dataModel) {
                    
                       if( array_key_exists('ID_geostreet', $dataModel)  == false or isset( $dataModel['ID_geostreet'])  == false){ $dataModel['ID_geostreet'] = 0; }
                       if( array_key_exists('ID_geocity', $dataModel)  == false or isset( $dataModel['ID_geocity'])  == false){ $dataModel['ID_geocity'] = 0; }
                       if( array_key_exists('ID_geodistrict', $dataModel)  == false or isset( $dataModel['ID_geodistrict'])  == false){ $dataModel['ID_geodistrict'] = 0; }
                       if( array_key_exists('num', $dataModel)  == false or isset( $dataModel['num'])  == false){ $dataModel['num'] = ''; }
                       if( array_key_exists('street', $dataModel)  == false or isset( $dataModel['street'])  == false){ $dataModel['street'] = ''; }
                       if( array_key_exists('gps_lat_street', $dataModel)  == false or isset( $dataModel['gps_lat_street'])  == false){ $dataModel['gps_lat_street'] = ''; }
                       if( array_key_exists('gps_lon_street', $dataModel)  == false or isset( $dataModel['gps_lon_street'])  == false){ $dataModel['gps_lon_street'] = ''; }
                       if( array_key_exists('zoommap', $dataModel)  == false or isset( $dataModel['zoommap'])  == false){ $dataModel['zoommap'] = 19; }
                       
                       return $dataModel;
      }
    
      public  function model_dataSYSqhmnetworkVIEW($dataModel) {
                    
                       if( array_key_exists('gps_lat_street', $dataModel)  == false or isset( $dataModel['gps_lat_street'])  == false){ $dataModel['gps_lat_street'] = 0; }
                       if( array_key_exists('ID_geoloc', $dataModel)  == false or isset( $dataModel['ID_geoloc'])  == false){ $dataModel['ID_geoloc'] = 0; }
                       if( array_key_exists('type', $dataModel)  == false or isset( $dataModel['type'])  == false){ $dataModel['type'] = ''; }
                       if( array_key_exists('ID_activite', $dataModel)  == false or isset( $dataModel['ID_activite'])  == false){ $dataModel['ID_activite'] = ''; }
                       if( array_key_exists('galaxy', $dataModel)  == false or isset( $dataModel['galaxy'])  == false){ $dataModel['galaxy'] = ''; }
                       if( array_key_exists('Country', $dataModel)  == false or isset( $dataModel['Country'])  == false){ $dataModel['Country'] = ''; }
                       if( array_key_exists('departement', $dataModel)  == false or isset( $dataModel['departement'])  == false){ $dataModel['departement'] = 0; }
                       if( array_key_exists('zipcode', $dataModel)  == false or isset( $dataModel['zipcode'])  == false){ $dataModel['zipcode'] = ''; }
                       if( array_key_exists('district', $dataModel)  == false or isset( $dataModel['district'])  == false){ $dataModel['district'] =0; }
                       if( array_key_exists('street', $dataModel)  == false or isset( $dataModel['street'])  == false){ $dataModel['street'] = 0; }
                       if( array_key_exists('residence', $dataModel)  == false or isset( $dataModel['residence'])  == false){ $dataModel['residence'] = 0; }
                       return $dataModel;
      }
    
       public  function model_dataSYSqhmnetwork($dataModel) {
                    
                        if( array_key_exists('type', $dataModel)  == false or isset( $dataModel['type'])  == false){ $dataModel['type'] = ''; }
                        if( array_key_exists('zone', $dataModel)  == false or isset( $dataModel['zone'])  == false){ $dataModel['zone'] = ''; }
                      
                        if( array_key_exists('statut', $dataModel)  == false or isset( $dataModel['statut'])  == false){ $dataModel['statut'] = ''; }
                        if( array_key_exists('mystatut', $dataModel)  == false or isset( $dataModel['mystatut'])  == false){ $dataModel['mystatut'] = ''; }
                        if( array_key_exists('category', $dataModel)  == false or isset( $dataModel['category'])  == false){ $dataModel['category'] = ''; }
                        if( array_key_exists('ID_answer', $dataModel)  == false or isset( $dataModel['ID_answer'])  == false){ $dataModel['ID_answer'] = 0; }
                        if( array_key_exists('Recipient_ID_sys_users', $dataModel)  == false or isset( $dataModel['Recipient_ID_sys_users'])  == false){ $dataModel['Recipient_ID_sys_users'] = 0; }
                        if( array_key_exists('Recipient_ID_ste', $dataModel)  == false or isset( $dataModel['Recipient_ID_ste'])  == false){ $dataModel['Recipient_ID_ste'] = 0; }
                        if( array_key_exists('Recipient_email', $dataModel)  == false or isset( $dataModel['Recipient_email'])  == false){ $dataModel['Recipient_email'] = ''; }
                        if( array_key_exists('Sender_ID_sys_users', $dataModel)  == false or isset( $dataModel['Sender_ID_sys_users'])  == false){ $dataModel['Sender_ID_sys_users'] = 0; }
                       if( array_key_exists('Sender_ID_ste', $dataModel)  == false or isset( $dataModel['Sender_ID_ste'])  == false){  $dataModel['Sender_ID_ste'] = 0; }
                        if( array_key_exists('Sender_email', $dataModel)  == false or isset( $dataModel['Sender_email'])  == false){ $dataModel['Sender_email'] = ''; }
                        if( array_key_exists('t_link', $dataModel)  == false or isset( $dataModel['t_link'])  == false){ $dataModel['t_link'] = ''; }
                        if( array_key_exists('t_img', $dataModel)  == false or isset( $dataModel['t_img'])  == false){ $dataModel['t_img'] = ''; }
                        if( array_key_exists('t_movies', $dataModel)  == false or isset( $dataModel['t_movies'])  == false){ $dataModel['t_movies'] = ''; }
                   
                        if( array_key_exists('t_sound', $dataModel)  == false or isset( $dataModel['t_sound'])  == false){ $dataModel['t_sound'] = ''; }
                        if( array_key_exists('t_link_ext', $dataModel)  == false or isset( $dataModel['t_link_ext'])  == false){ $dataModel['t_link_ext'] = ''; }
                        if( array_key_exists('content', $dataModel)  == false or isset( $dataModel['content'])  == false){ $dataModel['content'] = ''; }
                        if( array_key_exists('title', $dataModel)  == false or isset( $dataModel['title'])  == false){ $dataModel['title'] = ''; }
                        if( array_key_exists('content_html', $dataModel)  == false or isset( $dataModel['content_html'])  == false){ $dataModel['content_html'] = ''; }
                        if( array_key_exists('time_event', $dataModel)  == false or isset( $dataModel['time_event'])  == false){ $dataModel['time_event'] = ''; }
                        if( array_key_exists('url_qhm', $dataModel)  == false or isset( $dataModel['url_qhm'])  == false){ $dataModel['url_qhm'] = ''; }
                        if( array_key_exists('ID_sys_users', $dataModel)  == false or isset( $dataModel['ID_sys_users'])  == false){ $dataModel['ID_sys_users'] = ''; }
                        if( array_key_exists('ID_sys_qhmnetwork', $dataModel)  == false or isset( $dataModel['ID_sys_qhmnetwork'])  == false){ $dataModel['ID_sys_qhmnetwork'] = 0; }
                     
                        if( array_key_exists('ID_ste', $dataModel)  == false or isset( $dataModel['ID_ste'])  == false){ $dataModel['ID_ste'] = ''; }
                        if( array_key_exists('users_nom', $dataModel)  == false or isset( $dataModel['users_nom'])  == false){ $dataModel['users_nom'] = ''; }
                     if( array_key_exists('users_prenom', $dataModel)  == false or isset( $dataModel['users_prenom'])  == false){ $dataModel['users_prenom'] = ''; }
                        if( array_key_exists('users_gender', $dataModel)  == false or isset( $dataModel['users_gender'])  == false){ $dataModel['users_gender'] = ''; }
                        if( array_key_exists('iso2', $dataModel)  == false or isset( $dataModel['iso2'])  == false){ $dataModel['iso2'] = ''; }
                        if( array_key_exists('ID_geocity', $dataModel)  == false or isset( $dataModel['ID_geocity'])  == false){ $dataModel['ID_geocity'] = ''; }
                        if( array_key_exists('img_user', $dataModel)  == false or isset( $dataModel['img_user'])  == false){ $dataModel['img_user'] = ''; }
                     
                        if( array_key_exists('ID_geodept', $dataModel)  == false or isset( $dataModel['ID_geodept'])  == false){ $dataModel['ID_geodept'] = ''; }
                        if( array_key_exists('departement', $dataModel)  == false or isset( $dataModel['departement'])  == false){ $dataModel['departement'] = ''; }
                        if( array_key_exists('country', $dataModel)  == false or isset( $dataModel['country'])  == false){ $dataModel['country'] = ''; }
                       if( array_key_exists('cityname', $dataModel)  == false or isset( $dataModel['cityname'])  == false){  $dataModel['cityname'] = ''; }
                       if( array_key_exists('ID_city', $dataModel)  == false or isset( $dataModel['ID_city'])  == false){  $dataModel['ID_city'] = ''; }
                       if( array_key_exists('galaxy', $dataModel)  == false or isset( $dataModel['galaxy'])  == false){  $dataModel['galaxy'] = ''; }
                       if( array_key_exists('planet', $dataModel)  == false or isset( $dataModel['planet'])  == false){  $dataModel['planet'] = ''; }
                       if( array_key_exists('zipcode', $dataModel)  == false or isset( $dataModel['zipcode'])  == false){  $dataModel['zipcode'] = ''; }
                       if( array_key_exists('residence', $dataModel)  == false or isset( $dataModel['residence'])  == false){  $dataModel['residence'] = ''; }
                        if( array_key_exists('district', $dataModel)  == false or isset( $dataModel['district'])  == false){ $dataModel['district'] = ''; }
                        if( array_key_exists('type_space', $dataModel)  == false or isset( $dataModel['type_space'])  == false){ $dataModel['type_space'] = ''; }
                        if( array_key_exists('view', $dataModel)  == false or isset( $dataModel['view'])  == false){ $dataModel['view'] = 0; }
                        if( array_key_exists('raison_soc', $dataModel)  == false or isset( $dataModel['raison_soc'])  == false){ $dataModel['raison_soc'] = ''; }
                       if( array_key_exists('ext_ID1', $dataModel)  == false or isset( $dataModel['ext_ID1'])  == false){  $dataModel['ext_ID1'] = ''; }
                       
                         if( array_key_exists('street', $dataModel)  == false or isset( $dataModel['street'])  == false){  $dataModel['street'] = ''; }
                     
                     
                     return $dataModel;
                     
       }               
    public  function model_dataSYSGEOCITY() {

                    $dataModel['ID_geocity'] = 0;

                    $dataModel['iso2'] = '';

                     $dataModel['city'] = '';

                    $dataModel['residents'] = 0;
 
                    $dataModel['gps_lat_city'] = '';

                    $dataModel['gps_lon_city'] = '';

                    $dataModel['zoommap_city'] = 0;

                    $dataModel['zipcode'] = '';

                    $dataModel['departement'] = 0;

                    $dataModel['insee'] = '';
     
                    $dataModel['state'] = 0;
      
            
            return $dataModel;
    }
    
    public  function model_affzone($dataModel) {
            if( array_key_exists('channel', $dataModel)  == false or isset( $dataModel['channel'])  == false){ $dataModel['channel'] = ''; }
              //  $dataModel['channel'] = '';
            if( array_key_exists('info_channel', $dataModel)  == false or isset( $dataModel['info_channel'])  == false){ $dataModel['info_channel'] = ''; }
             //   $dataModel['info_channel'] = '';
           if( array_key_exists('state', $dataModel)  == false or isset( $dataModel['state'])  == false){ $dataModel['state'] = ''; } 
             //   $dataModel['state'] = '';
            if( array_key_exists('gps_lat', $dataModel)  == false or isset( $dataModel['gps_lat'])  == false){ $dataModel['gps_lat'] = ''; }
              //  $dataModel['gps_lat'] = '';
            if( array_key_exists('gps_lon', $dataModel)  == false or isset( $dataModel['gps_lon'])  == false){ $dataModel['gps_lon'] = ''; }
             //   $dataModel['gps_lon'] = '';
            if( array_key_exists('zo', $dataModel)  == false or isset( $dataModel['zo'])  == false){ $dataModel['zo'] = ''; }
            //    $dataModel['zo'] = '';
           if( array_key_exists('departement', $dataModel)  == false or isset( $dataModel['departement'])  == false){ $dataModel['departement'] = ''; }
               // $dataModel['departement'] = '';
            if( array_key_exists('ID_geocity', $dataModel)  == false or isset( $dataModel['ID_geocity'])  == false){ $dataModel['ID_geocity'] = ''; }
           //     $dataModel['ID_geocity'] = '';
            if( array_key_exists('cityname', $dataModel)  == false or isset( $dataModel['cityname'])  == false){ $dataModel['cityname'] = ''; }
             //   $dataModel['cityname'] = '';
           if( array_key_exists('zone_city', $dataModel)  == false or isset( $dataModel['zone_city'])  == false){ $dataModel['zone_city'] = ''; }
              //  $dataModel['zone_city'] = '';
          if( array_key_exists('iso2', $dataModel)  == false or isset( $dataModel['iso2'])  == false){ $dataModel['iso2'] = ''; }
                //$dataModel['iso2'] = '';
           if( array_key_exists('adr', $dataModel)  == false or isset( $dataModel['adr'])  == false){ $dataModel['adr'] = ''; }
          
            //    $dataModel['adr'] = '';
                 return $dataModel;
          
      } 
    
   public  function model_VAR() {
       if(empty($_GET['t_notif'])){ $_GET['t_notif'] = ''; }
         if(empty($_GET['confirminvit'])){ $_GET['confirminvit'] = ''; }
        if(empty($_GET['private'])){ $_GET['private'] = ''; }
       if(empty($_GET['WordMedium'])){ $_GET['WordMedium'] = ''; }
         if(empty($_GET['medium'])){ $_GET['medium'] = ''; }
        if(empty($_GET['PerfFlx'])){ $_GET['PerfFlx'] = ''; }
        if(empty($_GET['precpte'])){ $_GET['precpte'] = ''; }
         if(empty($_GET['ncnx'])){ $_GET['ncnx'] = ''; }
        if(empty($_GET['usepage'])){ $_GET['usepage'] = ''; }
           if(empty($_GET['type_flx'])){ $_GET['type_flx'] = ''; }
           if(empty($_GET['sys'])){ $_GET['sys'] = ''; }
           if(empty($_GET['idzn'])){ $_GET['idzn'] = ''; }
           if(empty($_GET['type_flx'])){ $_GET['type_flx'] = ''; }
         if(empty($_GET['zone'])){ $_GET['zone'] = ''; }
          if(empty($_GET['statut'])){ $_GET['statut'] = ''; }             
          if(empty($_GET['lg'])){ $_GET['lg'] = ''; }     
          if(empty($_GET['sch'])){ $_GET['sch'] = ''; }  
          if(empty($_GET['stepID_sys_qhmnetwork'])){ $_GET['stepID_sys_qhmnetwork'] = ''; }  
          if(empty($_GET['endID_sys_qhmnetwork'])){ $_GET['endID_sys_qhmnetwork'] = ''; }  
          if(empty($_GET['ID_ste'])){ $_GET['ID_ste'] = 0; }  
          if(empty($_GET['ID_sys_user'])){ $_GET['ID_sys_user'] = 0; }  
          if(empty($_GET['sp'])){ $_GET['sp'] = ''; }  
          if(empty($_GET['city'])){ $_GET['city'] = ''; }  
          if(empty($_GET['Country'])){ $_GET['Country'] = ''; }  
          if(empty($_GET['users_prenom'])){ $_GET['users_prenom'] = ''; }  
          if(empty($_GET['visit'])){ $_GET['visit'] = ''; }  
          if(empty($_GET['nt'])){ $_GET['nt'] = ''; }  
           if(empty($_GET['SH'])){ $_GET['SH'] = ''; }  
           if(empty($_GET['prox'])){ $_GET['prox'] = ''; }  
           if(empty($_GET['modif_data'])){ $_GET['modif_data'] = ''; }  
          if(empty($_GET['HeadFlxPublish'])){ $_GET['HeadFlxPublish'] = ''; }  
          if(empty($_GET['ID_putaon'])){ $_GET['ID_putaon'] = ''; }  
          if(empty($_GET['hiddenform'])){ $_GET['hiddenform'] = ''; }  
          if(empty($_GET['answer'])){ $_GET['answer'] = ''; }  
          if(empty($_GET['read_nw'])){ $_GET['read_nw'] = ''; }  
          if(empty($_GET['mois'])){ $_GET['mois'] = ''; }  
          if(empty($_GET['annee'])){ $_GET['annee'] = ''; }  
          if(empty($_GET['date_id'])){ $_GET['date_id'] = ''; }  
          if(empty($_GET['title'])){ $_GET['title'] = ''; } 
          if(empty($_GET['creat_market'])){ $_GET['creat_market'] = ''; }  
          if(empty($_GET['view'])){ $_GET['view'] = ''; }  
          if(empty($_GET['flxadd'])){ $_GET['flxadd'] = ''; }  
          if(empty($_GET['SubrQ'])){ $_GET['SubrQ'] = ''; }  
          if(empty($_GET['type'])){ $_GET['type'] = ''; } 
          if(empty($_GET['ID_sys_qhmnetwork'])){ $_GET['ID_sys_qhmnetwork'] = ''; }  
          if(empty($_GET['winpost'])){ $_GET['winpost'] = ''; }  
          if(empty($_GET['nt'])){ $_GET['nt'] = ''; }  
          if(empty($_GET['STATE_process'])){ $_GET['STATE_process'] = ''; }  
           if(empty($_GET['aff_end_event'])){ $_GET['aff_end_event'] = ''; }  
           if(empty($_GET['flxrespanswer'])){ $_GET['flxrespanswer'] = ''; }  
           if(empty($_GET['nextmess'])){ $_GET['nextmess'] = ''; }  
           if(empty($_GET['hidden'])){ $_GET['hidden'] = ''; }  
          
   }
        
   public  function model_dataSTE($dataModel) {
  
      if( array_key_exists('url_qhapps', $dataModel)  == false or isset( $dataModel['url_qhapps'])  == false){
         $dataModel['url_qhapps'] = '';
      }
       if( array_key_exists('url_qhm', $dataModel)  == false or isset( $dataModel['url_qhm'])  == false){
         $dataModel['url_qhm'] = '';
      }
      if( array_key_exists('ref_cli', $dataModel)  == false or isset( $dataModel['ref_cli'])  == false){
         $dataModel['ref_cli'] = '';
      }
       if( array_key_exists('type', $dataModel)  == false or isset( $dataModel['type'])  == false){
         $dataModel['type'] = '';
      }
      if( array_key_exists('raison_soc', $dataModel)  == false or isset( $dataModel['raison_soc'])  == false){
         $dataModel['raison_soc'] = '';
      }     
      if( array_key_exists('tags', $dataModel)  == false or isset( $dataModel['tags'])  == false){
         $dataModel['tags'] = '';
      } 
       if( array_key_exists('img_ste', $dataModel)  == false or isset( $dataModel['img_ste'])  == false){
         $dataModel['img_ste'] = '';
      }   
      if( array_key_exists('img_head', $dataModel)  == false or isset( $dataModel['img_head'])  == false){
         $dataModel['img_head'] = '';
      }      
       if( array_key_exists('ID_juridique', $dataModel)  == false or isset( $dataModel['ID_juridique'])  == false){
         $dataModel['ID_juridique'] = '';
      }  
      if( array_key_exists('ID_activite', $dataModel)  == false or isset( $dataModel['ID_activite'])  == false){
         $dataModel['ID_activite'] = '';
      }  
      if( array_key_exists('STATE_ste', $dataModel)  == false or isset( $dataModel['STATE_ste'])  == false or empty( $dataModel['STATE_ste'])){
         $dataModel['STATE_ste'] = 0;
      }  
      if( array_key_exists('site_ste', $dataModel)  == false or isset( $dataModel['site_ste'])  == false){
         $dataModel['site_ste'] = '';
      }  
      if( array_key_exists('email_ste', $dataModel)  == false or isset( $dataModel['email_ste'])  == false){
         $dataModel['email_ste'] = '';
      }  
      if( array_key_exists('category', $dataModel)  == false or isset( $dataModel['category'])  == false){
         $dataModel['category'] = '';
      }  
      if( array_key_exists('Country', $dataModel)  == false or isset( $dataModel['Country'])  == false){
         $dataModel['Country'] = '';
      }  

      if( array_key_exists('galaxy', $dataModel)  == false or isset( $dataModel['galaxy'])  == false){
         $dataModel['galaxy'] = 'SOLSY';
      }  
       if( array_key_exists('planet', $dataModel)  == false or isset( $dataModel['planet'])  == false){
         $dataModel['planet'] = 'EARTH';
      }
       if( array_key_exists('departement', $dataModel)  == false or isset( $dataModel['departement'])  == false or empty($dataModel['departement'])){
         $dataModel['departement'] = 0;
      }
       if( array_key_exists('zipcode', $dataModel)  == false or isset( $dataModel['zipcode'])  == false){
         $dataModel['zipcode'] = '';
      }
            if( array_key_exists('city', $dataModel)  == false or isset( $dataModel['city'])  == false or empty( $dataModel['city'])){
         $dataModel['city'] = 0;
      }     
      if( array_key_exists('district', $dataModel)  == false or isset( $dataModel['district'])  == false or empty( $dataModel['district'])){
         $dataModel['district'] = 0;
      }
            if( array_key_exists('street', $dataModel)  == false or isset( $dataModel['street'])  == false or empty( $dataModel['street'])){
         $dataModel['street'] = 0;
      }
       if( array_key_exists('residence', $dataModel)  == false or isset( $dataModel['residence'])  == false or empty( $dataModel['residence'])){
         $dataModel['residence'] = 0;
      }
      if( array_key_exists('devise', $dataModel)  == false or isset( $dataModel['devise'])  == false){
         $dataModel['devise'] = '';
      }
      if( array_key_exists('desc_nw', $dataModel)  == false or isset( $dataModel['desc_nw'])  == false){
         $dataModel['desc_nw'] = '';
      }
      if( array_key_exists('time_creation', $dataModel)  == false or isset( $dataModel['time_creation'])  == false){
         $dataModel['time_creation'] = '';
      }
      
      
      
      $data['url_qhapps'] = $dataModel['url_qhapps'];
      $data['url_qhm'] = $dataModel['url_qhm'];
      $data['ref_cli'] = $dataModel['ref_cli'];
      $data['type'] = $dataModel['type'];
      $data['raison_soc'] = $dataModel['raison_soc'];
      $data['tags'] = $dataModel['tags'];
      $data['img_ste'] = $dataModel['img_ste'];
      $data['img_head'] = $dataModel['img_head'];
      $data['ID_juridique'] = $dataModel['ID_juridique'];
      $data['ID_activite'] = $dataModel['ID_activite'];
      $data['STATE_ste'] = $dataModel['STATE_ste'];
      $data['site_ste'] = $dataModel['site_ste'];
      $data['email_ste'] = $dataModel['email_ste'];
      $data['category'] = $dataModel['category'];
      $data['Country'] = $dataModel['Country'];
      $data['galaxy'] = $dataModel['galaxy'];
      $data['planet'] = $dataModel['planet'];
      $data['departement'] = $dataModel['departement'];
      $data['zipcode'] = $dataModel['zipcode'];
      $data['city'] = $dataModel['city'];
      $data['district'] = $dataModel['district'];
      $data['street'] = $dataModel['street'];
      $data['residence'] = $dataModel['residence'];
      $data['devise'] = $dataModel['devise'];
      $data['district'] = $dataModel['district'];
      $data['desc_nw'] = $dataModel['desc_nw'];
      $data['time_creation'] = $dataModel['time_creation'];

     
       return $data;
   }
   
   
   public  function model_dataSYSUSER($dataModel) {
  
      if( array_key_exists('date_creation', $dataModel)  == false or isset( $dataModel['date_creation'])  == false){
         $dataModel['date_creation'] = '';
      }
      $data['date_creation'] = $dataModel['date_creation'];
      
     if( array_key_exists('time_creation', $dataModel)  == false or isset( $dataModel['time_creation'])  == false){
         $dataModel['time_creation'] = '';
      }
      $data['time_creation'] = $dataModel['time_creation'];
  
      if( array_key_exists('url_qhm', $dataModel)  == false or isset( $dataModel['url_qhm'])  == false){
         $dataModel['url_qhm'] = '';
      }
      $data['url_qhm'] = $dataModel['url_qhm'];
      
      if( array_key_exists('STATE_user', $dataModel)  == false or isset( $dataModel['STATE_user'])  == false or empty( $dataModel['STATE_user'])){
         $dataModel['STATE_user'] = 0;
      }
      $data['STATE_user'] = $dataModel['STATE_user'];
      
      if( array_key_exists('img_user', $dataModel)  == false or isset( $dataModel['img_user'])  == false){
         $dataModel['img_user'] = '';
      }
      $data['img_user'] = $dataModel['img_user'];
    
       if( array_key_exists('users_gender', $dataModel)  == false or isset( $dataModel['users_gender'])  == false){
         $dataModel['users_gender'] = '';
      }
      $data['users_gender'] = $dataModel['users_gender'];
      
       if( array_key_exists('users_nom', $dataModel)  == false or isset( $dataModel['users_nom'])  == false){
         $dataModel['users_nom'] = '';
      }
      $data['users_nom'] = $dataModel['users_nom'];
      
      if( array_key_exists('users_prenom', $dataModel)  == false or isset( $dataModel['users_prenom'])  == false){
         $dataModel['users_prenom'] = '';
      }
      $data['users_prenom'] = $dataModel['users_prenom'];
      
      if( array_key_exists('login_user', $dataModel)  == false or isset( $dataModel['login_user'])  == false){
         $dataModel['login_user'] = '';
      }
      $data['login_user'] = $dataModel['login_user'];
      
      if( array_key_exists('passw_user', $dataModel)  == false or isset( $dataModel['passw_user'])  == false){
         $dataModel['passw_user'] = '';
      }
      $data['passw_user'] = $dataModel['passw_user'];
      
       if( array_key_exists('ID_ste', $dataModel)  == false or isset( $dataModel['ID_ste'])  == false  or empty( $dataModel['ID_ste'])){
         $dataModel['ID_ste'] = 0;
      }
      $data['ID_ste'] = $dataModel['ID_ste'];
      
       if( array_key_exists('users_email', $dataModel)  == false or isset( $dataModel['users_email'])  == false){
         $dataModel['users_email'] = '';
      }
      $data['users_email'] = $dataModel['users_email'];
      
      if( array_key_exists('ext_ID1', $dataModel)  == false or isset( $dataModel['ext_ID1'])  == false or empty( $dataModel['ext_ID1']) ){
         $dataModel['ext_ID1'] = 0;
      }
      $data['ext_ID1'] = $dataModel['ext_ID1'];

       if( array_key_exists('autocnt', $dataModel)  == false or isset( $dataModel['autocnt'])  == false or empty( $dataModel['autocnt'])){
         $dataModel['autocnt'] = 0;
      }
      $data['autocnt'] = $dataModel['autocnt'];
      
      if( array_key_exists('lg', $dataModel)  == false or isset( $dataModel['lg'])  == false){
         $dataModel['lg'] = '';
      }
      $data['lg'] = $dataModel['lg'];
            
      if( array_key_exists('mystatut', $dataModel)  == false or isset( $dataModel['mystatut'])  == false){
         $dataModel['mystatut'] = '';
      }
      $data['mystatut'] = $dataModel['mystatut'];
             
  

      return $data;
   }
   
   public  function model_dataBASE($dataModel) {
  
      if( array_key_exists('ID_ste', $dataModel)  == false or isset( $dataModel['ID_ste'])  == false){
         $dataModel['ID_ste'] = '';
      }
      $data['ID_ste'] = $dataModel['ID_ste'];
      
       if( array_key_exists('view_global', $dataModel)  == false or isset( $dataModel['view_global'])  == false or empty( $dataModel['view_global'])){
         $dataModel['view_global'] = 0;
      }
      $data['view_global'] = $dataModel['view_global'];
      
      if( array_key_exists('state', $dataModel)  == false or isset( $dataModel['state'])  == false or empty( $dataModel['state'])){
         $dataModel['state'] = 0;
      }
      $data['state'] = $dataModel['state'];
      
      if( array_key_exists('label', $dataModel)  == false or isset( $dataModel['label'])  == false){
         $dataModel['label'] = '';
      }
      $data['label'] = $dataModel['label'];
      
      if( array_key_exists('val', $dataModel)  == false or isset( $dataModel['val'])  == false){
         $dataModel['val'] = '';
      }
      $data['val'] = $dataModel['val'];
      
       if( array_key_exists('type_data', $dataModel)  == false or isset( $dataModel['type_data'])  == false){
         $dataModel['type_data'] = '';
      }
      $data['type_data'] = $dataModel['type_data'];
      
       if( array_key_exists('src', $dataModel)  == false or isset( $dataModel['src'])  == false){
         $dataModel['src'] = '';
      }
      $data['src'] = $dataModel['src'];
      

      
      
       return $data;
   }
   
}


