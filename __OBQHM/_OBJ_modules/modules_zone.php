<?php

class modules_zone extends core_Query {
        
                    public  function model_idzn($idzn,$zone) {	
                            $inst_zone = NEW modules_zone();
                            if($idzn == 'zipcode_city'){                                
                                      $g = $inst_zone->aff_zipcode_city($zone);
                            }elseif($idzn == 'district'){       
                                      $g = $inst_zone->aff_district($zone);
                              }elseif($idzn == 'desktop.home_startqhm_move_district'){       
                                     $g = $inst_zone->aff_zipcode_city($zone);   
                            }elseif($idzn == 'continental'){       
                                      $g = $inst_zone->aff_continental($zone);    
                            }elseif($idzn == 'country'){       
                                      $g = $inst_zone->aff_country($zone);      
                             }elseif($idzn == 'planet'){       
                                      $g = $inst_zone->aff_planet();      
                            }elseif($idzn == 'departement'){       
                                      $g = $inst_zone->aff_departement($zone);         
                            }elseif($idzn == 'locate'){       
                                
                                      $g = $inst_zone->aff_locate($zone);         
                            }
                            
                            if(empty($g['channel'])){
                                       return false;
                            }else{
                                       return $g;
                            }
                           
                    }
                    
                    	public  function aff_continental($zone) {
                                            $aff_zone['channel'] ='';
                                           $aff_zone['info_channel'] = '';
                                           $aff_zone['state'] ='';
                                           $aff_zone['state'] ='';
		$aff_zone['gps_lat'] ='';
		$aff_zone['gps_lon'] = '';
		$aff_zone['zo'] ='';	
                                           $aff_zone['departement'] ='';         
		$aff_zone['adr'] =  '';
                                           $aff_zone['channel'] = '';
                                           $aff_zone['iso2']=     '';
                                           $aff_zone['cityname']  =  '';
                                           $aff_zone['ID_geocity']='';
                                           $aff_zone['zone_city']  = '';
                                      
                                  //      $inst_sys = new SYS();
                                        $sysidzn = explode('_',$zone);
                                       //  $sys_geoopt2 = $inst_sys->READ_ID_GEO($sysidzn[2]) ;
                                        
                                        $aff_zone['gps_lat'] = '';
                                        $aff_zone['gps_lon'] = '';
                                        $aff_zone['iso2']= '';
                                        $aff_zone['channel'] = $sysidzn[2];
                                        $aff_zone['zo'] = 5;
                                   
                                        $aff_zone['state'] =true;       
                                        return  $aff_zone;                                     
	}
        

	public  function aff_country($zone) {
                                            $aff_zone['channel'] ='';
                                           $aff_zone['info_channel'] = '';
                                           $aff_zone['state'] ='';
                                           $aff_zone['state'] ='';
		$aff_zone['gps_lat'] ='';
		$aff_zone['gps_lon'] = '';
		$aff_zone['zo'] ='';	
                                           $aff_zone['departement'] ='';         
		$aff_zone['adr'] =  '';
                                           $aff_zone['channel'] = '';
                                           $aff_zone['iso2']=     '';
                                           $aff_zone['cityname']  =  '';
                                           $aff_zone['ID_geocity']='';
                                           $aff_zone['zone_city']  = '';
                                      
                                        $inst_sys = new SYS();
                                        $sysidzn = explode('_',$zone);
                                         $sys_geoopt2 = $inst_sys->READ_ID_GEO($sysidzn[2]) ;
                                        $aff_zone['gps_lat'] = $sys_geoopt2['gps_lat_geo'];
                                        $aff_zone['gps_lon'] = $sys_geoopt2['gps_lon_geo'];
                                        $aff_zone['iso2']=       $sys_geoopt2['iso2'];
                                        $aff_zone['channel'] = $sys_geoopt2['country'];
                                        $aff_zone['zo'] = 5;
                                   
                                        $aff_zone['state'] =true;       
                                        return  $aff_zone;                                     
	}
        
        	public  function aff_planet() {
                                          $aff_zone['info_channel'] = '';
                                           $aff_zone['state'] ='';
                                           $aff_zone['state'] ='';
		$aff_zone['gps_lat'] ='';
		$aff_zone['gps_lon'] = '';
		$aff_zone['zo'] ='';	
                                           $aff_zone['departement'] ='';         
		$aff_zone['adr'] =  '';
                                           $aff_zone['channel'] = '';
                                           $aff_zone['iso2']=     '';
                                           $aff_zone['cityname']  =  '';
                                           $aff_zone['ID_geocity']='';
                                           $aff_zone['zone_city']  = '';
                                           
                                     $aff_zone['channel'] = GLOBALMAP;
                                     $aff_zone['state'] =true;                                    
                                     return  $aff_zone;                                     
	}
	
                public  function aff_zipcode_city($zone) {	
                                       $aff_zone['info_channel'] = '';
                                           $aff_zone['state'] ='';
                                           $aff_zone['state'] ='';
		$aff_zone['gps_lat'] ='';
		$aff_zone['gps_lon'] = '';
		$aff_zone['zo'] ='';	
                                           $aff_zone['departement'] ='';         
		$aff_zone['adr'] =  '';
                                           $aff_zone['channel'] = '';
                                           $aff_zone['iso2']=     '';
                                           $aff_zone['cityname']  =  '';
                                           $aff_zone['ID_geocity']='';
                                           $aff_zone['zone_city']  = '';
                     
                                             $inst_sys = new SYS();
                                    $sysidzn = explode('_',$zone);
                                    $sys_cityopt2 = $inst_sys->READ_ID_GEOCITY($sysidzn[4]);
                                 
                                           
                                   $sys_departement = $inst_sys->READ_ID_GEODEPT($sys_cityopt2['departement']);
                                    $aff_zone['zo'] = $sys_cityopt2['zoommap_city'];
                                    $aff_zone['adr'] =  str_replace(' ','_',$sys_cityopt2['city'] ).'_'.str_replace(' ','_',$sys_departement['departement']).'_'.str_replace(' ','_',$sys_cityopt2['iso2']) ;
                                    $aff_zone['gps_lat'] = $sys_cityopt2['gps_lat_city'];
                                    $aff_zone['gps_lon'] = $sys_cityopt2['gps_lon_city'];
                                     $aff_zone['channel'] = ucfirst(strtolower($sys_cityopt2['city'])) ;
                                     $aff_zone['departement'] = $sys_departement['ID_geodept'];
                                     $aff_zone['ID_geocity']=$sys_cityopt2['ID_geocity'];
                                      $aff_zone['iso2']=       $sys_cityopt2['iso2'];
                                    
                                   //   $aff_zone['info_channel'] = 'Hello! Bienvenue sur le canal  <span style="font-weight:bold;">'.$sys_cityopt2['city'].'</span>';
                                      $aff_zone['state'] =true;
                                      
                                    return  $aff_zone;
                }
                
                public function aff_departement($zone){           
                                       $aff_zone['info_channel'] = '';
                                           $aff_zone['state'] ='';
                                           $aff_zone['state'] ='';
		$aff_zone['gps_lat'] ='';
		$aff_zone['gps_lon'] = '';
		$aff_zone['zo'] ='';	
                                           $aff_zone['departement'] ='';         
		$aff_zone['adr'] =  '';
                                           $aff_zone['channel'] = '';
                                           $aff_zone['iso2']=     '';
                                           $aff_zone['cityname']  =  '';
                                           $aff_zone['ID_geocity']='';
                                           $aff_zone['zone_city']  = '';
                      $core_Model = new core_model();
                                           $aff_zone = $core_Model->model_affzone($aff_zone);
                                           
                               $inst_sys = new SYS();
                                $sysidzn = explode('_',$zone);
                                $sys_cityopt2 = $inst_sys->READ_ID_GEOCITY($_GET['prox']);
                                $sys_departement = $inst_sys->READ_ID_GEODEPT($sysidzn[3]);
                                $aff_zone['zo'] = $sys_departement['zoommap_departement'];
                                $aff_zone['adr'] =  str_replace(' ','_',$sys_departement['departement']).'_'.str_replace(' ','_',$sys_cityopt2['iso2']) ;
		//$aff_zone['adr'] = $sys_departement['departement'].', '. $sys_departement['iso2'] ;
                                $aff_zone['gps_lat'] = $sys_departement['gps_lat_departement'];
                                $aff_zone['gps_lon'] = $sys_departement['gps_lon_departement'];
                                $aff_zone['departement'] = $sys_departement['ID_geodept'];
                                $aff_zone['ID_geocity'] = $_GET['prox'];
                                $aff_zone['channel'] = 'Proximité';
                                $aff_zone['cityname']  =  ucfirst(strtolower($sys_cityopt2['city']));
                                   //  $aff_zone['info_channel'] = 'Hello! Bienvenue sur le canal   <span style="font-weight:bold;">'.$aff_zone['channel'].'</span>';
                                $aff_zone['iso2']=     $sys_departement['iso2'];
                                $aff_zone['zone_city']  =  $sys_cityopt2['ID_geocity'];
                                $aff_zone['state'] =true;
                                return  $aff_zone;
                }
                
                public function  aff_district($zone){
                                           $aff_zone['info_channel'] = '';
                                           $aff_zone['state'] ='';
                                           $aff_zone['state'] ='';
		$aff_zone['gps_lat'] ='';
		$aff_zone['gps_lon'] = '';
		$aff_zone['zo'] ='';	
                                           $aff_zone['departement'] ='';         
		$aff_zone['adr'] =  '';
                                           $aff_zone['channel'] = '';
                                           $aff_zone['iso2']=     '';
                                           $aff_zone['cityname']  =  '';
                                           $aff_zone['ID_geocity']='';
                                           $aff_zone['zone_city']  = '';
                   
                    
                                      $inst_sys = new SYS();
                                     $sysidzn = explode('_',$zone);
                                     $sys_districtopt2 = $inst_sys->READ_ID_GEODISTRICT($sysidzn[5]);
                                     $sys_cityopt2 = $inst_sys->READ_ID_GEOCITY($sysidzn[4]);
                                      if(empty( $sys_districtopt2['district'])){
                                            $aff_zone['channel'] ='Oups ce canal n\'est pas très actif :(';
                                            $aff_zone['info_channel'] = 'Peut-être n\'avez-vous  pas indiqué votre quartier ou íbui n\'a aucun quartier à vous proposer pour votre ville .  ';
                                            $aff_zone['state'] =false;
                                    }else{
                                      $aff_zone['state'] =true;
		$aff_zone['gps_lat'] = $sys_districtopt2['gps_lat_district'];
		$aff_zone['gps_lon'] = $sys_districtopt2['gps_lon_district'];
		$aff_zone['zo'] = $sys_districtopt2['zoommap'];		
		$sys_departement = $inst_sys->READ_ID_GEODEPT($sys_cityopt2['departement']);
                                    
                                     $aff_zone['departement'] = $sys_departement['ID_geodept'];            
                                      $aff_zone['adr'] =  str_replace(' ','_',$sys_districtopt2['district']).'_'.str_replace(' ','_',$sys_cityopt2['city'] ).'_'.str_replace(' ','_',$sys_departement['departement']).'_'.str_replace(' ','_',$sys_cityopt2['iso2']) ;
		//$aff_zone['adr'] =  $sys_districtopt2['district']  .','.$sys_cityopt2['city'] .', '.$sys_departement['departement'].', '. $sys_cityopt2['iso2'] ;
                                     $aff_zone['channel'] = ucfirst(strtolower($sys_districtopt2['district'])) ; 
                                     $aff_zone['iso2']=       $sys_cityopt2['iso2'];
                                    // $aff_zone['info_channel'] = 'Hello! Bienvenue sur le canal  <span style="font-weight:bold;">'.$sys_districtopt2['district'].'</span>';
                                    }
                                     $aff_zone['cityname']  =  ucfirst(strtolower($sys_cityopt2['city']));
                                     $aff_zone['ID_geocity']=$sys_cityopt2['ID_geocity'];
                                      $aff_zone['zone_city']  =  $sys_cityopt2['ID_geocity'];
                                   
                                     return $aff_zone;
                }
                
                
                      
}


