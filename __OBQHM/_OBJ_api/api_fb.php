<?php
class api_fb extends core_Query {
     public  function ID_profil_fb() {
                        $fb = new Facebook\Facebook([
                       'app_id' => FB_APP_ID, // Replace {app-id} with your app id
                       'app_secret' => FB_SECRET,
                      'default_access_token' => FB_TOKEN, // optional
                       'default_graph_version' => 'v2.2',
                       ]);
                        
                       return $fb;
     }
    
    public  function login_profil_fb($fb) {
                     $helper = $fb->getRedirectLoginHelper();

                     $permissions = ['email']; // Optional permissions
                          $loginUrl = $helper->getLoginUrl(''.URL_RACINE_ID_m.'Query_fb.php', $permissions);
            
            return $loginUrl;        
    }    
    
    public  function Read_profil_fb($fb) {
   
            $helper = $fb->getRedirectLoginHelper();

            try {
              // Returns a `Facebook\FacebookResponse` object
                $accessToken = $helper->getAccessToken();

                $response = $fb->get('/me?fields=id,name,email',$accessToken);
            } catch(Facebook\Exceptions\FacebookResponseException $e) {
              echo 'Graph returned an error: ' . $e->getMessage();
              exit;
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
              echo 'Facebook SDK returned an error: ' . $e->getMessage();
              exit;
            }

            $user = $response->getGraphUser();	
            return $user;
    }	
	
    
}
?>
