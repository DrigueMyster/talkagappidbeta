var map;
var initialize;
 var lat;
 var long;
 var zo;
 
initialize = function(lat,long,zo){
  var latLng = new google.maps.LatLng(lat,long); // Correspond au coordonnées de Lille
  var myOptions = {
    zoom      : zo,
    center    : latLng,
     streetViewControl: false,
    mapTypeId : google.maps.MapTypeId.ROADMAP, // Type de carte, différentes valeurs possible HYBRID, ROADMAP, SATELLITE, TERRAIN
    maxZoom   : 20
  };
 
  map      = new google.maps.Map(document.getElementById('map-canvas'), myOptions);
  
    var marker = new google.maps.Marker({
    position : latLng,
    map      : map,
    title    : "Lille"
    //icon     : "marker_lille.gif" // Chemin de l'image du marqueur pour surcharger celui par défaut
  });
  
  
 
};
 
       