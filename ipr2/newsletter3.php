<?php
$inst_sys = new SYS();
$inst_Query = new core_Query();
$inst_routines= new core_Routines();
if(empty($_GET['viewbrowser'])){ $_GET['viewbrowser'] = false; }

$message='<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>TalkAG</title>
 <link href="https://fonts.googleapis.com/css?family=Exo" rel="stylesheet">
 		<style type="text/css">
			/* ----- Custom Font Import ----- */
			

			/* ----- Text Styles ----- */
			

			@media only screen and (max-width: 700px){
				/* ----- Base styles ----- */
				.full-width-container{
					padding: 0 !important;
				}

				.container{
					width: 100% !important;
				}

				/* ----- Header ----- */
				.header td{
					padding: 30px 15px 30px 15px !important;
				}

				/* ----- Projects list ----- */
				.projects-list{
					display: block !important;
				}

				.projects-list tr{
					display: block !important;
				}

				.projects-list td{
					display: block !important;
				}

				.projects-list tbody{
					display: block !important;
				}

				.projects-list img{
					margin: 0 auto 25px auto;
				}

				/* ----- Half block ----- */
				.half-block{
					display: block !important;
				}

				.half-block tr{
					display: block !important;
				}

				.half-block td{
					display: block !important;
				}

				.half-block__image{
					width: 100% !important;
					background-size: cover;
				}

				.half-block__content{
					width: 100% !important;
					box-sizing: border-box;
					padding: 25px 15px 25px 15px !important;
				}

				/* ----- Hero subheader ----- */
				.hero-subheader__title{
					padding: 80px 15px 15px 15px !important;
					font-size: 35px !important;
				}

				.hero-subheader__content{
					padding: 0 15px 90px 15px !important;
				}

				/* ----- Title block ----- */
				.title-block{
					padding: 0 15px 0 15px;
				}

				/* ----- Paragraph block ----- */
				.paragraph-block__content{
					padding: 25px 15px 18px 15px !important;
				}

				/* ----- Info bullets ----- */
				.info-bullets{
					display: block !important;
				}

				.info-bullets tr{
					display: block !important;
				}

				.info-bullets td{
					display: block !important;
				}

				.info-bullets tbody{
					display: block;
				}

				.info-bullets__icon{
					text-align: center;
					padding: 0 0 15px 0 !important;
				}

				.info-bullets__content{
					text-align: center;
				}

				.info-bullets__block{
					padding: 25px !important;
				}

				/* ----- CTA block ----- */
				.cta-block__title{
					padding: 35px 15px 0 15px !important;
				}

				.cta-block__content{
					padding: 20px 15px 27px 15px !important;
				}

				.cta-block__button{
					padding: 0 15px 0 15px !important;
				}
                                
                               
                                                                                   
                                                                                        
                                                                                  
                                                                                                                                                                                    .video-container {
                                                                                                    position: relative;
                                                                                                    padding-bottom: 56.25%;
                                                                                                    padding-top: 30px;
                                                                                                    height: 0;
                                                                                                    overflow: hidden;
                                                                                            }

                                                                                            .video-container iframe,  
                                                                                            .video-container object,  
                                                                                            .video-container embed {
                                                                                                    position: absolute;
                                                                                                    top: 0;
                                                                                                    left: 0;
                                                                                                    width: 100%;
                                                                                                    height: 100%;
                                                                                            }
			}
		</style>

</head>
<body style="font-family:Exo; background-color:#f4f4f4; margin:0; padding:0; color:#333333;">

<table width="100%" bgcolor="#f4f4f4" cellpadding="0" cellspacing="0" border="0">
    <tbody>
        <tr>
            <td style="padding:40px 0;">
                <!-- begin main block -->
                <table cellpadding="0" cellspacing="0" width="608" border="0" align="center">
                    <tbody>
                        <tr>
                            <td>
                            
                             <a href="https://www.talkag.com/" style="display:block; width:407px; height:50px; margin:0 auto 30px;">
                                    <img src="https://www.talkag.com/img_flx_cont/talkag.png" width="150"  alt="Pixelbuddha" style="display:block; border:0; margin:0;margin-left: auto;margin-right: auto;">
                                </a>
                                <p style="margin:0 0 36px; text-align:center; font-size:20px; line-height:20px; text-transform:uppercase; color:#626658;">
                                    Talk AG, Parlons agriculture
                                </p>';

                              
                                
 $message .='
 
                                <!-- begin wrapper -->
                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                    <tbody>
                                     
                                         
                                        <tr>
                                        
                                            <td colspan="3" rowspan="3" bgcolor="#FFFFFF" style="padding:0 0 30px;-moz-border-radius: 4px;   -webkit-border-radius: 4px;  border-radius:4px;">
                                               
                                                <!-- begin articles -->
                                                 
                                                  <a href="https://www.talkag.com/ID/" ><img src="https://www.talkag.com/img_flx_cont/shareta.jpg"  alt="talkAG" style="width:100%;height:auto;display:block; border:0; margin:0 0 44px; background:#eeeeee;"></a>
                                              
                                                <br> <p style="margin:0 30px 33px;font-size:24px; line-height:20px; font-weight:bold; color:#484a42;">
                                                   Bonjour  '.$_GET['NAME'].'
                                                </p>
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tbody>
                                                       
                                                        
                                                        
                                                     <tr valign="top">
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                            <td colspan="3">
                                                               
                                                                <p style="margin:0 0 35px; font-size:14px; line-height:18px; color:#2C2D30;">Talk<strong>AG</strong> le réseau social  pour les agriculteurs et les professionnels de l\'agriculture .
                                                               
                                                                
                                                            </td>
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                        </tr>
                                                        
                                                        
                                                          <tr valign="top" align="center">
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                            <td colspan="3">
                                                               
                                                               <br><br> <br><br>
                                                                
                                                            </td>
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;height: 20px;">&nbsp;</p></td>
                                                        </tr>
                                                        ';
                                             

//Aucune+Donne
$items = ' AND (';

 $exp_PRODUITS= explode('+',$_GET['PRODUITS']);
 
//ventes
 //activite
 //itmes base
 $p=0;
  $word='';
        for($i = 1; ; $i++){
                if ($i > count($exp_PRODUITS)-1) {  break;  }
                 if(strlen($exp_PRODUITS[$i]) > 3 AND $exp_PRODUITS[$i] != 'Aucune' AND $exp_PRODUITS[$i] != 'Donnee' and  $exp_PRODUITS[$i] != 'divers'  AND $exp_PRODUITS[$i] != 'autres'   AND $exp_PRODUITS[$i] !=  '(dont'  AND $exp_PRODUITS[$i] !=  'produits' AND $exp_PRODUITS[$i] !=  'base' AND $exp_PRODUITS[$i] !=  'spécialisé'){                       
                      $matching = $inst_Query->read_Query(''.SYS_QHMNETWORK,'',' `'.SYS_QHMNETWORK.'`.`zone` LIKE "%'.$_GET['cont'].'%" and`'.SYS_QHMNETWORK.'`.`content`   LIKE "%'.$exp_PRODUITS[$i].'%"  AND    `'.SYS_QHMNETWORK.'`.`statut` != "message"  AND  `'.SYS_QHMNETWORK.'`.`type` != "answer"   AND  `'.SYS_QHMNETWORK.'`.`category` != "friends"  AND  `'.SYS_QHMNETWORK.'`.`t_img` != ""  ORDER BY   `'.SYS_QHMNETWORK.'`.`ID_sys_qhmnetwork` DESC      LIMIT 5   ');
                      foreach($matching as $datamatching){  
                          $p ++;
                          
                      }
                     if($p > 0){
                     $items .=  '  `'.SYS_QHMNETWORK.'`.`content`   LIKE "%'.$exp_PRODUITS[$i].'%" OR  `'.SYS_QHMNETWORK.'`.`title`   LIKE "%'.$exp_PRODUITS[$i].'%" OR ';       
                 //    $word .= $p.'-'.$exp_PRODUITS[$i].', ';
                    }
                     $p = 0;
                         //  $word .= $exp_PRODUITS[$i].', ';
            
                            $i ++;
                }                      
      }

      if($i > 0){
                $items2 = substr($items, 0, -3);
                $items2 .= ')';
      }else{
                $items2='';
      }
      
   


         $message .='   <tr valign="top" >
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                            <td colspan="3" style="height:5px;">
                                                                <p style="margin:0 0 35px; font-size:14px; line-height:5px; color:#2C2D30;">Activité de votre fil d\'actualité: </p>
                                                            </td>
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                        </tr>
                                                    
                                                       ';     
   /*            $message .='   <tr valign="top" >
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                            <td colspan="3" style="height:5px;">
                                                                <p style="margin:0 0 35px; font-size:14px; line-height:20px; color:#2C2D30;">'.$word.' </p>
                                                            </td>
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                        </tr>
                                                    
                                                       ';  */
        
    
$message .=' <tr valign="top">';
  //

 $readflx1= $inst_Query->read_Query(''.SYS_QHMNETWORK,'',' `'.SYS_QHMNETWORK.'`.`zone` LIKE "%'.$_GET['cont'].'%"  '.$items2.'    AND    `'.SYS_QHMNETWORK.'`.`statut` != "message"  AND  `'.SYS_QHMNETWORK.'`.`type` != "answer"   AND  `'.SYS_QHMNETWORK.'`.`category` != "friends"  AND  `'.SYS_QHMNETWORK.'`.`t_img` != ""  ORDER BY   `'.SYS_QHMNETWORK.'`.`ID_sys_qhmnetwork` DESC      LIMIT 2   ');
  foreach($readflx1 as $datareadflx1){  
      
                                                   $message .='         
                                                       <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                            <td>
                                                                <a style="display:block; margin:0 0 14px;" href="https://www.talkag.com/share/?modif_data='.$datareadflx1['ID_sys_qhmnetwork'].'&lg='. $_GET['lg'].'"><img src="https://www.talkag.com/imgprofile'.$datareadflx1['t_img'].'" width="255" height="150" alt="More" style="display:block; margin:0; border:0; background:#eeeeee;-moz-border-radius: 4px;   -webkit-border-radius: 4px;  border-radius:4px;"></a>
                                                                <p style="font-size:14px; line-height:22px; font-weight:bold; color:#333333; margin:0 0 5px;"><a href="https://www.talkag.com/share/?modif_data='.$datareadflx1['ID_sys_qhmnetwork'].'&lg='. $_GET['lg'].'" style="color:#333;; text-decoration:none;">'.$datareadflx1['title'].'</a></p>
                                                                <p style="margin:0 0 35px; font-size:12px; line-height:18px; color:#333333;">'.substr(ucfirst(nl2br($datareadflx1['content'])),0,100).'</p>
                                                            </td>
                                                            
                                                          ';
  }        
  
  if(empty($datareadflx1['ID_sys_qhmnetwork']) ){
            $limit = 2;
        
            $readflx1B = $inst_Query->read_Query(''.SYS_QHMNETWORK,'',' `'.SYS_QHMNETWORK.'`.`zone` LIKE "%'.$_GET['cont'].'%"    AND    `'.SYS_QHMNETWORK.'`.`statut` != "message"  AND  `'.SYS_QHMNETWORK.'`.`type` != "answer"   AND  `'.SYS_QHMNETWORK.'`.`category` != "friends"  AND  `'.SYS_QHMNETWORK.'`.`t_img` != ""  ORDER BY   `'.SYS_QHMNETWORK.'`.`ID_sys_qhmnetwork` DESC      LIMIT 2  ');
            foreach($readflx1B as $datareadflx1B){  

                                                             $message .='         
                                                                 <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                                      <td>
                                                                          <a style="display:block; margin:0 0 14px;" href="https://www.talkag.com/share/?modif_data='.$datareadflx1B['ID_sys_qhmnetwork'].'&lg='. $_GET['lg'].'"><img src="https://www.talkag.com/imgprofile'.$datareadflx1B['t_img'].'" width="255" height="150" alt="More" style="display:block; margin:0; border:0; background:#eeeeee;-moz-border-radius: 4px;   -webkit-border-radius: 4px;  border-radius:4px;"></a>
                                                                          <p style="font-size:14px; line-height:22px; font-weight:bold; color:#333333; margin:0 0 5px;"><a href="https://www.talkag.com/share/?modif_data='.$datareadflx1B['ID_sys_qhmnetwork'].'&lg='. $_GET['lg'].'" style="color:#333;; text-decoration:none;">'.$datareadflx1B['title'].'</a></p>
                                                                          <p style="margin:0 0 35px; font-size:12px; line-height:18px; color:#333333;">'.substr(ucfirst(nl2br($datareadflx1B['content'])),0,100).'</p>
                                                                      </td>

                                                                    ';
            } 
  }
  
  
    $message .='              </tr>';
  
  $message .=' <tr valign="top">';
   $readflx = $inst_Query->read_Query(''.SYS_QHMNETWORK,'',' `'.SYS_QHMNETWORK.'`.`zone` LIKE "%'.$_GET['cont'].'%"  '.$items2.'    AND    `'.SYS_QHMNETWORK.'`.`statut` != "message"  AND  `'.SYS_QHMNETWORK.'`.`type` != "answer"   AND  `'.SYS_QHMNETWORK.'`.`category` != "friends"  AND  `'.SYS_QHMNETWORK.'`.`t_img` != ""  ORDER BY   `'.SYS_QHMNETWORK.'`.`ID_sys_qhmnetwork` DESC      LIMIT 2,2   ');
  foreach($readflx as $datareadflx){  
                                                   $message .='         
                                                       <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                            <td>
                                                                <a style="display:block; margin:0 0 14px;" href="https://www.talkag.com/share/?modif_data='.$datareadflx['ID_sys_qhmnetwork'].'&lg='. $_GET['lg'].'" ><img src="https://www.talkag.com/imgprofile'.$datareadflx['t_img'].'" width="255" height="150" alt="More" style="display:block; margin:0; border:0; background:#eeeeee;-moz-border-radius: 4px;   -webkit-border-radius: 4px;  border-radius:4px;"></a>
                                                                <p style="font-size:14px; line-height:22px; font-weight:bold; color:#333333; margin:0 0 5px;"><a href="https://www.talkag.com/share/?modif_data='.$datareadflx['ID_sys_qhmnetwork'].'&lg='. $_GET['lg'].'" style="color:#333;; text-decoration:none;">'.$datareadflx['title'].'</a></p>
                                                                 <p style="margin:0 0 35px; font-size:12px; line-height:18px; color:#333333;">'.substr(ucfirst(nl2br($datareadflx['content'])),0,100).'</p>
                                                            </td>
                                                            
                                                          ';
  }      
  
  
  $message .=' <tr valign="top">';
   $readflx = $inst_Query->read_Query(''.SYS_QHMNETWORK,'',' `'.SYS_QHMNETWORK.'`.`zone` LIKE "%'.$_GET['cont'].'%"  '.$items2.'    AND    `'.SYS_QHMNETWORK.'`.`statut` != "message"  AND  `'.SYS_QHMNETWORK.'`.`type` != "answer"   AND  `'.SYS_QHMNETWORK.'`.`category` != "friends"  AND  `'.SYS_QHMNETWORK.'`.`t_img` != ""  ORDER BY   `'.SYS_QHMNETWORK.'`.`ID_sys_qhmnetwork` DESC      LIMIT 4,2   ');
  foreach($readflx as $datareadflx){  
                                                   $message .='         
                                                       <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                            <td>
                                                                <a style="display:block; margin:0 0 14px;" href="https://www.talkag.com/share/?modif_data='.$datareadflx['ID_sys_qhmnetwork'].'&lg='. $_GET['lg'].'"><img src="https://www.talkag.com/imgprofile'.$datareadflx['t_img'].'" width="255" height="150" alt="More" style="display:block; margin:0; border:0; background:#eeeeee;-moz-border-radius: 4px;   -webkit-border-radius: 4px;  border-radius:4px;"></a>
                                                                <p style="font-size:14px; line-height:22px; font-weight:bold; color:#333333; margin:0 0 5px;"><a href="https://www.talkag.com/share/?modif_data='.$datareadflx['ID_sys_qhmnetwork'].'&lg='. $_GET['lg'].'" style="color:#333;; text-decoration:none;">'.$datareadflx['title'].'</a></p>
                                                                 <p style="margin:0 0 35px; font-size:12px; line-height:18px; color:#333333;">'.substr(ucfirst(nl2br($datareadflx['content'])),0,100).'</p>
                                                            </td>
                                                            
                                                          ';
  }   
  
  
  $message .=' <tr valign="top">';
   $readflx = $inst_Query->read_Query(''.SYS_QHMNETWORK,'',' `'.SYS_QHMNETWORK.'`.`zone` LIKE "%'.$_GET['cont'].'%"  '.$items2.'    AND    `'.SYS_QHMNETWORK.'`.`statut` != "message"  AND  `'.SYS_QHMNETWORK.'`.`type` != "answer"   AND  `'.SYS_QHMNETWORK.'`.`category` != "friends"  AND  `'.SYS_QHMNETWORK.'`.`t_img` != ""  ORDER BY   `'.SYS_QHMNETWORK.'`.`ID_sys_qhmnetwork` DESC      LIMIT 6,2   ');
  foreach($readflx as $datareadflx){  
                                                   $message .='         
                                                       <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                            <td>
                                                                <a style="display:block; margin:0 0 14px;" href="https://www.talkag.com/share/?modif_data='.$datareadflx['ID_sys_qhmnetwork'].'&lg='. $_GET['lg'].'"><img src="https://www.talkag.com/imgprofile'.$datareadflx['t_img'].'" width="255" height="150" alt="More" style="display:block; margin:0; border:0; background:#eeeeee;-moz-border-radius: 4px;   -webkit-border-radius: 4px;  border-radius:4px;"></a>
                                                                <p style="font-size:14px; line-height:22px; font-weight:bold; color:#333333; margin:0 0 5px;"><a href="https://www.talkag.com/share/?modif_data='.$datareadflx['ID_sys_qhmnetwork'].'&lg='. $_GET['lg'].'" style="color:#333;; text-decoration:none;">'.$datareadflx['title'].'</a></p>
                                                                 <p style="margin:0 0 35px; font-size:12px; line-height:18px; color:#333333;">'.substr(ucfirst(nl2br($datareadflx['content'])),0,100).'</p>
                                                            </td>
                                                            
                                                          ';
  }   
                                          $message .='              </tr>
                                                        
                                                  
                                                        
                                                  
                             
                                                    </tbody>
                                                </table>
                                                <!-- /end articles -->
                                                <p style="margin:0; border-top:2px solid #e5e5e5; font-size:5px; line-height:5px; margin:0 30px 29px;">&nbsp;</p>
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tbody>
                                                        <tr valign="top">
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                            <td>
                                                                <p style="margin:0 0 4px; font-weight:bold; color:#333333; font-size:14px; line-height:22px;">TalkAG</p>
                                                                <p style="margin:0; color:#333333; font-size:11px; line-height:18px;">
                                                                    <br>
                                                                     <a href="https://www.talkag.com/" style="color:#e3001a; text-decoration:none; font-weight:bold;">www.talkag.com</a>
                                                                </p>
                                                            </td>
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                            <td width="120">
                                                                <a href="https://www.facebook.com/Talk-AG-Parlons-agriculture-192238808048333/" style="float:left; width:24px; height:24px; margin:6px 8px 10px 0;">
                                                                    <img src="https://www.talkag.com/img_flx_cont/facebook.png" width="24" height="24" alt="facebook" style="display:block; margin:0; border:0; background:#eeeeee;">
                                                                </a>
                                                              <p style="margin:0; font-weight:bold; clear:both; font-size:12px; line-height:22px;">
                                                                    <a href="https://www.facebook.com/Talk-AG-Parlons-agriculture-192238808048333/" style="color:#e3001a; text-decoration:none;">Notre page Facebook</a><br>
                                                                   
                                                                </p>
                                                             
                                                            </td>
                                                            <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- end content --> 
                                            </td>
                                            
                                        </tr>
                                   
                                    </tbody>
                                </table>
                                <!-- end wrapper-->
                                         <p style="margin:0; padding:34px 0 0; text-align:center; font-size:11px; line-height:13px; color:#333333;">
                                            Ce message a été envoyé à <strong> '.$_GET['EMAIL'].'</strong>
                                         </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- end main block -->
            </td>
        </tr>
    </tbody>
</table>
</body>
</html>
';