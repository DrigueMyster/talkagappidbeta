<?php
ini_set('display_errors','off');
session_start();

include '__config_data.php'; // tables
include '__AL.php';

if(empty($_POST['lg'])){ $_POST['lg'] = 'en'; }
include '../LG/'.$_POST['lg'].'/data_lg.php';
$inst_sys = new SYS();
$inst_Query = new core_Query();
$inst_routines= new core_Routines();
 $core_Model = new core_model();
 $inst_fuser = new modules_Req_fuser();

$DATA_sys = $inst_sys->READ_SYS_process_by_Session_SID(); // verif si session toujours active et meme ID_session()    
$err = false;
  
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= CHARSET ?>"/>
        <title>groups</title>
        <?php
        
     if(empty($_GET['addusergrp'])){ $_GET['addusergrp']= false; }
     if(empty($_POST['type'])){ $_GET['type']= ''; }
        
        if ($_POST['rQ'] == 'flx' ) {
                     foreach($_POST as $indice => $valeur){ $dataf[str_replace('_' . $_POST['rQ'] . '', '', $indice)] = $_POST[$indice];    }
                     
                        if (empty($dataf['content']) ) {
                            $err = true;
                            echo '<script type="text/javascript">parent.document.getElementById("textbarerror").innerHTML = "'.ucfirst(ERROR_CONTENTGRP).'"</script>';
                        }elseif (empty($dataf['title']) ) {
                            $err = true;
                            echo '<script type="text/javascript">parent.document.getElementById("textbarerror").innerHTML = "'.ucfirst(ERROR_CONTENTGRP).'"</script>';    
                   
                        } else {
                            echo '<script type="text/javascript">parent.document.getElementById("head_barerror").style.display = "none"</script>';
                        }
                        if($dataf['category'] == 'select' and $_GET['impggrp'] != true){
                           
                                if($inst_sys->READ_SYS_process_by_TYPEprocess('CREATGRPuser'.$_POST['IDgroup'].'',$DATA_sys['ID_sys_users']) == false){
                                        $err = true;
                                        echo '<script type="text/javascript">parent.document.getElementById("textbarerror").innerHTML = "Vous devez indiquer des personnes à inviter"</script>';    
                                }else {
                                        echo '<script type="text/javascript">parent.document.getElementById("head_barerror").style.display = "none"</script>';
                                }
                             
                        }
                        
            if ($err == true ) {
             
	echo '<script type="text/javascript">parent.document.getElementById("head_barerror").style.display = "block"</script>';
	echo '<script type="text/javascript">parent.document.getElementById("prgsflxnextvalidpublish").style.display = "block"</script>';
	echo '<script type="text/javascript">parent.document.getElementById("2prgsflxnextvalidpublish").style.display = "none"</script>';
               
            } else {

                $dataf['t_img'] ='';
                if (!empty($_POST['upload_image_flximg'])) {
                    $dataimg['upload_image'] = $_POST['upload_image_flximg'];
                    $dataimg['url_qhm'] = $dataf['url_qhm'];
                    $req_img = $inst_sys->valid_flx_img($dataimg);
                    $dataf['t_img'] = $req_img['t_img'];
               
                }
                
                
           
                       
                if (!empty($_POST['upload_image_flxaudio'])) {
                    
                    $dataimg['upload_image'] = $_POST['upload_image_flxaudio'];
                    $dataimg['url_qhm'] = $dataf['url_qhm'];
                    $req_audio = $inst_sys->valid_flx_img($dataimg);
                    $dataf['t_sound'] =$req_audio['t_img'];
                    $dataf['title'] = $dataf['title_fm2'];
                    
                            $cover = base64_decode($dataf['t_imgsrc']);
                            if(!empty($cover)){                                                             
                                    if($inst_routines-> checkurlFileImg($cover) == false  ){
                                                 $cover = ''.URL_MASTER.'_IMG/soundDefault.png';
                                    }

                                    $ext = $inst_routines->checkurlFileImg($dataf['t_imgsrc']) ;
                                    if(empty($ext)){
                                                  $cover = ''.URL_MASTER.'_IMG/soundDefault.png';
                                                  $ext = 'png';
                                    }      
                                  
                                    $file = 'flx'.$DATA_sys['ID_ste'].''.$DATA_sys['ID_sys_users'].''.date("Ymd").''.date("Ymd").''.date("His").'.'.$ext.'';
                                    $inst_routines->downloadurlFileImg($dataf['t_imgsrc'],''.IMG_FLX.''. $_POST['url_qhm'].''.$file.''); 
                                    $dataf['t_img'] =  ''.IMG_FLX.''. $_POST['url_qhm'].''.$file.'' ;
                            }                    
                }
        
                if (!empty($_POST['upload_image_flxvideo'])) {
                    $dataimg['upload_image'] = $_POST['upload_image_flxvideo'];
                    $dataimg['url_qhm'] = $dataf['url_qhm'];
                    $req_audio = $inst_sys->valid_flx_img($dataimg);
                    $dataf['t_movies'] = str_replace('./../', URLSSL_MASTER, $req_audio['t_img']) ;
                    
                            $cover = base64_decode($dataf['t_imgsrc']);
                            if(!empty($cover)){                                                             
                                    if($inst_routines-> checkurlFileImg($cover) == false  ){
                                                 $cover = ''.URL_MASTER.'_IMG/videoDefault.png';
                                    }

                                    $ext = $inst_routines->checkurlFileImg($dataf['t_imgsrc']) ;
                                    if(empty($ext)){
                                                  $cover = ''.URL_MASTER.'_IMG/videoDefault.png';
                                                  $ext = 'png';
                                    }      
                                   // $DATA_sys = $inst_sys->READ_SYS_process_by_Session_SID(); // verif si session toujours active et meme ID_session()       
                                    $file = 'flx'.$DATA_sys['ID_ste'].''.$DATA_sys['ID_sys_users'].''.date("Ymd").''.date("Ymd").''.date("His").'.'.$ext.'';
                                    $inst_routines->downloadurlFileImg($dataf['t_imgsrc'],''.IMG_FLX.''. $_POST['url_qhm'].''.$file.''); 
                                    $dataf['t_img'] =  ''.IMG_FLX.''. $_POST['url_qhm'].''.$file.'' ;
                            }                    
                }                

                    if (!empty($_POST['uploadlink_fm3'])) {
                        $parts = explode(',', $_POST['uploadlink_fm3']);
                        $data = $parts[1];
                        $data = base64_decode($data);
                        $dataf['t_img'] = '' . $_POST['type_form_flximg_uploadfm3'] . '.png';
                        file_put_contents('' . IMG_FLX . '' . $dataf['url_qhm'] . '' . $dataf['t_img'] . '', $data);
                    }
                    
                 
                    
                      if (empty($dataf['category'])) { $dataf['category'] = 'publish'; }
                      
                      if (empty($dataf['content'])) { $dataf['content'] = ''; }
                      $dataf['content'] = $dataf['content'].' '.$hashitems;
                      $dataf['content'] = strip_tags($dataf['content'] );
                   
                  
                    if (!empty($_POST['t_movies_flx'])) {
                        $dataf['t_movies'] = base64_decode($_POST['t_movies_flx']);

                    }            
                    
                     if(!empty($_POST['title_flx'])){ $dataf['title'] = $_POST['title_flx']; }
                     
                     $dataf['type'] = $_POST['type'];
                     
                        if(!empty($_GET['modif_data']) ){         $req_account['key'] = $_GET['modif_data']; }
                        
                       if($_GET['addusergrp'] == false)  { 
                                if(!empty($_GET['modif_data']) ){

                                         if(empty($dataf['t_img'])){ $dataf['t_img'] = $_POST['t_imgACTU']; }
                                         $inst_Query->update_Query(SYS_GROUPS, '  `' . SYS_GROUPS . '`.`ID_sys_groups` =  "'.$_GET['modif_data'].'"     ', $dataf);
                                         $req_account['key'] = $_GET['modif_data'];
                                }else{
                                         $req_account = $inst_sys->INSERT_SYS_groups($dataf);
                                }
                       }
        
        
                       if(!empty($dataf['t_img'])){ $data['t_link_img'] = $dataf['t_img'] ; }
                       
                      if( $req_account['key'] > 0  and $_GET['addusergrp'] == true ){ //LANCEMENT DES INVITATION
                                $data['t_link_img']  = $_POST['t_imgACTU']; 
                                $DATA_user = $inst_fuser->READ_fuser_by_ID_sys_users($DATA_sys['ID_sys_users']);
                            
                            
                                $listinvit = $inst_sys->READ_SYS_process_list_TYPEprocess('CREATGRPuser'.$_POST['IDgroup'].'');
                                 $data['objetnotif'] =''. $DATA_user['users_prenom'].' '. $DATA_user['users_nom'].'  vous invite à rejoindre son groupe de discussion:  '.$dataf['title'].'';
                                 $data['objet'] =''. $DATA_user['users_prenom'].' '. $DATA_user['users_nom'].'  vous invite à rejoindre son groupe de discussion: '.$dataf['title'].' '.$dataf['content'].'';
                                 $t_link ='sys=desktop.invitgroup&ID_sys_groups='.$req_account['key'].'&lg='.$_POST['lg'].'';  
                                 foreach ($listinvit as $DATA_LISTinvit) {
                                        $inst_Query->update_Query(SYS_PROCESS, '  `' . SYS_PROCESS . '`.`TYPE_process` =  "CREATGRPuser'.$_POST['IDgroup'].'"  AND `' . SYS_PROCESS . '`.`VARrQ`  =  "'.$DATA_LISTinvit['VARrQ'].'"    ', $dataINTRABASE = array('TYPE_process' => 'INVITGRPuser'.$req_account['key'] .''));
                             
                                        $DATA_recipientuser = $inst_fuser->READ_fuser_by_ID_sys_users($DATA_LISTinvit['VARrQ']);
                                    
                                        $req_notify = $inst_sys->INSERT_SYS_notify($data_notify=array('lnk_button' => ''.URL_RACINE.'?sys=desktop.invitgroup&ID_sys_groups='.$req_account['key'].'&lg='.$_POST['lg'].'', 'objetnotif' => $data['objetnotif'] ,'Recipient_ID_ste'=> $DATA_recipientuser['ID_ste']   ,'Recipient_ID_sys_users'=> $DATA_recipientuser['ID_sys_users']  , 'Recipient_email'=> $DATA_recipientuser['login_user'] ,'content'=>'invitgroup','cc_email'=>1,'t_notif'=>'invitgroup','Sender_email'=>$DATA_user['login_user'],'Sender_ID_ste'=>$DATA_sys['ID_ste'],'Sender_ID_sys_users'=>$DATA_sys['ID_sys_users'],'t_link'=>$t_link,'t_link_img'=>$data['t_link_img'],'state'=>'0','object' =>$data['objet'],'lg'=> $_POST['lg']));
                                      
                                     }
                            
                              
                       }elseif($dataf['category'] == 'select' AND $req_account['key'] > 0  and $_GET['addusergrp'] == false ){ //LANCEMENT DES INVITATION
                            
                                $DATA_user = $inst_fuser->READ_fuser_by_ID_sys_users($DATA_sys['ID_sys_users']);
                                
                                $listinvit = $inst_sys->READ_SYS_process_list_TYPEprocess('CREATGRPuser'.$_POST['IDgroup'].'');
                                $data['objetnotif'] =''. $DATA_user['users_prenom'].' '. $DATA_user['users_nom'].'  vous invite à rejoindre son groupe de discussion:  '.$dataf['title'].'';
                                $data['objet'] =''. $DATA_user['users_prenom'].' '. $DATA_user['users_nom'].'  vous invite à rejoindre son groupe de discussion: '.$dataf['title'].' '.$dataf['content'].'';
                                $t_link ='sys=desktop.invitgroup&ID_sys_groups='.$req_account['key'].'&lg='.$_POST['lg'].'';  
                                 foreach ($listinvit as $DATA_LISTinvit) {
                                        $inst_Query->update_Query(SYS_PROCESS, '  `' . SYS_PROCESS . '`.`TYPE_process` =  "CREATGRPuser'.$_POST['IDgroup'].'"  AND `' . SYS_PROCESS . '`.`VARrQ`  =  "'.$DATA_LISTinvit['VARrQ'].'"     ', $dataINTRABASE = array('TYPE_process' => 'INVITGRPuser'.$req_account['key'] .''));
                                       
                                        $DATA_recipientuser = $inst_fuser->READ_fuser_by_ID_sys_users($DATA_LISTinvit['VARrQ']);
                                        
                                        $req_notify = $inst_sys->INSERT_SYS_notify($data_notify=array('lnk_button' => ''.URL_RACINE.'?sys=desktop.invitgroup&ID_sys_groups='.$req_account['key'].'&lg='.$_POST['lg'].'', 'objetnotif' => $data['objetnotif'] ,'Recipient_ID_ste'=> $DATA_recipientuser['ID_ste']   ,'Recipient_ID_sys_users'=> $DATA_recipientuser['ID_sys_users']  , 'Recipient_email'=> $DATA_recipientuser['login_user'] ,'content'=>'invitgroup','cc_email'=>1,'t_notif'=>'invitgroup','Sender_email'=>$DATA_user['login_user'],'Sender_ID_ste'=>$DATA_sys['ID_ste'],'Sender_ID_sys_users'=>$DATA_sys['ID_sys_users'],'t_link'=>$t_link,'t_link_img'=>$data['t_link_img'],'state'=>'0','object' =>$data['objet'],'lg'=> $_POST['lg']));
                                 }
                            
                              
                       }else if($dataf['category'] == 'friends' AND $req_account['key'] > 0  and $_GET['addusergrp'] == false ){ 
                                $LIST_flxuser = $inst_sys->READ_SYS_flxuserlist($DATA_sys['ID_ste'],$DATA_sys['ID_sys_users'] ,''.''.''.''.'', '','','','friends');
                                $DATA_user = $inst_fuser->READ_fuser_by_ID_sys_users($DATA_sys['ID_sys_users']);
                                
                                   
                                 
                                $data['objetnotif'] =''. $DATA_user['users_prenom'].' '. $DATA_user['users_nom'].'  vous invite à rejoindre son groupe de discussion: '.$dataf['title'].'';
                                $data['objet'] =''. $DATA_user['users_prenom'].' '. $DATA_user['users_nom'].'  vous invite à rejoindre son groupe de discussion: <strong>'.$dataf['title'].'</strong><br><br>'.$dataf['content'].'';
                                $t_link ='sys=desktop.invitgroup&ID_sys_groups='.$req_account['key'].'&lg='.$_POST['lg'].'';  
                                
                                
                                foreach ( $LIST_flxuser as $DATA_LIST_flx) {
                                          $LIST_flxuserconfirme = $inst_sys->READ_subscrib_by_ID('',$DATA_LIST_flx['ID_sys_users'],'',$DATA_sys['ID_sys_users']);
                                         if($LIST_flxuserconfirme['ID_sys_users'] == $DATA_sys['ID_sys_users']){
                                             
                                                    $DATA_recipientuser = $inst_fuser->READ_fuser_by_ID_sys_users($DATA_LIST_flx['ID_sys_users']);
                                                    $inst_sys->insert_SYS_process($dataf=array('VARrQ' =>$DATA_LIST_flx['ID_sys_users'],'TYPE_process' =>  'INVITGRPuser'.$req_account['key'].'','ID_sys_users' => $DATA_sys['ID_sys_users'] ));
                                                     $req_notify = $inst_sys->INSERT_SYS_notify($data_notify=array('lnk_button' => ''.URL_RACINE.'?sys=desktop.invitgroup&ID_sys_groups='.$req_account['key'].'&lg='.$_POST['lg'].'', 'objetnotif' => $data['objetnotif'] ,'Recipient_ID_ste'=> $DATA_LIST_flx['ID_ste']   ,'Recipient_ID_sys_users'=> $DATA_LIST_flx['ID_sys_users']  , 'Recipient_email'=> $DATA_LIST_flx['login_user'] ,'content'=>'invitgroup','cc_email'=>1,'t_notif'=>'invitgroup','Sender_email'=>$DATA_user['login_user'],'Sender_ID_ste'=>$DATA_sys['ID_ste'],'Sender_ID_sys_users'=>$DATA_sys['ID_sys_users'],'t_link'=>$t_link,'t_link_img'=>$data['t_link_img'],'state'=>'0','object' =>$data['objet'],'lg'=> $_POST['lg']));
                                         }
                                }
                              
                       }
            }
          

            if ($err == true) {
                
            } elseif ($err == 'sessOUT') {
                session_regenerate_id();
            } else {
                    ?><script type="text/javascript">top.Remove_winBoxAlertQHMw('<?= $_POST['lg'] ?>');</script><?php  
                    if($_GET['addusergrp'] == false){
                         ?><script>top.initint('<?=  URL_RACINE_DESK_m ?>','sys=desktop.group&ID_sys_groups=<?= $req_account['key'] ?>&lg=<?= $_POST['lg'] ?>');</script><?php
                    }
               
            }
            
            } else if ($_POST['rQ'] == 'ANNULrequestgrp' ) { //demande à rejoidre un groupe      
                 $inst_Query->delete_Query(SYS_PROCESS, '`'.SYS_PROCESS.'`.`TYPE_process`= "REQUESTGRPuser'.$_GET['modif_data'].'" AND `'.SYS_PROCESS.'`.`ID_sys_users`="'.$DATA_sys['ID_sys_users'].'" ' );    
                    $inst_Query->delete_Query(SYS_NOTIFY, '`'.SYS_NOTIFY.'`.`Sender_ID_sys_users`= "'.$DATA_sys['ID_sys_users'].'" AND `'.SYS_NOTIFY.'`.`Recipient_ID_sys_users`="'.$_GET['grpID_sys_users'].'"  AND `'.SYS_NOTIFY.'`.`t_notif`="requestgroup" ' );    
                    
                  ?><script>top.initint('<?=  URL_RACINE_DESK_m ?>','sys=desktop.allgroup&lg=<?= $_POST['lg'] ?>');</script><?php 
                    ?><script type="text/javascript">top.Remove_winBoxAlertQHMw('<?= $_POST['lg'] ?>');</script><?php 
            
            
          } else if ($_POST['rQ'] == 'REJECTrequestgrp' ) { //demande à rejoidre un groupe    
                    $inst_Query->delete_Query(SYS_PROCESS, '`'.SYS_PROCESS.'`.`TYPE_process`= "REQUESTGRPuser'.$_GET['modif_data'].'" AND `'.SYS_PROCESS.'`.`ID_sys_users`="'.$_GET['grpuserID_sys_users'].'" ' );    
                    ?><script>top.initint('<?=  URL_RACINE_DESK_m ?>','sys=desktop.allgroup&lg=<?= $_POST['lg'] ?>');</script><?php 
                    ?><script type="text/javascript">top.Remove_winBoxAlertQHMw('<?= $_POST['lg'] ?>');</script><?php 
            
            
        } else if ($_POST['rQ'] == 'ACCEPTrequestgrp' ) { //accepter à rejoidre un groupe     
                    $DATA_usergrp = $inst_fuser->READ_fuser_by_ID_sys_users($_GET['grpuserID_sys_users']); //le demanadeur
                    $DATA_user = $inst_fuser->READ_fuser_by_ID_sys_users($DATA_sys['ID_sys_users']); //proprio de grp
                    $DATA_LIST_grp = $inst_sys->READ_group($_GET['modif_data']);
                    $inst_Query->delete_Query(SYS_PROCESS, '`'.SYS_PROCESS.'`.`TYPE_process`= "REQUESTGRPuser'.$DATA_LIST_grp['ID_sys_groups'].'" AND `'.SYS_PROCESS.'`.`ID_sys_users`="'.$_GET['grpuserID_sys_users'].'" ' );    
                    $inst_sys->insert_SYS_process($dataf=array('VARrQ' =>$_GET['grpuserID_sys_users'],'TYPE_process' =>  'GRPuser'.$DATA_LIST_grp['ID_sys_groups'].'','ID_sys_users' => $DATA_LIST_grp['ID_sys_users'] ));
                  
                     if(!empty($DATA_LIST_grp['t_img'])){ $data['t_link_img'] = $DATA_LIST_grp['t_img'] ; }
                    $data['objetnotif'] =''. $DATA_user['users_prenom'].' '. $DATA_user['users_nom'].'  a accepté votre demande pour rejoindre le  groupe de discussion:  <strong>'.$DATA_LIST_grp['title'].'</strong>';
                    $data['objet'] =''. $DATA_user['users_prenom'].' '. $DATA_user['users_nom'].'   a accepté votre demande pour rejoindre le  groupe de discussion: <strong>'.$DATA_LIST_grp['title'].'</strong>';
                    $t_link ='sys=desktop.group&ID_sys_groups='.$_GET['modif_data'].'&lg='.$_POST['lg'].'';  
                    $req_notify = $inst_sys->INSERT_SYS_notify($data_notify=array('lnk_button' => ''.URL_RACINE.'?sys=desktop.group&ID_sys_groups='.$_GET['modif_data'].'&lg='.$_POST['lg'].'', 'objetnotif' => $data['objetnotif'] ,'Recipient_ID_ste'=> $DATA_usergrp['ID_ste']   ,'Recipient_ID_sys_users'=> $DATA_usergrp['ID_sys_users']  , 'Recipient_email'=> $DATA_usergrp['login_user'] ,'content'=>'invitgroup','cc_email'=>1,'t_notif'=>'invitgroup','Sender_email'=>$DATA_user['login_user'],'Sender_ID_ste'=>$DATA_sys['ID_ste'],'Sender_ID_sys_users'=>$DATA_sys['ID_sys_users'],'t_link'=>$t_link,'t_link_img'=>$data['t_link_img'],'state'=>'0','object' =>$data['objet'],'lg'=> $_POST['lg']));
                    ?><script>top.initint('<?=  URL_RACINE_DESK_m ?>','sys=desktop.allgroup&lg=<?= $_POST['lg'] ?>');</script><?php 
                    ?><script type="text/javascript">top.Remove_winBoxAlertQHMw('<?= $_POST['lg'] ?>');</script><?php 
            
              
        } else if ($_POST['rQ'] == 'requestgrp' ) { //demande à rejoidre un groupe
                    $DATA_usergrp = $inst_fuser->READ_fuser_by_ID_sys_users($_GET['grpID_sys_users']);
                    $DATA_user = $inst_fuser->READ_fuser_by_ID_sys_users($DATA_sys['ID_sys_users']);
                    $DATA_LIST_grp = $inst_sys->READ_group($_GET['modif_data']);

                    $inst_sys->insert_SYS_process($dataf=array('VARrQ' =>$DATA_LIST_grp['ID_sys_users'],'TYPE_process' =>  'REQUESTGRPuser'.$_GET['modif_data'].'','ID_sys_users' => $DATA_sys['ID_sys_users'] ));
                     if(!empty($DATA_LIST_grp['t_img'])){ $data['t_link_img'] = $DATA_LIST_grp['t_img'] ; }
                    $data['objetnotif'] =''. $DATA_user['users_prenom'].' '. $DATA_user['users_nom'].'  fait une demande pour se joindre à votre groupe de discussion:  <strong>'.$DATA_LIST_grp['title'].'</strong>';
                    $data['objet'] =''. $DATA_user['users_prenom'].' '. $DATA_user['users_nom'].'  fait une demande pour se joindre à votre groupe de discussion:  <strong>'.$DATA_LIST_grp['title'].'</strong>';
                    $t_link ='sys=desktop.allgroup&lg='.$_POST['lg'].'';  
                    $req_notify = $inst_sys->INSERT_SYS_notify($data_notify=array('lnk_button' => ''.URL_RACINE.'?sys=desktop.allgroup&lg='.$_POST['lg'].'', 'objetnotif' => $data['objetnotif'] ,'Recipient_ID_ste'=> $DATA_LIST_grp['ID_ste']   ,'Recipient_ID_sys_users'=> $DATA_LIST_grp['ID_sys_users']  , 'Recipient_email'=> $DATA_usergrp['login_user'] ,'content'=>'invitgroup','cc_email'=>1,'t_notif'=>'requestgroup','Sender_email'=>$DATA_user['login_user'],'Sender_ID_ste'=>$DATA_sys['ID_ste'],'Sender_ID_sys_users'=>$DATA_sys['ID_sys_users'],'t_link'=>$t_link,'t_link_img'=>$data['t_link_img'],'state'=>'0','object' =>$data['objet'],'lg'=> $_POST['lg']));
                    ?><script>top.initint('<?=  URL_RACINE_DESK_m ?>','sys=desktop.allgroup&lg=<?= $_POST['lg'] ?>');</script><?php 
                    ?><script type="text/javascript">top.Remove_winBoxAlertQHMw('<?= $_POST['lg'] ?>');</script><?php 

        } else if ($_POST['rQ'] == 'rejectgrp' ) {
                        if(!empty($_GET['modif_data']) and !empty($DATA_sys['ID_sys_users']) ){
                                            $inst_Query->delete_Query(SYS_PROCESS, '`'.SYS_PROCESS.'`.`TYPE_process`= "INVITGRPuser'.$_GET['modif_data'].'" AND `'.SYS_PROCESS.'`.`VARrQ`="'.$DATA_sys['ID_sys_users'].'" ' );    
                                            echo '<script type="text/javascript">parent.document.getElementById("grpinvitstandby'.$_GET['modif_data'].'").style.display = "none"</script>';
                                          
                                             ?><script type="text/javascript">top.Remove_winBoxAlertQHMw('<?= $_POST['lg'] ?>');</script><?php    
                        }
                        
         } else if ($_POST['rQ'] == 'quitgrp' ) {
                        if(!empty($_GET['modif_data']) and !empty($DATA_sys['ID_sys_users']) ){
                                            $inst_Query->delete_Query(SYS_PROCESS, '`'.SYS_PROCESS.'`.`TYPE_process`= "GRPuser'.$_GET['modif_data'].'" AND `'.SYS_PROCESS.'`.`VARrQ`="'.$DATA_sys['ID_sys_users'].'" ' );    
                                      /*      echo '<script type="text/javascript">parent.document.getElementById("membergrp'.$_GET['modif_data'].'").style.display = "none"</script>';*/
                                          
                                           
                                            ?><script>top.initint('<?=  URL_RACINE_DESK_m ?>','sys=desktop.allgroup&lg=<?= $_POST['lg'] ?>');</script><?php 
                                            ?><script type="text/javascript">top.Remove_winBoxAlertQHMw('<?= $_POST['lg'] ?>');</script><?php 
                        }
 
         } else if ($_POST['rQ'] == 'closegrp' ) {
              if(!empty($_GET['modif_data']) and !empty($DATA_sys['ID_sys_users']) ){
                                $inst_Query->update_Query(SYS_GROUPS, '  `' . SYS_GROUPS . '`.`ID_sys_groups` =  "'.$_GET['modif_data'].'"     ', $dataINTRABASE = array('state' => '1'));
                             
                                ?><script>top.initint('<?=  URL_RACINE_DESK_m ?>','sys=desktop.group&ID_sys_groups=<?= $_GET['modif_data'] ?>&lg=<?= $_POST['lg'] ?>');</script><?php 
                                ?><script type="text/javascript">top.Remove_winBoxAlertQHMw('<?= $_POST['lg'] ?>');</script><?php 
              }
            } else if ($_POST['rQ'] == 'opengrp' ) {
                                 if(!empty($_GET['modif_data']) and !empty($DATA_sys['ID_sys_users']) ){
                                 $inst_Query->update_Query(SYS_GROUPS, '  `' . SYS_GROUPS . '`.`ID_sys_groups` =  "'.$_GET['modif_data'].'"     ', $dataINTRABASE = array('state' => '0'));
                             
                        
                               ?><script>top.initint('<?=  URL_RACINE_DESK_m ?>','sys=desktop.group&ID_sys_groups=<?= $_GET['modif_data'] ?>&lg=<?= $_POST['lg'] ?>');</script><?php 
                              ?><script type="text/javascript">top.Remove_winBoxAlertQHMw('<?= $_POST['lg'] ?>');</script><?php
              }
        }
        
        
        ?>

    </head>
    <body></body>
</html>