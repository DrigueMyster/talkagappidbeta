<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>íbui - invitation</title>
		<style type="text/css">
			/* ----- Custom Font Import ----- */
			@import url(https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic&subset=latin,latin-ext);

			/* ----- Text Styles ----- */
			table{
				font-family: Arial, sans-serif;
				-webkit-font-smoothing: antialiased;
				-moz-font-smoothing: antialiased;
				font-smoothing: antialiased;
			}

			@media only screen and (max-width: 700px){
				/* ----- Base styles ----- */
				.full-width-container{
					padding: 0 !important;
				}

				.container{
					width: 100% !important;
				}

				/* ----- Header ----- */
				.header td{
					padding: 30px 15px 30px 15px !important;
				}

				/* ----- Projects list ----- */
				.projects-list{
					display: block !important;
				}

				.projects-list tr{
					display: block !important;
				}

				.projects-list td{
					display: block !important;
				}

				.projects-list tbody{
					display: block !important;
				}

				.projects-list img{
					margin: 0 auto 25px auto;
				}

				/* ----- Half block ----- */
				.half-block{
					display: block !important;
				}

				.half-block tr{
					display: block !important;
				}

				.half-block td{
					display: block !important;
				}

				.half-block__image{
					width: 100% !important;
					background-size: cover;
				}

				.half-block__content{
					width: 100% !important;
					box-sizing: border-box;
					padding: 25px 15px 25px 15px !important;
				}

				/* ----- Hero subheader ----- */
				.hero-subheader__title{
					padding: 80px 15px 15px 15px !important;
					font-size: 35px !important;
				}

				.hero-subheader__content{
					padding: 0 15px 90px 15px !important;
				}

				/* ----- Title block ----- */
				.title-block{
					padding: 0 15px 0 15px;
				}

				/* ----- Paragraph block ----- */
				.paragraph-block__content{
					padding: 25px 15px 18px 15px !important;
				}

				/* ----- Info bullets ----- */
				.info-bullets{
					display: block !important;
				}

				.info-bullets tr{
					display: block !important;
				}

				.info-bullets td{
					display: block !important;
				}

				.info-bullets tbody{
					display: block;
				}

				.info-bullets__icon{
					text-align: center;
					padding: 0 0 15px 0 !important;
				}

				.info-bullets__content{
					text-align: center;
				}

				.info-bullets__block{
					padding: 25px !important;
				}

				/* ----- CTA block ----- */
				.cta-block__title{
					padding: 35px 15px 0 15px !important;
				}

				.cta-block__content{
					padding: 20px 15px 27px 15px !important;
				}

				.cta-block__button{
					padding: 0 15px 0 15px !important;
				}
                                
                                
                                                                                        .button {
                                                                                            display: inline-block;
                                                                                            zoom: 1; /* zoom and *display = ie7 hack for display:inline-block */
                                                                                            display: inline;
                                                                                            vertical-align: baseline;
                                                                                            margin: 0 2px;
                                                                                            outline: none;
                                                                                            cursor: pointer;
                                                                                            text-align: center;
                                                                                            text-decoration: none;
                                                                                            font: 16px/100% arial;
                                                                                            padding: .5em 2em .55em;
                                                                                            text-shadow: 0 1px 1px rgba(0,0,0,.1);
                                                                                            -webkit-border-radius: .2em; 
                                                                                            -moz-border-radius: .2em;
                                                                                            border-radius: .2em;
                                                                                            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
                                                                                            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
                                                                                            box-shadow: 0 1px 2px rgba(0,0,0,.2);
                                                                                        }

                                                                                        .blue {
                                                                                            color: #d9eef7;
                                                                                            border: solid 1px #0076a3;
                                                                                            background: #0095cd;

                                                                                        }

                                                                                        .blue:hover {
                                                                                            background: #007ead;

                                                                                        }
			}
		</style>

		<!--[if gte mso 9]><xml>
			<o:OfficeDocumentSettings>
				<o:AllowPNG/>
				<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml><![endif]-->
	</head>

	<body style="padding: 0; margin: 0;" bgcolor="#e5ecf0">
		<span style="color:transparent !important; overflow:hidden !important; display:none !important; line-height:0px !important; height:0 !important; opacity:0 !important; visibility:hidden !important; width:0 !important; mso-hide:all;"></span>

		<!-- / Full width container -->
		<table class="full-width-container" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" bgcolor="#e5ecf0" style="width: 100%; height: 100%; padding: 30px 0 30px 0;">
			<tr>
				<td align="center" valign="top">
					<!-- / 700px container -->
					<table class="container" border="0" cellpadding="0" cellspacing="0" width="700" bgcolor="#ffffff" style="width: 700px;  -webkit-border-radius: 9px;  -moz-border-radius: 9px; border-radius: 9px;">
						<tr>
							<td align="center" valign="top">
								
<table class="container" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
												<tr>
													<td class="cta-block__title" style="padding: 55px 0 0 0; background-image:  url(https://www.talkag.com/img_flx_cont/talkag.png);background-repeat:no-repeat;background-position:center center;background-size: auto 45px;"></td>
												</tr>
</table>

								<!-- / Title -->
								<table class="container title-block" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    
									<tr>
										<td align="center" valign="top">
											<table class="container" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
												<tr>
													<td style="border-bottom: solid 1px #eeeeee; padding: 35px 0 18px 0; font-size: 26px;" align="left"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<!-- /// Title -->

								<!-- / Paragraph -->
								<table class="container paragraph-block" border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td align="center" valign="top">
											<table class="container" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
												<tr>
                                                                                                    <td class="paragraph-block__content" style="padding: 25px 0 18px 0; font-size: 16px; line-height: 27px; color: #000000;;" align="left">
                                                                                                        Conditions d'utilisation
Accord de l'utilisateur
introduction

Nous sommes un réseau social et une plate-forme d'affaires en ligne pour les particuliers dans le domaine agricole. En utilisant nos services, vous entrez dans un accord légal et vous acceptez par la présente toutes ces conditions. Vous acceptez également notre Politique de confidentialité, qui explique en détail comment nous collectons, utilisons, partageons et stockons les informations personnelles de nos utilisateurs.

1.1 But

Notre objectif est de connecter les individus dans le domaine agricole et de leur permettre d'élargir leur réseau, en leur permettant d'être productifs et performants. Notre plate-forme est conçue pour vous permettre, ainsi qu'à d'autres professionnels, de se rencontrer, de développer des relations professionnelles, de fournir des idées et des encouragements et de montrer du soutien les uns envers les autres dans un réseau de relations de confiance.

1.2 Accord

Vous acceptez qu'en vous inscrivant à TalkAG, vous concluez un accord juridiquement contraignant, même si vous utilisez ces services pour le compte d'une entreprise. Ce «Contrat» inclut les termes définis dans le présent Contrat d'utilisateur et la Politique de confidentialité.

Les utilisateurs qui s'inscrivent à notre plateforme sont des "Membres" et les utilisateurs qui visitent notre plateforme mais ne choisissent pas de s'inscrire sont des "Visiteurs". Les conditions énoncées dans cet accord d'utilisation s'appliquent à la fois aux membres et aux visiteurs.

1.3 Bêta

Les utilisateurs qui s'inscrivent à l'édition bêta du site Web comprennent que ce service est une version de version bêta et qu'il n'est pas conçu pour fonctionner au niveau de performance typique d'un produit disponible pour une version commerciale. En tant que tel, vous utilisez ce produit à vos risques et périls. Ce service sera probablement modifié fréquemment, ce qui peut entraîner une perte complète et totale de données. La plate-forme peut fonctionner de manière incorrecte et sera probablement modifiée de manière substantielle avant sa diffusion publique. Nous nous réservons également le droit de décider de ne pas libérer le produit du tout. La plate-forme et toutes les parties de la plate-forme sont fournies «telles quelles» sans aucune garantie. Nous déclinons toute responsabilité en cas de perte de données, de perte d'informations, de modification de la plateforme ou d'indisponibilité de la plate-forme.

2.1 Obligations

Pour utiliser notre plateforme, vous acceptez que:

Vous répondez à l'exigence d'âge minimum, qui est de 14 aux France,; Toutefois, si les lois exigent que vous soyez plus âgé pour participer à la plateforme, y compris la collecte, le stockage et l'utilisation de vos renseignements personnels, l'âge minimum est alors plus élevé. La plate-forme n'est pas destinée à être utilisée par des personnes de moins de 14 ans.
Vous possédez les droits sur tout le contenu que vous publiez ou que vous rendez disponible sur notre plateforme. Conformément à la loi Digital Millennium Copyright Act, TalkAG répondra aux avis de retrait DMCA en supprimant tout contenu jugé par TalkAG, à sa seule discrétion, de violer ou de porter atteinte aux droits de quelqu'un ou d'autre chose. Si vous êtes titulaire d'un droit d'auteur ou si vous êtes autorisé à agir en son nom, veuillez consulter notre Politique DMCA.

Vous n'afficherez que du contenu en harmonie avec le but de cette plate-forme. TalkAG, à sa seule discrétion, se réserve le droit de supprimer tout contenu obscène, provocateur, offensant ou autrement discutable.

Vous n'utiliserez pas notre plateforme pour abuser, menacer, diffamer, harceler, intimider ou usurper l'identité d'une personne. TalkAG, à sa seule discrétion, se réserve le droit de résilier l'adhésion de toute personne en violation de ces termes.

Vous ne publiez aucun spam sur cette plate-forme, y compris les flux appartenant à des groupes et / ou à des membres individuels. TalkAG se réserve le droit de supprimer tout contenu que TalkAG, à sa seule discrétion, considère comme spam, ce qui inclut - mais n'est pas limité à - publier du contenu dupliqué, publier du contenu promotionnel dans des flux que vous ne possédez pas, publier hors-sujet contenu.

Vous maintenez activement les groupes que vous créez sur notre plateforme; Dans le cas contraire, vous renoncez aux droits de propriété. Si aucun administrateur de groupe n'a publié de message dans votre groupe depuis plus d'un an, le groupe sera défini comme inactif, auquel cas TalkAG se réserve le droit de désactiver, renommer, supprimer ou modifier le groupe inactif.

2.2 Adhésion

Entre vous et TalkAG, vous êtes le propriétaire des informations et contenus que vous publiez ou soumettez sur la plate-forme - y compris les informations ou contenus saisis dans les sweepstakes, cadeaux, concours ou autres promotions TalkAG - et vous accordez à TalkAG licence exclusive: le droit mondial, transférable et sous-licenciable d'utiliser, copier, modifier, distribuer, publier et traiter toutes les informations et contenus que vous fournissez via notre plateforme ou directement sur nos comptes de médias sociaux, sans autre consentement, avis et / ou compensation à vous ou à d'autres. Ces droits sont limités de la façon suivante:

Vous pouvez mettre fin à cette licence pour un contenu spécifique en supprimant ce contenu de la plate-forme, ou en mettant fin à votre abonnement, à l'exception du délai raisonnable de suppression de la sauvegarde et d'autres systèmes.
Nous n'inclurons pas votre contenu dans les publicités pour les produits et services de tiers.

                                                                                                        
                                                                                                        
                                                                                                        
                                                                                                    </td>
												</tr>
											</table>
										</td>
									</tr>
								</table>

	

								<!-- / CTA Block -->
								<table class="container cta-block" border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td align="center" valign="top">
											<table class="container" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
						

												<tr>
													<td align="center">
														<table class="container" border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td class="cta-block__button" width="330" align="center" style="width: 300px;">
																	<a href="https://www.ibui.me" style="border: 3px solid #eeeeee; color: #969696; text-decoration: none; padding: 15px 45px; text-transform: uppercase; display: block; text-align: center; font-size: 14px; -webkit-border-radius: .2em; 
                                                                                            -moz-border-radius: 6px;
                                                                                            border-radius: 6px;
                                                                                            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
                                                                                            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
                                                                                            box-shadow: 0 1px 2px rgba(0,0,0,.2);color: #d9eef7;
                                                                                            border: solid 1px #0076a3;
                                                                                            background: #0095cd;">'.ucfirst($answer['IPR_ANSWER_BUTCONNECT']).'</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<!-- /// CTA Block -->

								

								<!-- / Info Bullets -->
								<table class="container info-bullets" border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
									<tr>
										<td align="center">
											<table class="container" border="0" cellpadding="0" cellspacing="0" width="620" align="center" style="width: 620px;">
												<tr>
													<td class="info-bullets__block" style="padding: 30px 30px 15px 30px;" align="center">
														<table class="container" border="0" cellpadding="0" cellspacing="0" align="center">
															<tr>
																<td class="info-bullets__icon" style="padding: 0 15px 0 0;">
																	<img src="https://www.ibui.me/img_flx_cont/img13.png">
																</td>

																<td class="info-bullets__content" style="color: #969696; font-size: 16px;">'.ucfirst($answer['IPR_ANSWER_SENDTO']).' '.$DATA_notify['Recipient_email'].'</td>
															</tr>
														</table>
													</td>

												</tr>

												
											</table>
										</td>
									</tr>
								</table>
								<!-- /// Info Bullets -->

								

								<!-- / Footer -->
								<table class="container" border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
									<tr>
										<td align="center">
											<table class="container" border="0" cellpadding="0" cellspacing="0" width="620" align="center" style="border-top: 1px solid #eeeeee; width: 620px;">
												<tr>
													<td style="color: #d5d5d5; text-align: center; font-size: 15px; padding: 10px 0 60px 0; line-height: 22px;">Copyright &copy; '.date('Y') .' <a href="https://www.ibui.me" target="_blank" style="text-decoration: none; border-bottom: 1px solid #d5d5d5; color: #d5d5d5;">íbui</a>. <br />All rights reserved.</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<!-- /// Footer -->
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>

